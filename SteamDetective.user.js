// ==UserScript==
// @name         SteamDetective
// @namespace    https://gitlab.com/carbonwind
// @homepageURL  https://gitlab.com/carbonwind/steamdetective
// @supportURL   https://gitlab.com/carbonwind/steamdetective/-/issues
// @version      1.22
// @description  Displays a quick summary of a person's reputation and additional information
// @author       carbonwind
// @match        https://steamcommunity.com/id/*
// @match        https://steamcommunity.com/profiles/*
// @exclude      /^https?:\/\/steamcommunity\.com\/(?:id|profiles)\/.*\/.+
// @exclude      /^https?:\/\/steamcommunity\.com\/(?:id|profiles)\/.*(\?xml=)
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAAAAAA6mKC9AAAAWElEQVR42oXPwQnAMAxD0VwzpsbxEFrkT1ejkgZCoD4I/MDYHvOocQcZrA2mpMILTIc08QsiCBMFXA2NqBxoTyDxgQB3rhGRneVAO+kn2mvT+zzsfvrvtw8r4zYhV5URUAAAAABJRU5ErkJggg==
// @run-at       document-end
// @grant        GM_addStyle
// @grant        GM_xmlhttpRequest
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_deleteValue
// @grant        GM_setClipboard
// @connect      backpack.tf
// @connect      mannco.store
// @connect      skial.com
// @connect      steamcommunity.com
// @connect      steamhistory.net
// @connect      steamrep.com
// @connect      steamidapi.uk
// @connect      steamtrades.com
// @connect      rep.tf
// @connect      vacbanned.com
// ==/UserScript==

// Steam variables used in script
/* globals g_steamID g_rgProfileData ShowAlertDialog */

// Settings related to the script
const DEFAULT_TIMEOUT = 15000;

var steamdetective = {
    agedisclaimer_enabled: GM_getValue("agedisclaimer_enabled", true),
    forcedfallback_enabled: GM_getValue("forcedfallback_enabled", false)
}

// Module properties
// - id_prefix: Prefix to use in HTML IDs
// - module_name: Name to use in the user interface
// - summary_name (optional): Name to display in summaries, if not set will use module_name
// - summary_enabled: Indicates if a module will show up on the profile summary (right side)
// - requires_apikey: Indicates if module needs an API key (for error messages)
// - api_timeout: Time to wait for a response (in milliseconds)
// - api_key (optional): Property used to get the API key (if applicable)
// - profile_url: URL to use in summary
var steamrep = {
    id_prefix: "sr",
    module_name: "SteamRep",
    is_enabled: GM_getValue("steamrep_enabled", true),
    summary_enabled: GM_getValue("steamrep_summaryenabled", true),
    history_enabled: GM_getValue("steamrep_historyenabled", true),
    requires_apikey: false,
    api_timeout: GM_getValue("steamrep_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://steamrep.com/profiles/${g_rgProfileData.steamid}`
};

var mannco = {
    id_prefix: "mannco",
    module_name: "Mannco.store",
    is_enabled: GM_getValue("mannco_enabled", true),
    summary_enabled: GM_getValue("mannco_summaryenabled", true),
    requires_apikey: false,
    api_timeout: GM_getValue("mannco_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://mannco.store/store/${g_rgProfileData.steamid}`
}

var vacbanned = {
    id_prefix: "vacbanned",
    module_name: "VACBanned.com",
    summary_name: "VACBanned",
    is_enabled: GM_getValue("vacbanned_enabled", true),
    summary_enabled: GM_getValue("vacbanned_summaryenabled", false),
    requires_apikey: false,
    api_timeout: GM_getValue("vacbanned_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `http://vacbanned.com/view/detail/id/${g_rgProfileData.steamid}`
}

var steamhistory = {
    id_prefix: "sh",
    module_name: "SteamHistory.net",
    summary_name: "SteamHistory",
    is_enabled: GM_getValue("steamhistory_enabled", true),
    summary_enabled: GM_getValue("steamhistory_summaryenabled", false),
    requires_apikey: false,
    api_timeout: GM_getValue("steamhistory_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://steamhistory.net/id/${g_rgProfileData.steamid}`
}

var steamiduk = {
    id_prefix: "sid",
    module_name: "SteamID.uk",
    is_enabled: GM_getValue("sid_enabled", true),
    summary_enabled: GM_getValue("sid_summaryenabled", false),
    requires_apikey: true,
    api_timeout: GM_getValue("sid_apitimeout", DEFAULT_TIMEOUT),
    api_key: GM_getValue("sid_apikey", ""),
    my_id: GM_getValue("sid_myid", ""), // Exclusive to SteamID.uk, required for API requests
    profile_url: `https://steamid.uk/profile/${g_rgProfileData.steamid}`
}

var steamtrades = {
    id_prefix: "st",
    module_name: "SteamTrades.com",
    summary_name: "SteamTrades",
    is_enabled: GM_getValue("strades_enabled", false),
    summary_enabled: GM_getValue("strades_summaryenabled", true),
    requires_apikey: false,
    api_timeout: GM_getValue("strades_apitimeout", DEFAULT_TIMEOUT),
    profile_url: `https://www.steamtrades.com/user/${g_rgProfileData.steamid}`
}

// Global variables
var profile = { id2: "", id3: "", id64: "", custom_url: "", username: "", is_vacbanned: false, has_fakeid64: false };
var status_names = {
    400: "Bad Request", 401: "Unauthorized", 403: "Forbidden",
    404: "Not Found", 405: "Method Not Allowed", 408: "Request Timeout",
    414: "URI Too Long", 429: "Too Many Requests", 500: "Internal Server Error",
    501: "Not Implemented", 502: "Bad Gateway", 503: "Service Unavailable",
    504: "Gateway Timeout"
}

// HTML elements
var username_label = document.getElementsByClassName("actual_persona_name")[0]; // Steam username in profile page

var bptf_info = document.createElement("div");
var marketplace_info = document.createElement("div");
var scrap_info = document.createElement("div");
var skial_info = document.createElement("div");

var bptf_summary = document.createElement("div");
var bptf_sumtitle = document.createElement("a");
var bptf_sumstatus = document.createElement("span");
var bptf_trust_summary = document.createElement("div");
var bptf_sumpostrust = document.createElement("div");
var bptf_sumnegtrust = document.createElement("div");
var mptf_summary = document.createElement("div");
var mptf_sumtitle = document.createElement("a");
var mptf_sumstatus = document.createElement("span");
var scrap_summary = document.createElement("div");
var scrap_sumtitle = document.createElement("a");
var scrap_sumstatus = document.createElement("span");
var skial_summary = document.createElement("div");
var skial_sumtitle = document.createElement("a");
var skial_sumstatus = document.createElement("span");

// Settings storage variables
var bptf_apikey = GM_getValue("bptf_apikey", "");
var bptf_apitimeout = GM_getValue("bptf_apitimeout", DEFAULT_TIMEOUT);
var bptf_enabled = GM_getValue("bptf_enabled", true);
var reptf_apitimeout = GM_getValue("reptf_apitimeout", DEFAULT_TIMEOUT);
var reptf_enabled = GM_getValue("reptf_enabled", true);
var skial_apitimeout = GM_getValue("skial_apitimeout", DEFAULT_TIMEOUT);
var skial_enabled = GM_getValue("skial_enabled", true);

// Logs variable of logged in user's ID64 in the console
console.log("[DEBUG] | g_steamID: " + g_steamID);

// Website icons (all stored locally as Data URIs to avoid tracking and improve loading times)
const ARCHIVEIS_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAACZklEQVQokW2SXUhTYRjHn/ecszPPkp05I2tJsrn1ZWXLibA0EBrUTWMXC7ULky4jr4IkchcrJt1EF13Joo8bzUEbjKi8WjkEWezDoKmRW+LamNtu2gfnnL29XZwlOnpunueB/+953+cDEUIAwOv1UhQliiL8z1QqFc/zdrsdABgACIVCqVRKkiSLxYIxblLTNB2NRrPZrCiKTqeTAQBBEAwGQzqdBgCEUBNACGlra7NarZIkNV7Q6XSJRCKZTPI8jzFmlSyFKACoY1yXJI7jpqamAoFAuVwGACT34HK53G63XHLpXShfLQCFLhhP63tPAYAoil6v12azCYJAySKdTgcApWxpePrybPcHX9fas/znB9r0bY8LAFiWrdVqJpNJkqQGQIAAwOC9wXaLZvPViqGvs298OPP044ax487jR7v9mM1mRo6UrHJudu7q6ytcXVXb2dxa+k6xB5Vn2aHrEyuxr5m1dYZpKHcdHVXH1dCRif86M3KxpbULwSHIf0PAmc0Dy3PzDE3vA4BC1bSYns+Ukjn1Kq3gSohS58IbuMZWEHWOoeHfuBvAH4J7VN2akS6hVs2v5jTHDbSyvc61nrw2tl4q6mPxbC63DxBE8db4xNH7E5cGzqcWI5abRsTgrYWQUFfTkWjfzEwoHm98RXYYYyV/IHLjYYZW99+dbuk8lvnxc+jF88qX2MvJyb2LZ/Ym+p4Tn44cfrv4fjkc7tfre98sPPF4mi6lASgUiu3t7UgkgjFu12jGRkcxxr/LZZ/PRwhxOBzNQKVSCQaDPM8DQKFY3CkU5CtECBFC/H6/VquVlX8BvK//yDJEbNYAAAAASUVORK5CYII=`;
const ARCHIVEORG_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAABWUlEQVQokX1RQarCMBCdGaXWqvfwDLpyI+gFvIfeQnDlLbyAeACLBxAXdi0IXUSFlpY0mb+YkJ9f4b9FGF7mvcybICICgD+NMfAXRMTMUqOAiHzfYrGYzWbz+TzLssvlstvtvNKbIjNvNpvxeDyZTOq6bprGGENEiBjHcZ7np9Npu926blHfbreyLK218AVETJJktVpdr1dE7MoLh8OhqqrhcFiWJTMnSVJVVdM0o9GoKIooip7P528mANjv94/HAwDSNE3TVBillDDn81k6EbErGxBXKcTIWutrzwCAW5m1thXAC1o8facMR/XbbAtCttXRYkicmDn88jCMr11oGVEplec5ALxeL7lWSn0+HwB4v99haPfTx+OxKIp+v1/XNTPHcay11loL0+v11uv1/X53a10ul1mWGWP8YCGYudPpTKdTJ5BXBoOB1rqVLwwWRZEL8235D4joB1rD3NJ+rOl5AAAAAElFTkSuQmCC`;
const BING_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAfZJREFUOI2Vkz9oU1EUh7/30uSlf15T0GAjlhYpWDAWoaBgwUHBRTe7unQsSNWhi+BqHdx0rJMIgg4VRFrBySVUKuJQO4gStLFNja9pXpP33r33OKS1CaYSDxy45/7u+fide7mWiLAXLwtMCAxeyXCfdkNE/iSvZWF0wRcWZaWwI/FG7aC0G2HHIiJX2YxHtZHMIsV2DHQ0FroKxq4zM0GQAuf/AFLb35HAasdA8wimVodIrb4GsOZZmV7mWb7SDqAKEtpIaGOCXSlB9MYzVweXQjP1mYffon8AdAXEN4hv0NsGgBdZnpQtzYijrMe/SlMDJT4U5ACA8kFXNLqiUdsKgFN9zOZFE3QoEk5EeiM/umq43hIQboGUNVLWhJ4GYMiF+Wyy/0t/7/NOp9sb7jbcjHGpJSCogPIjlB8RbO8PO97F+uw7/HCm0Fec08FR/Nie1vSMbBGL1G6jV7+D2zlmnuY277olY6cHelh9W3VuoD+1dHDnGuGPrz7++g7DUWjO5/i49N27N9Rl2V1OnEQ8gUolmCB+q6WDMxeYHAkOb7pRR9U9kXCUX82qmEVg2Wgs3nuHltfmGDvS0NPk4LLLz4tn3Un7dLJzx4jta6goWAuQc8f7puURY5l009BYjd95L14VIQkP0PToiI2TKWYyvX8dA+A378gC8sqr9y4AAAAASUVORK5CYII=`;
const BPTF_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAqFJREFUOI2Nk8trVHcUgL/zu3fuXJ1mNI1R8VFp8FFRE7VBsaaIiqBiF6IoUmqRlm4KoitRaBZVUETqJgtF/AfUqptAKSpiDaJGLanGxMQMYXxMY5yYTDTJzNxzXIyYilI8q/PgPDjnO2KmfIwYIAgi8o5fuvutCpAPZgEioEBkBFVJ7jOYRcoqRuPb6xvsrfHfbiKMjBTo7csiKGXJ8SysmvbPLzWvqklMQWrWAuDiMb8YeI7A98gXChQKeeK+x8jwMHVzp3L+6F7+PLafVfM/oyOTXdCQqrxJtgNrPlsqYGZEkZJwBXasW0bd3Onki0rCi/h5+2YSY0IuXr1Btn8QD+VWx+PaE5nPm7A81noJZ0Aq/Yht61ey5uulJMs+oa0rRfXMGQDcbX/IdweO0/ooiwCiRZra0l+dSFdeJJtCtuz5rfDThuX+6rol/3uFTTt/ZVx5BWqKAEXxWbt4VqPzYx5/NbfwtKcXgNYHXZxuvMDtu20APMu+4Nwfl3CeR4SVLoKBQM/AUMb5IlzryNDZnQbg9r12jjde52pzCwCZnl6OnLpAmEiixQiLFFWYNan83u7J7T/6quA7wXMeAJ5zjI0HxLyS7ZwwNhZgplipMfEwPlRf/XI+3kScmmJmqJaIjAcxHj5+ilHCwwzUFFPDLCLyYpzcsmA9kSDLvsepQRiGtLR1ArBx3SrOH97N0PDI2wnUQM0oSsCGL2cfpPvWZVnxAwC+mRL4jjNX/mZiRTmL5s3heV8/fQODpNJPSD/5F99zREDtjAlN34Z39hFWjqL8za5Dxht0c6+GeJEbJAziJMKA5wM5wiBG5afj8dHc7yvzSSZ9gSzZOlqgKzf6TKol5c06cK6kq8ObXaYPyHQiU+a8w4eYGh8nhjj3nvc1RtIbNJNjfhMAAAAASUVORK5CYII=`;
const DEFAULT_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABs0lEQVR4AWL4//8/RRjO8Iucx+noO0O2qmlbUEnt5r3Juas+hsQD6KaG7dqCKPgx72Pe9GIY27btZBrbtm3btm0nO12D7tVXe63jqtqqU/iDw9K58sEruKkngH0DBljOE+T/qqx/Ln718RZOFasxyd3XRbWzlFMxRbgOTx9QWFzHtZlD+aqLb108sOAIAai6+NbHW7lUHaZkDFJt+wp1DG7R1d0b7Z88EOL08oXwjokcOvvUxYMjBFCamWP5KjKBjKOpZx2HEPj+Ieod26U+dpg6lK2CIwTQH0oECGT5eHj+IgSueJ5fPaPg6PZrz6DGHiGAISE7QPrIvIKVrSvCe2DNHSsehIDatOBna/+OEOgTQE6WAy1AAFiVcf6PhgCGxEvlA9QngLlAQCkLsNWhBZIDz/zg4ggmjHfYxoPGEMPZECW+zjwmFk6Ih194y7VHYGOPvEYlTAJlQwI4MEhgTOzZGiNalRpGgsOYFw5lEfTKybgfBtmuTNdI3MrOTAQmYf/DNcAwDeycVjROgZFt18gMso6V5Z8JpcEk2LPKpOAH0/4bKMCAYnuqm7cHOGHJTBRhAEJN9d/t5zCxAAAAAElFTkSuQmCC`;
const DISPENSER_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAABRFBMVEUAAACEExCDEg+MHRtmERJnEgxwExJzFhlyFRhqFRRhFBBQCwdVBwp9Cgt7Bwl6BAB4AQCIKihmGhk/CwlSDQphCw2eDAmcCgibBwCoODXjxcTVt7a3m5noyci3Yl+nMC2sNTauNzhvGx1DBwqbBgfszczct7jlwMHbvbzq1diUDgvHf4HpysnixMLCeXWvNzOTDAqNAQCiMzHYs7TgwsDZu7qVEAySCgnNi4nrxcZuGReTRz2QCAiVHRpvKSd+VU7PlJbMkpS2nI7RlphsGBZZDAdtS0JqCw1vGhhxMSttWFBHCgZbDglkDxBQGx5iDAddERJKIR5kDwk5BQh/DARqHRxrHx1wIyBwJCWGFhdqCgRPJSKVPj9APDo6NjU7NzZSKSp0FhROEhA2MjE3LS40MC81MTBRGxpbFxM+AwNIEw1IExLfQvxRAAAAAXRSTlMAQObYZgAAAKtJREFUGBkFwTFKxFAUQNF7n+//6CRkZIhgKa7AfdiKhStzNZaCja7BatqAgxll4vccEZsgAKymAiACaq+CoIA6qaqqqk46qrSl9zs0gFoXYFsO2xECqPVqYLyYWWYIuKGVsuMIxwECfruOn8/9DJyucfI2T0UV1Y9Y7t77y7d1s8k8O/cV79UG4Qrp1+gD2oAQQHxSAfQPQh0eVbAhtDAOXS2ZL1lKZpZ8/gdaDCWUu0mvMQAAAABJRU5ErkJggg==`;
const ETF2L_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAwtJREFUOI2lk11sU3UYxn///zk97Wk3u3Vrx+bYHOB0E+cIG2zihhPIAtFMDd6oEOON4oUxi4RkEomaoKbqhVGJ2fAG0RCDERQVUZkJs4kDg3GubKOjzT5kXbcxdtLu9OMcL2QKeslz+SbP8z7J+3vhJiX+OwiGbI6OoLzVyjq3i6HvRkn7s1MPeopKzmiS8UdrbrSoS6YCN6RtNKdAydewPS7qNZW4LXD2HT+4c/uzXYkvIyTe6bczQK6zUfzbIBiyaV8JyQxrvC5W1RSJo22f2I7TETJvb8GVp1ImBcb0r8f21t3fcaLCyzcnI7C7WSCDIRuAd39BZi2i2PQBorUU8/XNYApSzzSI0WQGM25ko7F5knU7UGxwBEO2Q1wLKPr2w72dxtz0Mp+vkMMfvJHyOpk90Tfo3bS+1uwPxwr2vLxfkVLiL/ZhAYnEjNDzC+MqwKXfzrTFBkIv5LJZ910VzWSMGUbG5/ns8EFqb32eYs1EtxaYmLhM/YoNzF81GBobtv2Vtd0q4On/6qPttmW5pZTMzl3h/Z5DDISHCQ9f5MgXX9Pe1kJV5XJiYxMMDl0kk80iVW3ykRffO6Raudy6+emJjQBSSpoa1rC2fjUjkShCCCKXYnycmOHCcARFkQghWFw08QbKe204pxizU/uMufgGQFiWRUmgmKcffwxFUcjzeHitq5PSEj/f/9SHaaZpaW5EczqTd2zauf+28vLfVX9F9R/uW3xvSilxOKRq6gFsh57ZvHWbtXHLNlHgIVN5Z72rsKJGsx3j1VctfbLhgYdjtzc29WoClq5AKg2leYizU+jRcyfv9a1sHzyV5s/EU+hwRQv2F1Sf7dlzoHHrEwfurqvraV9xHUhLMGUs1MEfj+xIjv68z5kf+GH5Qy+9unj+0ydT09EWpeq+7tzlgbVNHbtO6xqnJhb+Bkm9nmuPg+yyVffEw+Fe018VmDQFC2PRkVLLmCla39FyoWtX6+fdrzxng/3P7hsaXFOZkaNMlxi6RsTMsHoxjStf43wWUrub//d/N6e/ABSTOJ9mBF3zAAAAAElFTkSuQmCC`;
const ETOOLS_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAAByklEQVQokY2RzWsTURTFz0y+qpNCxpHgKtKFJVqTIBXSGgW71oWFSEW6EDHt0l0RhCwsLYLgwmWpH+BS0P8gFRFbSaxaXKhQNYItNkrS5nMy8+a4eGNoa4Qe7urB7553zlUyg5PljxXsTXo0hLQ2xj0rrY15/8LOesWceLhSFUrdErEDgduXB8K9PoKd9Qo8AFyHtXJDu5HbbLblpkqzHb258HOzRQqHthzp4ALn7y5ttdok8wuFfK5A8le1lZlbJrkL8EIBgKaN3oCv8PzNzMgsgOyL7IkziULJ/De3m6FuCQAgCBD0qgCw36d2AwgAR42ejap5cmRw+mWWQCyV+P673hfsArhPd8YHjk2/+lFuxFKJeCrxtdSIGJq+z//sddEtp+OgqABgaL7VW6enHn/Il8yaJU6F/UYwcO9KfOLBClAcTR4mhEvIlixhk/b2Gz1ZLF6bXyaZuf/+6dK3zuHcL3lUGVcQwnYEINJDkXPHjcn5t3NX47lPW4ufN3a0tF0StoRzIRkhcP3Ru6EjB1fXasP94e6AlN9DUzijycghvefLem38bN8Oh11VSAU8ADDcH5a7XUCPhi4GL/3PR1FBQlFABwD0aOgPV/QYSI9k7OEAAAAASUVORK5CYII=`;
const FIREPOWERED_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAIAAACQkWg2AAAAA3NCSVQICAjb4U/gAAABtUlEQVQokZVSPY9SQRQ99848zZPNrkQ+3E0gYqIxMQtGC2yxtbKwpPA/yA/Q+BPo4AeQGCtaChOSzYtBuqUwFNK4FBbq233PPJl7LYZFV2KxU01u5pxz55xDzWYTlzlWRC4HUFVVJaL/vVDVCwAn0rgRHuQsQwACFIDslsyt+yrCzKvVarlcTqdTAMxsneizfX66FwsZhbIyVOThveDFK6gSkdefz+ftdjtNU1MqlRybz9nVaWwnP+y1MChzpgd3zIMnSZL0er3FYlGr1crlchiG4/HYinPvT34e7ewBAGj/+pXD8MyvmyRJt9sNgqBQKLRarUqloqrWqT6v5l7fjSEMAA7u3DZrbb1eLxaLjUYDwGw2c85ZFfn0ffXu646CBfooJzVOPCCfzw8GA29gFEX9fh+AVZGPJ98+fHEEJsWbxzdv59cKaZoOh8Msy6IoGo1GIkJE6+AMAAgpEf7kGMdxp9MhIiIyxnjHLiTNqqKq4F+qFlBVZmZmH59P0HohP3Kgt/N4shu45cQcvzw7Pd0uDlWr1U0vNh35pyye20/sRkvPc/UXz70NW//hb44NcruIRPQbXDP5e/4Mvl4AAAAASUVORK5CYII=`;
const GHOSTARCHIVE_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAADFBMVEXx9vzD1PmQr/BbgdNH4I8iAAAAWUlEQVR42mPYo7XqF8MfrVX2IIKf4Q8XkPjLtFKf4W9DynqG/0+41jPs/7JqP8Ny79//GEL5/61iCHVg4GJYOoGBkWGZ1v4FDP///1vC8GX9qxUMT7J/LQIAizglEuKez2AAAAAASUVORK5CYII=`;
const GOOGLE_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAglJREFUOI2Nk99L02EUxj/vNkthRdToFxLNJqaJESYVXtSN3ZS1CqMM6qKLQKJ/oPsF3eV1F1HRBrVcGV4FdRPeJUK0oeYaUmyzRbOxpvtuTxd+v7atIA+ci/e8z3nOe57zHmgwSQFJEUkzksq2z9ixtkZ8bSKSxvR/i0r6Z3LCQRRfxZS7claZ7u3KHPApd3lQxfE67rhDYmyCMSCo5RLfh89QSc6A2w1WebWCZwNaLtLU08fWxy+cus+NMRecniVJi+dOKtPfrUzPLhUij2TNz8lKzqkQfqjs0XZV8j8a2/FjiyPrQ1iZI61Kd+yUtZBahxSSpDC2wrImA1qZQMXo6HqTJSnhAfwAys9h3NA80FsnsLn1kw6vWTv/qkCwy829ay0AbZ46sMBUGkZUY9uaDW4DtQgXkATwbG6nBEwsxOtHPLqJRMhLIuTl4mEPhbLY53M51/Mu4D1AevcdDi6e4NRUhM9LX/6qns3D/UmLchX6O9cePmW0+j0/AQzErvO1mOVbpcjdQyMca+3FZVxMp6e5/SaFL32DPT6LJzdbHAK/85GiwPlSdYXg+Agfl1J4jJuyKghoMi5kCuzQcd4OhWjZCMAzY8yQQwAQB/YDvJx9zYPZGO9ycapA35YAVwOnudQ56FSOA13G/JmOsw/Rdcz+qRqXqU51yS8prNXlWrE9Ycf2NuJ/A6uf5JCErH2FAAAAAElFTkSuQmCC`;
const INVENTORYGIFT_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAIVBMVEX////y8vL///+IiIjt7OxpaWn////GxcWkpKQwMDAEBARrLJjqAAAAB3RSTlMEO5zl8Pj9kGzU+gAAAIJJREFUeNpjYGAQcktRZAACpjQgUGBgYFBLS0s3TQILlM5aEa7AIOYxq3LV1FmBDGpRK71WrVq1kMFt1dKuiMpVkxjSV01PjwyJMmPIaGmbtWpVexmD66wpq1atWpnEoLUqYtWqVUuTGKRWzQKJBDKwWpS7uLiUG4CsAIIkJEthzgAAHfku50U+VlEAAAAASUVORK5CYII=`;
const MANNCO_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAsdJREFUOI19kGtIk2EUx//Pe9k0Cy/RzTmXhguWSNQixSIYoQ0jyoj8Fgu7kEQUswILsqK+FBUVgZXEDDS6mOVGpbUSMygKs1olpF3casuttTZve9+dPgzXjNYPHnjO4Xl+nPMHALS2Emy26LHfJixfvqHw3g1KAwCbjWA0btHX1FzX2O2Ev2E7t18ssXdcvexyeYWkJAWJIk8et3/qpBRlSKNRPQkEQlqX06MWRX4sJ0ftqD10c2lZGQvFDPnzSi5VVu6it45estnaqampmXw+P715/Y5KSyqors5Cg4M+6uv7RFXb9lBBfukniyVuEm2eob7afIDGiURiVxoZGaV4ZDlCM6bpaeumM2vG/3MMoAhFAADBYAgcp8RG0w4AgFKpQEbaAszTGaKPOYY8bRYGBj5kxwTxgfh8fgC5eOvoi/XCYRmfP3onBscQ/qeAMQYAEEQ+1hMFDgrln5pJEoaHfPrWVsoCAI4AxnFRjyAIE0TRAuDk6IpfIkBYp8Og/6updt+K/qqqupVCSkqS5Bz4Bqu1DW73d2jUqZDCEqzWdkiShIy0ZKROT4Ox142nP0aQvasGIAVk01qh//2z+ezY0UeLmm+dPe/x+JIVCp7NmZPX5f/p1bicnukAYQo/NlcwVfBs7TrQs25IDQ2AVgfxfpt/7+EH6XGzTsRsthge3bVckwK/0rnihVAcPAh5SILU7wRZW8B3PBzeva9ZzycSKCR2bdhgyOXM1YhcrEfkVQ9Ypgo0KxukXwZ69Vzsbm+UuUQCjufDbCwEPhyAsKoUfPlqyPUXgNERQJaATDVGg/6pCQWFS9ftFzse+8PrK8EvXgS+uBjCydNAajrwwwvWacdsXVFXwgwAoKWFcO7s5kaP42UFK5gLzMwEeX1ATw8mq3LudHQ2Gf8rGOfEiRdFt64cWR0MeFXK5MnexUvK7cdPmW4CwG/w4EhqFFDJLQAAAABJRU5ErkJggg==`;
const MPTF_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAhFBMVEUvl9sxl9syl9szl9svldwAvZshjthHcEwwltsyl9szmNwRfdsLp4kzl9spk9szmNslj9cGnX4LpYgNu5otldozmNwNsZKtwrsfPv8Huo+RyroMvJoAuIeHybM1m980mNw0md02nOE3n+Uav58PporV09SIvq5IrJRnxKueybyzzMS90svC3L2oAAAAHnRSTlOmyeHurv5WALrS6jvy+HbcYEvf5pzu8+8vgfvJaGh79MtMAAAArklEQVQYlVXP2xKCMAwE0CitoYDijUG8bJsCXv///2yQF/chkz2TlxCn7Kqqattqpzvp2A/DcB8f+wlWmxUf+t7ex+eBU6E8RsMJXqNlE2NOObycut6+z91JPCYIBV/s58JFUFjAezFsz2zEeywUACmvt1IABQccHTLmDO4IOHIBxBBjBEwIjmoFSudCCjUtFbgJoWGF5QzbGLczFF6By1L/gC8o+8GUBBmt/2H9BQA9Ebsy06ZIAAAAAElFTkSuQmCC`;
const REDDIT_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABn0lEQVR4AWySA6wdURRFp2ZcRTVi1w1q27YdJ7Vt27YV1HYbfNs2HtY/N+cmj5Os8d6Hjv9BX6eOMEC4JiQJLkuycF0YaP7xKQLFzYSDQq4A4cm1/zQLJ74fVjSgllBT7qvpvb6/ryYqrm1dCcEI98+HQ0vgwlp4eR7GNoE+DlZT27F15YSIzU9jGsOR5SraPgVenPMZqGagYxsGht6WXtZgSSfYOd08C9WgXw397gt0zbHdhv41YGYHoT1sHg+vLkL0L4j8AY+Pw9phMKUFTG0N/apjDZKMgcu4sqwLpMVCdgoU5kJpIfaAsmIozIH0OEiOhKVdsJm4Ag3yMgDg8kbYMEqFpUVyPxpu7ASAgmxY2SPAIEnQtEyXAb48gUfHwFUJleVwew98uA8Ab276jzPJ10TjuH0qVFZARRnE/ddohTl6X14Kbhfsm+ffyGu+MZquD6kPl9ZDThr8/2CiKf/fa3nXt8PQhiFj9C2S+dC/po7v3Gp4ehqeCWaJlnfT1FXsW6Swq9zbUjUcfJBaXPlhIqmZidjs/Bgq5oGenQGmGkYQ+gIA9QAAAABJRU5ErkJggg==`;
const RGL_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAh1JREFUOI11k8trU1EQh7859+amebSxD6yCILZVUYqCC0sxirpSwRara8UuXEr9C/wDBIsLQURBAqILQVeCUAShiq9CEdRiKeKjCKbUlDZXc1/jIubRJP5WhzlzhpnvN0dokN4ZOej17p1y+rJFPj2B5c8gAiCgr+Tsw2P1+XZjAX4t2PaZ244mu5ziu8cEro+IAEom6XQ0ppumAqqIWIgYUqevYto3oqrlBlqouQBo5SBiaBs61/KuOoLmRvcDFoVFKK2C/2fw38yoKvam3di9Owm+z0Jbul1zo0OEoWFpHowg7q3RUskPncTwOLH+LIhgkt2qICK1tvV3Qb25KfG/vCYqLpE+dQUCD9Hc2PKK63cSS5A5f79CHICosMjqgwkS2QvEBg6DVWZTmUVCv+ZCtJYncpcxqW5KM/cI8gsE32bQMMB9OolM38Dq2kqsP4uz6zjiJMoMqjA2D2JS3fhf3+JO30TiaUSkbKFlQ+gT/JwnWFogPniyBrECy9lxBIDSm7vVx40SEdJjk2DF6mxUBI2wtw2jgUe48gNjpMkuwpImj17C6umrxawYNqKzG5JOBx8fge+Ssb32gqfb6203iU5SJy6LlX9f5OX1uSppjZrXS3Njh1ZW155JWwZ7yz7ie0YwPQOI7cDzay8kO3GgPr/5L+Q/WJmLs2DHa0VVaxAa1LzKYlDPXR9qAfT/HSBo5COhvz5sxVr8BPgL9KC++Hv3ClgAAAAASUVORK5CYII=`;
const SCRAP_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAt1BMVEVHcEwWiugAf//qTDsXieZBpvXbJCT/AAAAqv/ANSfINynBNSYjk+4ViObFMyEwmu8Piuv0RSzBNiTANCXDNSXBNSYWieYPi+4XieYNjfI+pPXTPzEejul3ZJHnSTc0rf/EMh8YiOVCer+jSlHiSDjANibBNScWieazLh4/pfVApfXANSfOPSzoSjjnSjfsSDQ3q/2pTlKXWWvWKQphc60XiOhIfMHNLxYIj/MYiebBNSbFNyjENCK2oB+jAAAAOXRSTlMAKAj+/P4HAgP9JahN24D8/OVa0EzcW4qto7z68RO84Pp+ZGT9kvLJMq2TafCsk07rf1OlVGt/3dzjtFwPAAAAuUlEQVQYlU2OxxaCQAxFA4zMhCKgKChgA3vvZuD/v0sYXHhXyT057wUEOIy5QxACWgRExIhGej2JVmjxmjFyNfhjmBJ54/G2lULXQUs7DTv4JWUxed5lOnWU0J2nS+RHt2vOVei8afks7tWs6gJXwvdH+RsRZSPqm8Xr0Z2hHOxDs145nIMgKJLSztoCDiejMAzjYIFpciWs42SSlHLV+39105eIfdtSobwGTFtKlEslVBKH3gAxBP4FPYIP3EjUE/QAAAAASUVORK5CYII=`;
const SEARX_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAdVBMVEVHcEwUFBQHBwcAAAAFBQUAAAAAAAAAAAAAAAAFBQUAAAAAAAATExMTExMAAAAAAAAAAAAAAAAAAAAAAAAUFBQAAAD////t7e1ycnLOzs7b29vV1dVra2uxsbEkJCS6urr29vbk5OSSkpLBwcGJiYmjo6Ofn58+KB04AAAAFXRSTlMA/lKRSJ68qPHUcXfe8f315cA2PreujiKhAAAAh0lEQVQYlWWPiQ7CMAxD067XNm7weu3m+P9PZFBgGrMURXmKZJtopfM0vDz9bq6oNAAOgvI3KKAx2mqop51egEf1Uo19Agq2H+62j5AJaGatg7MBRQIZYmhaH64QHx+MPgbvnfkaa9y6rnHIaSZosTPYzGmlyogMtn8lcjCxJEcwviQSl1X5JxdvCFsM3V/oAAAAAElFTkSuQmCC`;
const SKIAL_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAmBJREFUOI2Vk81LVGEUxn/nvde5o6PjjB9jyiBqkBUhGEJtbBFCi8xlIG1t07bWtWgV0R8QCOGioqJFJETmJumDFklglIiVn6mNpjN+zZ3rvafFTDqSm87u8D7nvA/P8xyJXnl0wwmVdKOBz/+UGMvNeYO2EyrpNpbdsW9eAxTwVAAoEUVQEKt4HicENhr4GvigAaAoQspKQkUjJMpABFaysDZPtTeFJT4gBXjg28WsVCFV0c7T3mZ6jgfYSOElwpuZWjofNFC99A6LPbammHaKOP0Xmuhp3WZgFEL3skj/NnfeCh31WUZ6E6zYyfxPhSpioFASIxnfYSbt0fdCkMUhytTl2tdmUmttOLYLktunZdECAX+D2VU4FHa5e87h+mgXSwsrsLHAraHXoC4JywMxBywQQ02wzOXhZQbOxzkaT/OkC365URbTdTyfDHj5eRyy3/PCFsqKnL7YJ2KSkLeqbHOe+xMW79O1lGo5voFEZIP2hEtjXRPD0xtEdtJ5Fqrz+1zAWKSkkjJ3lsWxcW5+iUJpAyRbuH3SoyWWIVx7mGB2blf93QWqAW60laudx/iZ8Xn44ROx7WnI/GZtBuZaj1AZUpJhYfVgEZWcWviOUl3lc+lMB5OpEwSB0lQTxjM5MhphdvkHUTnARhFD+foEzz4mONtWRbwy4FTM2QVuZsO8GlujIjOOHOgCglGP9ekRHqeaCcfqSVSEEYGl9SzZ1QWcrSmM7uxzwUaMJcbKZ1vy0SzdmkS3vrFYiLJBKf17TIWDEmOhqpbt5rxBJwT/nrNi0KIO0D2Iqlpuzhv8A1Ss++oiBIaEAAAAAElFTkSuQmCC`;
const SR_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAEtJREFUOI1jYBgFjEjs/9jkP2ER50PSx4SmmRENo2iCaUQ2lImBQsCCxkd2LiMDEYBqYQAzDMPv2DQhA5LCAF8g/kfDMBcRZchIBwC/LRIyZvwz+wAAAABJRU5ErkJggg==`;
const STEAMCOLLECTOR_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAcNJREFUOI2l079LlXEUx/HX97nXUsgiJbQhKDFyKbxk5Q+KoMYgoikCibagrcH+gcigIShwamiVfoC0FAQu9xItLgUOLdHvJDELTbvP82246PNQZkFnOh8OnzfnnO/5BojoH43VhEoIWqwTMVrMmHp6OQwFhIihazENiSRm61nzCAkxk1VHQikcGo3Vcsngv5qLkHqqVk6o/Mlcj8z84Huks0xLqTBKRkKlXJw54uVSI5/JqLRy/QhbW7j/glvTDLQSVroIWspF86eUeydZrlNq4uhevi4xt8DNXi6+p2eMgU05ZBWwnHGwg8P78jZvPKJtAzvauPSA8fM8Ps3wBF0bfwHMpxzfmZun39Hdzom+ht6/h9tPOHeMD3dzQLJi+JjSsz0HfPpCV0FvbmZ6lhIUlr4KmE/paM8L/bsZm8z1+FPO9vF6tth3MQ0o8WaOLKNzC1dOcfUhM8sM99K7iwt36G3OAWHwWowrorag8RwRTTw7w4HuRu3VZ65MMPmWbRsKgIHRuLDW/WeR50t8W86BlY2/HFO0WM6YKie/n3IS2NeMZmtGSEhTU//9mZKA6kgo1VO1GC3+zRyjxXqqVh0JpYCffrKfRkNiO78AAAAASUVORK5CYII=`;
const STEAMHISTORY_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQBAMAAADt3eJSAAAAJFBMVEW4eXmec3OTUVGVOjpuQkJiOjpHLi5dFhYjHR1ECgoZAAAAAAA08fiKAAAANklEQVR42mPYDQUMxDF2dEMZiWreYWBGQLmYKJgxWYSZFcxoYdJUBTOKzasqwYxVq1avIs4KALGRU6kNYQe5AAAAAElFTkSuQmCC`;
const STEAMLADDER_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAABP0lEQVQoz33QvWrCUBiA4W+wdnbURd2dLIK9Abu7CrY34ODq0CK04OQNdOvP5jk5JhxtHJREpxg5ngwhQl0MLVioS5c0Il/HGGjLcwMvL6g14mtfxq1TlucRp2zcaV/EV2vQW09wgSKUO/l5ZCfCBU6w9woknKOD8hcOzpGEQBlHE+211OLstYkcKYNhRmmytl5YJeRJZJXQC6ytNIcZ0FOszho8a4I8YgLPsgar6ymgj+phjJaQ93GWGKN6oA9AAuvPSAtJAOTdRIFLlN9xSxRoInmD/hV1uT/ryIq8OFKZdbhP3f4lPMEozfMAIhYpAIDnR+lngEGO3ihdveQm5WnETeolpUuvBzkgLxynaG/kKM7eTJEjGQLZ/7t6D8SboI0ikB9xIrBxjMQDrUo8bWu0nKI8izhFo6VtiadVfwAriWTb6bbagwAAAABJRU5ErkJggg==`;
const STEAMDB_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAAAAAA6mKC9AAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QA/4ePzL8AAAAHdElNRQflBw0XMwwuo7iyAAAA4UlEQVQY02NgwAGYmKAMRgYGBhY1cxVBhud3Lt36zcDAyMAgURwuyfJr25VvfB/mvWZgYWCw8uH982byrl8cmiX/uxhYGBhO5vBwf7spEqqpIfePgYGFgYEhi+kkQ4yD8L8PZ9YwMLAwMPgalbIpv1j19sbXXveZDCwMDKJSVhPX/v3PLFEgI8bAwMjAoL5U/tWt5wySamIPo28ysDAwOAhl8agLMtzc9KXT4SYDCwODmJzlhKX/GJgkCuQgWqTL/b/ces4goc6zsfMpAyMDAwObppmKIMP7O6eu/8LiOQwAAOC0QgrKG2WwAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIxLTA3LTEzVDIzOjUwOjU4KzAwOjAwqRiy6gAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMS0wNy0xM1QyMzo1MDo1OCswMDowMNhFClYAAAAASUVORK5CYII=`;
const STEAMIDUK_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAbFBMVEUYTLUYTLYVS7YYTLYAH68XTLYYTLcXS7YAOLIAOrIVSbYSRrIYTbf///8SS7bN1u2PodUATrp+ltJcfskBQ7Xz9/0ANrPBzOjf5vUAK7LV3fBvis3r8Pm5x+chXb+rud8wacRAccYAAK6arNs8D6eEAAAADHRSTlPsypm76/jIxsbrmXuFhwQPAAAAAWJLR0QAiAUdSAAAAJFJREFUGJVVz4kOgyAQBNC1NR6tg8glIGq0//+PXUmT4oSE7MuSDPSkW15Ewy3NBaKAlsi4yRkTnAkhOIZeWVil7apSSrNpqfaQiBqjhE9YugvULjPMI1RH/QdYz+UPxAPmcmP1B2wBdYT+QfR5g19g14iSb7tdxaZjc2LKcYJhEIa7C8FH5Or3z1X0aNoi1fsLczIOQYfGvX0AAAAASUVORK5CYII=`;
const STEAMTRADES_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAS1BMVEUAAAD8sgfjZU1NpBUVfsf8swfjZE9MpRUVgMj7sQbhYkxMpBQTfcf7sAbhY01LpBUUfcb7sQbgYkxLpBUUfsf6sAXgYkxLpBQTfcZQxMYbAAAAFXRSTlMASUlJSUpKSkrk5OTk5ubm5ufn5+c/OHvaAAAAV0lEQVR42nXMRwrAMBADQHc7xb3+/6UxLATWsLpIzEGMiEpJIUhrpRMaAtdex6jIGCUsEYLYFeeMAGGMAFB/KLtsfSyAKbchn7n3HIHv3Z+QEeh8aeLtA/IfA5EtgV/9AAAAAElFTkSuQmCC`;
const STEAM_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAAmJLR0QA/4ePzL8AAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMDYtMTZUMTg6MTk6MTcrMDA6MDChxVOHAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTA2LTE2VDE4OjE5OjE3KzAwOjAw0JjrOwAAAXdJREFUOE+lkzGqwkAQhidBEaK2gpWIpakVvYSgR7CwEAQDgcTKAwhpxBvkLukERbHSShDR0sJqXv5x41vN8zV+8Du7k5nfDZM1OIa+AQZgNBpxPp/nQqHAxWLxRdlsFn/CmUxGalCbIAZIoOCTms0mu67LjUbjmUtMxACueoOu3W4nhYvFQuJ2u5U8eoAY4NjvjZDnebzf7yWCyWTCh8OBHceR50B+8Z7vzdB6vZZiHd/3eblcynNgxgusEF6o1Wpk2zbdbjeVeYDay+WidkQyRsMw1PYX3bTb7VK1WqXT6URhGFKlUpH1/X5X54iZz+dsWRa3Wi2Ookhl07TbbTk+Xhs8DXRmsxlfr1e1Y16tVtKk618DEB9b4ng8TjWnDJAIgkASCZ1Oh4fDYaoxEUYPxCCXy0lys9nwdDrlXq/H5/M51aQr+ZBkjIPBAIHq9Todj0cql8tUKpUk94l+v/9YiE0Mvm1MIR4pm6b556WC3i/Tl9eZ6Add4b/skypHSwAAAABJRU5ErkJggg==`;
const TWITTER_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAaBJREFUOI3Vkb9rU2EUhp/z3XubS9pUwYoNRaKDk6ODFBysODkJDi5qogVHQekfoAgu+g84aAImgUI3VxV0du6sWKNWEJq0eu/9fhynhKRNd32n7xy+9+E958B/oaWN/plqc3fjRKv/eNhbeP6zCiAAx1uDayW4utWo3NxvrrYGT2fS8pq6AkyE2uyLiOxkmb+/fffoGwNgPMvR7NyNpVb//X5AkiRrIf+NeovaDEx0UpXTUUkujBIoUOvmSgiot97DB6f64kfjSKfWLVRtNgJKFOOde/u1MX8ZwAAce7lXV1e8Uw0gEsVRvJImpXatm0+Yh3KKHb4NwOL58muTzl5CA6ii3qGuQG1+cKMmRtV0JgCbZ+VXMdh7pGiOyOHnEEFdXmzfmWtPAAB6q5WHAjuImW4GJJ7Be22P90a/BXBBOgRvp6WQKCHY4nPv9vzqVADAt0blgXPyhKBhPLYkKcHZT1v1yqkDYIDaqz8XfXDPolJ6DgzqChABY1Cb73ql2atX7k0da7xYbPaviMh1JCwEbzYljta/3yp/PHQp/4T+AgbtsdyJATuvAAAAAElFTkSuQmCC`;
const UGC_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAAAAAA6mKC9AAAAAXNCSVQI5gpbmQAAAExJREFUGJWtyrkNgDAQBdHhkLZmanAJNEcBliBzOATmdETAS/5qtMy8JBrRhi+iQIlVRRUQROoF/fmpda/QHTuSJbMJLN79Z4nhIaYdsukY6jT/2okAAAAASUVORK5CYII=`;
const VAC_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAIeSURBVDhPpVNLTxNhFD0zfScCZU1tbe2CaRt8JJLIAleVqDtJJJGkS9xIIrLpr8GfgRvEVTfy0OgaaKSdhoaWYrHvjvdcMoCPlZzJzdzvm++e79xzM0Y8EXdwDZgejwcM0zT/N84Tl2QwGGA4HGr0ej0Nd81wL3TDmLQmtQXHcfTwzMMZHFWPYBomErcT6HV7OCgeyAFgfDyMT1tb8Pv9Kp8wrJSlBCzOpDN4NDuLkZFRiRt6A2/tdDo4aTTQ/NHExuYGisUivF7vOUE6k1YCHnr25Cl8wh4KBuEPBOAVAkcIukLebrfRljM/z87w4eMmAvKdUA+IsbFRtFotnNTrsG0b9VoNpVIJhxK8vVqtonZ8rKpCoRAMw9AwLxJ5HkxPw7IsvF1dVZmP5+bwcnER+3t7eD4/j9fLy2gIWTAQvDSexUwced6vr+PFwgK2xSiqSKVSSCaTKJfLiMVieLW0hH6/D7siCkVps9mEMXVnymHCuH/3Ht6srODd2hqy2awa2zg9RSIeR6FQQC6XQz6fx5dvX+H3+bR1I3Iz4lAF538rGkM4HFaHWczR8hvfqlLeHLFdqeiauPCAG/syb86YOV1mcO3m3KeprvGEEhAuyfbujs6eKrgnB+ATud1uFzufdzV3awgjGov+9jMNRWZf5EcmIhITSva9dIiymMq+rxYTfxEQ7JWF9IWgMsr+s5i4bOYK3HbYv+vJv4oB4BfUc/nIFeL+dgAAAABJRU5ErkJggg==`;
const YANDEX_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAAA3NCSVQICAjb4U/gAAAAS1BMVEVHcEz9Phv9Pxz/QR78Ph39Lgv8LQv9PBn9PRr8Phz////8AQD8KgD8Ngb8GAD+y8j9opz+4uD8aFn+t7P9gXf+zsv/7+/+1NL8UDm2iGRRAAAACnRSTlMA6f///Fhaxc3s30eCWwAAAJFJREFUGJVlT9sWwyAIU9BaQVYvXbf//9LBtHtZHjwmhhicU8SUQ8hpcws7IigQ98m90sLMBdDPdwCurbWhino2441eg54PHYsuIUinwUyHCcllADmplilAdkHPQm05IEyh0vW+hWwV+KB+j2iohYxfaLRadGnG99vNikmvXUo9BWZ5j1JEbbKq/y+n9ef60e4flbMGxZ/yNYwAAAAASUVORK5CYII=`;
const YOUTUBE_ICON = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAPZJREFUOI3FkrFKA0EYhL89sngBkWDwWts8jN1VqW1TSF7CTgRb3yKkSvqAWFjYWqWJKC4Yi7u93I6FGAKu4YyCAwvLzyzzz8zCf8N8Xp7grGvMMdbum3bbYq3d4Imi8PJ+Fapq6aT5EVwC8AonAYJAPz0eTqnhZpfHAtVwlwgOo+b6/SYRZARYRBWmU8k5Kc+3bfBCDc9RwnisNWYzKctiGbwljbrqdKDX+zIWmO8tTCZSWUrD4TYLjhU8RAmDQZMmFtRw+4sa7xMPV41yiCDA9fore7gI0AUO9tI0xdrWR04AGLyvqrIsAiwTcDmcj+BxV/G/wzt9WBUFg/DKewAAAABJRU5ErkJggg==`;

class SteamIdDetail {
    constructor(steam64str) {
        if (!steam64str) {
            throw new Error("Missing first parameter 'steam64str'.");
        }

        const [upper32, lower32] = this._getBinary(steam64str);
        this._y = lower32 & 1;
        this._accountNumber = (lower32 & (((1 << 31) - 1) << 1)) >> 1;
        this._instance = (upper32 & ((1 << 20) - 1));
        this._type = (upper32 & (((1 << 4) - 1) << 20)) >> 20;
        this._universe = (upper32 & (((1 << 8) - 1) << 24)) >> 24;

        this._steamId64 = steam64str;
    }

    _divide(str) {
        const length = str.length;
        const result = [];
        let num = 0;
        for (let i = 0; i < length; i++) {
            num += Number(str[i]);

            const r = Math.floor(num / 2);
            num = ((num - (2 * r)) * 10);

            if (r !== 0 || result.length !== 0) {
                result.push(r);
            }
        }

        return [result, num > 0 ? 1 : 0];
    }

    _getBinary(str) {
        let upper32 = 0;
        let lower32 = 0;
        let index = 0;
        let bit = 0;
        let _str = str;
        do {
            [_str, bit] = this._divide(_str);

            if (bit) {
                if (index < 32) {
                    lower32 |= (1 << index);
                } else {
                    upper32 |= (1 << (index - 32));
                }
            }

            index++;
        } while (_str.length > 0);

        return [upper32, lower32];
    }

    get id2() {
        return `STEAM_${this._universe}:${this._y}:${this._accountNumber}`;
    }


    get id3() {
        const map = new Map(
            [
                [0, "I"], // invalid
                [1, "U"], // individual
                [2, "M"], // multiset
                [3, "G"], // game server
                [4, "A"], // anon game server
                [5, "P"], // pending
                [6, "C"], // content server
                [7, "g"], // clan
                [9, "a"] // anon user
            ]
        );

        let type = null;
        if (map.has(this._type)) {
            type = map.get(this._type);
        }

        if (!type) {
            return null;
        }

        return `[${type}:${this._universe}:${(this._accountNumber << 1) | this._y}]`;
    }

    get id64() {
        return this._steamId64;
    }
}

function addFlag(p_hexcolor, p_text) {
    let account_info = document.getElementById("account-info");
    let flag = document.createElement("div");
    flag.style.background = `${p_hexcolor}2b`;
    flag.style.color = `${p_hexcolor}`;
    flag.style.border = `1px solid ${p_hexcolor}`;
    flag.className = "account-flag";
    flag.innerText = p_text;
    account_info.insertBefore(flag, account_info.firstChild);
}

// Gets Steam profile information in XML format, also updates the status for Steam ID details
function getSteamInfo() {
    console.log(`[DEBUG] | Calling getSteamInfo()`);

    let account_infobox = document.getElementById("account-info");
    let account_loadinglabel = document.getElementById("account-loading-label");
    let account_loadingdescription = document.getElementById("account-loading-description");

    GM_xmlhttpRequest({
        method: "GET",
        url: `${document.location.href.split(/(#|\?)/)[0]}?xml=1`,
        timeout: 15000, // Time to wait (in milliseconds) for a response
        headers: {
            "Content-Type": "application/xml",
            "Accept": "application/xml"
        },
        onreadystatechange: function() {
            if (this.readyState === 1) {
                account_loadinglabel.innerText = "Connecting to Steam...";
            }
            else if (this.readyState === 3) {
                account_loadinglabel.innerText = "Loading Steam info...";
            }
            else if (this.readyState === 4) {
                if (this.status === 200) {
                    parseSteamInfo(this.responseText);
                } else if (this.status >= 400 && this.status <= 499) {
                    account_loadinglabel.innerText = `Client error (${this.status})`;
                    account_loadinglabel.className = "color-warning";
                    account_loadingdescription.innerText = "Steam's servers might be having issues or you are currently being rate limited."
                } else if (this.status >= 500 && this.status <= 599) {
                    account_loadinglabel.innerText = `Server error (${this.status})`;
                    account_loadinglabel.className = "color-warning";
                    account_loadingdescription.innerText = "Steam's servers might be having issues or undergoing maintenance."
                } else {
                    account_loadinglabel.innerText = `Unexpected status (${this.status})`;
                    account_loadinglabel.className = "color-error";
                    account_loadingdescription.innerHTML = `This should never happen unless you're debugging this script or using an application to modify server responses.`;
                }
            }
        },
        ontimeout: function() {
            account_loadinglabel.innerText = `Timed out`;
            account_loadinglabel.className = "color-warning";
            account_loadingdescription.innerText = `Steam did not respond quickly enough.`;
        },
        onerror: function() {
            account_loadinglabel.innerText = `Application error`;
            account_loadinglabel.className = "color-error";
            account_loadingdescription.innerText = `Please report this issue if it persists.`;
        }
    });
}

function parseSteamInfo(p_info) {
    console.log(`[DEBUG] | Calling parseSteamInfo()`);

    let account_infobox = document.getElementById("account-info");

    var dp = new DOMParser();
    var parsed_xml = dp.parseFromString(p_info, "text/xml");

    let use_fallback = false; // Used to determine whether or not we are forced to use alternatives to the XML API for some information

    let privacy_state = ""
    let member_since = "Unknown";
    let limited_account = false;
    let community_banned, tradeban_state, vac_banned;
    let profileban_text = document.querySelector(".profile_ban");

    if (parsed_xml.getElementsByTagName("steamID64").length > 0) {
        profile.id64 = parsed_xml.getElementsByTagName("steamID64")[0].childNodes[0].nodeValue;
    } else {
        profile.id64 = g_rgProfileData.steamid;
        console.log("%c[DEBUG] | Missing expected Steam data, using fallback mode.", "color: orange; font-size: 15px;");
        use_fallback = true;
    }

    if (steamdetective.forcedfallback_enabled) {
        console.log("%c[DEBUG] | Forced fallback option is enabled.", "color: #ff8efd; font-size: 15px;");
        use_fallback = true;
    }

    let sid = new SteamIdDetail(profile.id64);
    profile.id2 = sid.id2;
    profile.id3 = sid.id3;
    profile.custom_url = ""
    profile.username = username_label.innerText;

    if (parsed_xml.getElementsByTagName("privacyState").length > 0) {
        privacy_state = parsed_xml.getElementsByTagName("privacyState")[0].childNodes[0].nodeValue;
    }

    if (privacy_state === "public") {
        member_since = parsed_xml.getElementsByTagName("memberSince")[0].childNodes[0].nodeValue;
        profile.custom_url = parsed_xml.getElementsByTagName("customURL")[0].childNodes[0].nodeValue;
    } else {
        if (!use_fallback) {
            member_since = "Private";
        } else {
            member_since = "Unknown";
        }

        // Since the XML data is missing, this gets the custom URL from the address bar instead
        if (document.location.href.includes("/id/")) {
            profile.custom_url = document.location.href.split('/')[4];
        } else {
            profile.custom_url = "";
        }
    }

    if (parsed_xml.getElementsByTagName("vacBanned").length > 0) {
        vac_banned = parsed_xml.getElementsByTagName("vacBanned")[0].childNodes[0].nodeValue != 0;
        profile.is_vacbanned = vac_banned;
    }

    if (parsed_xml.getElementsByTagName("isLimitedAccount").length > 0) {
        limited_account = parsed_xml.getElementsByTagName("isLimitedAccount")[0].childNodes[0].nodeValue != 0;
    } else if (!use_fallback) {
        limited_account = true;
    }

    if (parsed_xml.getElementsByTagName("tradeBanState").length > 0) {
        tradeban_state = parsed_xml.getElementsByTagName("tradeBanState")[0].childNodes[0].nodeValue;

        // Workaround for trade bans/probations not showing on profiles that have them.
        if (profileban_text && tradeban_state === "None") {
            console.log("%c[DEBUG] | Detected Steam XML trade ban/probation bug, using workaround.", "color: orange; font-size: 15px;");
            if (profileban_text.innerText === "Currently trade banned") {
                tradeban_state = "Banned";
            } else if (profileban_text.innerText === "Currently on trade probation") {
                tradeban_state = "Probation";
            }
        }
    } else if (use_fallback && profileban_text) {
        if (profileban_text.innerText === "Currently trade banned") {
            tradeban_state = "Banned";
        } else if (profileban_text.innerText === "Currently on trade probation") {
            tradeban_state = "Probation";
        }
    } else {
        tradeban_state = "Unknown";
    }

    // Since there's no API for community bans that doesn't require an API key, we check for community ban page characteristics instead.
    // This has the limitation that community banned profiles that are private when banned are indistinguishable from normal private profiles.
    if (document.getElementsByClassName("profile_private_info").length > 0) {
        if (document.getElementsByClassName("profile_private_info")[0].innerText.length == 0 &&
            document.querySelectorAll(".private_profile .profile_header_bg_texture").length > 0) {
            community_banned = true;
        } else {
            community_banned = false;
        }
    }

    function checkJoinDate(p_membersince) {
        if (!use_fallback) {
            // If account is less than a year old, add the current year since Steam doesn't include it in that case.
            if (p_membersince.indexOf(", ") === -1 && p_membersince != "Private") {
                return `${p_membersince}, ${new Date().getFullYear()}`;
            } else if (p_membersince === "Private") {
                return `<span class='color-warning'>${p_membersince}</span>`;
            } else {
                return p_membersince;
            }
        } else {
            return "<span class='color-warning'>Unknown</span>";
        }
    }

    function detectDeceptiveURL(p_url) {
        if (profile.custom_url != profile.id64) {
            let fakeid64_regex = /^(profiles)?7[0-9]{3}/i;
            if (fakeid64_regex.test(profile.custom_url)) {
                p_url = `<span class="color-warning">${profile.custom_url}</span>`;
                profile.has_fakeid64 = true;
            } else {
                p_url = p_url.replace("I", `<span class="color-warning text-bold">I</span>`);
            }
        }

        return p_url;
    }

    function adjustURLStyling(p_url) {
        let classes = "";
        if (profile.custom_url === "") {
            classes += "font-default ";
        }
        if (p_url.length > 22) {
            classes += "url-long ";
        }
        if (p_url.length > 28) {
            classes += "url-max-long ";
        } else if (p_url.length > 23) {
            classes += "url-extra-long ";
        }
        return classes.trimEnd();
    }

    account_infobox.innerHTML =
        `
	${limited_account ? "<div id='limited-account-indicator'>Limited account</div>" : ""}
	${community_banned ? "<div id='community-ban-indicator'>Community banned</div>" : ""}
	${tradeban_state === "Banned" || tradeban_state === "Probation" ? "<div id='trade-ban-indicator'>Trade " + tradeban_state.toLowerCase() + "</div>" : ""}
    ${vac_banned ? "<div id='vac-ban-indicator'>VAC banned</div>" : ""}
	<table id="account-data-table">
		<tr><td class="account-label-cell corner-topleft-5px">ID2:</td><td class="account-data-cell corner-topright-5px">${profile.id2}</td></tr>
		<tr><td class="account-label-cell label-clickable" id="label-id3">ID3:</td><td class="account-data-cell" id="account-data-id3">${profile.id3}</td></tr>
		<tr><td class="account-label-cell label-clickable" id="label-id64">ID64:</td><td class="account-data-cell" id="account-data-id64">${profile.id64}</td></tr>
		<tr>
            <td class="account-label-cell label-clickable" id="label-url">URL:</td>
            <td class="account-data-cell ${adjustURLStyling(profile.custom_url)}" id="account-data-url">
                ${profile.custom_url === "" ? `<span class="color-warning">N/A</span>` : detectDeceptiveURL(profile.custom_url)}
            </td>
        </tr>
		<tr><td class="account-label-cell corner-bottomleft-5px">Join:</td><td class="account-data-cell corner-bottomright-5px">${checkJoinDate(member_since)}</td></tr>
	</table>
	<div id="acc-age-disclaimer" ${!steamdetective.agedisclaimer_enabled ? "class='hidden'" : ""}>Account age is not an indicator of trustworthiness on its own. <br> <a style="color:#fffc45" href="https://forums.steamrep.com/pages/obviousalts/" target="_blank" rel="noreferrer">Click here for more details</a></div>
    `;

    let label_id3 = document.getElementById("label-id3");
    let label_id64 = document.getElementById("label-id64");
    let label_url = document.getElementById("label-url");
    label_id3.onclick = () => copySteamDetail("id3");
    label_id64.onclick = () => copySteamDetail("id64");
    label_url.onclick = () => copySteamDetail("custom_url");

    if (profile.has_fakeid64) {
        addFlag("#ffa500", "Fake ID64 URL");
    }

    if (use_fallback && steamdetective.forcedfallback_enabled) {
        addFlag("#ff8efd", "Fallback mode (Debug forced)");
    } else if (use_fallback) {
        addFlag("#9eD0ff", "Fallback mode (missing XML data)");
    }

    getModuleInfo();
    addLinks();
}

function copySteamDetail(p_detail) {
    if (p_detail === "id3") {
        GM_setClipboard(`https://steamcommunity.com/profiles/${profile.id3}`);
        ShowAlertDialog('ID3 link copied', `<b>https://steamcommunity.com/profiles/${profile.id3}</b> has been copied to your clipboard.`);
    } else if (p_detail === "id64") {
        GM_setClipboard(`https://steamcommunity.com/profiles/${profile.id64}`);
        ShowAlertDialog('ID64 link copied', `<b>https://steamcommunity.com/profiles/${profile.id64}</b> has been copied to your clipboard.`);
    } else if (p_detail === "custom_url") {
        if (profile.custom_url) {
            GM_setClipboard(`https://steamcommunity.com/id/${profile.custom_url}`);
            ShowAlertDialog('Custom URL copied', `<b>https://steamcommunity.com/id/${profile.custom_url}</b> has been copied to your clipboard.`);
        } else {
            ShowAlertDialog('No custom URL', `<b>${g_rgProfileData.personaname}</b> has no custom URL. Nothing has been copied.`);
        }
    } else {
        ShowAlertDialog('Error', `An invalid detail was copied. Report the issue if you see this message.`);
    }
}

function getModuleInfo() {
    if (steamrep.is_enabled) {
        getSRInfo(profile.id64);
    }
    if (bptf_enabled) {
        getBPTFInfo(profile.id64, bptf_apikey);
    }
    if (mannco.is_enabled) {
        getManncoInfo(profile.id64);
    }
    if (reptf_enabled) {
        getExtraInfo(profile.id64);
    }
    if (skial_enabled) {
        checkSkialPresence(profile.id3);
    }
    if (vacbanned.is_enabled) {
        getVACBannedInfo(profile.id64);
    }
    if (steamhistory.is_enabled) {
        getSteamHistoryInfo(profile.id64);
    }
    if (steamiduk.is_enabled) {
        getSteamIdUkInfo(steamiduk.my_id, steamiduk.api_key, profile.id64);
    }
    if (steamtrades.is_enabled) {
        getSteamTradesInfo(profile.id64);
    }
}

function addLinks()
{
    let research_info = document.createElement("div");
    research_info.id = "research-info";

    research_info.innerHTML = `<hr class="splitter"> <h1 class="reputation-menu-title">Research</h1>`;
    research_info.append(buildResearchLink(ETOOLS_ICON, "eTools.ch (Multi-search)", "https://www.etools.ch/searchSubmit.do?query=", true, true, true, true, ['"', '"']));
    research_info.append(buildResearchLink(SEARX_ICON, "Searx.one (Multi-search)", "https://searx.one/search?q=", true, true, true, true, ['"', '"']));
    research_info.append(buildResearchLink(STEAMDB_ICON, "SteamDB.info", "https://steamdb.info/calculator/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMLADDER_ICON, "SteamLadder.com", "https://steamladder.com/profile/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMCOLLECTOR_ICON, "SteamCollector.com", "https://steamcollector.com/profiles/", false, false, true, false));
    research_info.append(buildResearchLink(STEAM_ICON, "SteamLevel.net", "https://steamlevel.net/profile/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMHISTORY_ICON, "SteamHistory.net", "https://steamhistory.net/id/", false, false, true, false));
    research_info.append(buildResearchLink(STEAMTRADES_ICON, "SteamTrades.com", "https://www.steamtrades.com/user/", false, false, true, false));
    research_info.append(buildResearchLink(INVENTORYGIFT_ICON, "Inventory.gift", "https://www.inventory.gift/user/", false, false, true, false));
    research_info.append(buildResearchLink(DISPENSER_ICON, "Dispenser.tf", "https://dispenser.tf/id/", false, false, true, false));
    research_info.append(buildResearchLink(VAC_ICON, "VACBanned.com", "http://vacbanned.com/view/detail/id/", false, false, true, false));
    research_info.append(buildResearchLink(UGC_ICON, "UGC League", "https://www.ugcleague.com/players_page.cfm?player_id=", false, false, true, false));
    research_info.append(buildResearchLink(RGL_ICON, "RGL.gg", "https://rgl.gg/Public/PlayerProfile.aspx?p=", false, false, true, false));
    research_info.append(buildResearchLink(ETF2L_ICON, "ETF2L", "https://etf2l.org/search/", false, false, true, false));
    research_info.append(buildResearchLink(FIREPOWERED_ICON, "FirePowered Bans", "https://firepoweredgaming.com/sourcebanspp/index.php?p=banlist&advSearch=", true, false, false, false, ["", "&advType=steamid"]));
    research_info.append(buildResearchLink(DEFAULT_ICON, "MVM Lobby", "https://mvmlobby.tf/profile/", false, false, true, false));
    research_info.append(document.createElement("br"));

    research_info.append(buildResearchLink(REDDIT_ICON, "Reddit", "https://old.reddit.com/search?q=", false, true, true, true));
    research_info.append(buildResearchLink(TWITTER_ICON, "Twitter", "https://twitter.com/search?q=", true, true, true, true, ["", "&f=live"]));
    research_info.append(buildResearchLink(YOUTUBE_ICON, "YouTube", "https://www.youtube.com/results?search_query=", true, true, true, true));

    research_info.innerHTML += '<hr class="splitter" style="margin-top: 25px;"><h1 class="reputation-menu-title">Archives</h1>';

    research_info.append(buildArchiveLink(ARCHIVEIS_ICON, "Archive.is", "https://archive.is/"));
    research_info.append(buildArchiveLink(ARCHIVEORG_ICON, "Wayback Machine", "https://web.archive.org/web/*/", ["","*"]));
    research_info.append(buildArchiveLink(GHOSTARCHIVE_ICON, "Ghost Archive", "https://ghostarchive.org/search?term="));
    research_info.append(buildArchiveLink(GOOGLE_ICON, "Google Web Cache", "https://webcache.googleusercontent.com/search?q=cache:"));
    research_info.append(buildArchiveLink(BING_ICON, "Bing Cache (Search)", "https://www.bing.com/search?q=url:"));
    research_info.append(buildArchiveLink(YANDEX_ICON,"Yandex Cache (Search)","https://www.yandex.com/search/?text="));

    document.getElementById("reputation-menu").appendChild(research_info);
}

function toggleDetails()
{
    let toggle_btn = document.getElementById("toggle-details-btn");
    let settings_btn = document.getElementById("settings-btn");

    let r = document.getElementById("reputation-menu");

    if (r.classList.value === "hidden") {
        toggle_btn.innerText = "Hide links and details";
        r.classList.remove("hidden");
    } else {
        toggle_btn.innerText = "View links and details";
        r.classList.add("hidden");
    }
}

function getSRInfo(p_id64) {
    if (steamrep.history_enabled) {
        let steamrep_container = document.getElementById(`${steamrep.id_prefix}-info-container`);
        let history_container = document.createElement("div");
        history_container.className = "history-table-container";
        history_container.id = `${steamrep.id_prefix}-history-container`;
        steamrep_container.appendChild(history_container);
    }

    function addSRLinks() {
        let content = document.getElementById(`${steamrep.id_prefix}-content`);
        content.innerHTML += `<br>
                              <a class="${steamrep.id_prefix}-research-link" href="https://steamrep.com/profiles/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SR_ICON});"></i>SteamRep profile page</a>
                              <a class="${steamrep.id_prefix}-research-link" href="https://forums.steamrep.com/search/search/?keywords=${p_id64}&o=date" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SR_ICON});"></i>SteamRep Forums search</a>`;
    }

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://steamrep.com/api/beta4/reputation/${p_id64}/?tagdetails=1&extended=1&json=1`,
        timeout: steamrep.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function() {
            if (this.readyState === 1) {
                updateRepContainer(steamrep, "Connecting");
                updateSummaryStatus(steamrep, "Connecting");
            }
            else if (this.readyState === 3) {
                updateRepContainer(steamrep, "Loading");
                updateSummaryStatus(steamrep, "Loading");
            }
            else if (this.readyState === 4) {
                if (this.status === 200) {
                    let json_response = JSON.parse(this.responseText);

                    function addSRTags() {
                        let tags = document.getElementById("sr-tags");
                        Object.values(json_response.steamrep.reputation.tags).forEach(val => {
                            if (val.length > 1) {
                                for (let i = 0; i < val.length; i++) {
                                    tags.innerHTML += (`${val[i].name} (${convertTimestamp(val[i].timestamp)}), `);
                                }
                            } else {
                                tags.innerHTML += (`${val.name} (${convertTimestamp(val.timestamp)})`);
                            }
                        });

                        tags.innerHTML = tags.innerHTML.replace(/[,]\s$/, '');
                    }

                    if (json_response.hasOwnProperty("steamrep")) {
                        // Scammer
                        if (json_response.steamrep.reputation.summary === "SCAMMER") {
                            updateRepContainer(steamrep, "Scammer");
                            updateSummaryStatus(steamrep, "Scammer");
                            addSRTags();
                            username_label.className = "actual_persona_name username-label-banned";
                        }
                        // Admin
                        else if (json_response.steamrep.reputation.summary === "ADMIN") {
                            updateRepContainer(steamrep, "Admin");
                            updateSummaryStatus(steamrep, "Admin");
                            addSRTags();
                            username_label.className = "actual_persona_name username-label-special";
                        }
                        // Caution
                        else if (json_response.steamrep.reputation.summary === "CAUTION") {
                            updateRepContainer(steamrep, "Caution");
                            updateSummaryStatus(steamrep, "Caution");
                            addSRTags();
                        }
                        // Normal
                        else if (json_response.steamrep.reputation.summary === "none") {
                            updateRepContainer(steamrep, "Normal");
                            updateSummaryStatus(steamrep, "Normal");
                        }

                        let content = document.getElementById(`${steamrep.id_prefix}-content`);

                        let report_count = parseInt(json_response.steamrep.stats.unconfirmedreports.reportcount);
                        if (report_count > 0) {
                            content.innerHTML += `<br><p class="${steamrep.id_prefix}-warning-label">${report_count} unconfirmed report(s) on the forums</p>`;
                        }
                        let srbanned_friends = parseInt(json_response.steamrep.stats.bannedfriends);
                        if (srbanned_friends > 0) {
                            content.innerHTML += `<br><p class="${steamrep.id_prefix}-warning-label">${srbanned_friends} SteamRep banned friend(s) - number may be inaccurate if friends are private</p>`;
                        }
                    }
                } else {
                    updateRepContainer(steamrep, this.status);
                    updateSummaryStatus(steamrep, this.status);
                }

                addSRLinks();
            }
        },
        ontimeout: function() {
            updateRepContainer(steamrep, "Timed out");
            updateSummaryStatus(steamrep, "Timed out");
            addSRLinks();
        },
        onerror: function() {
            updateRepContainer(steamrep, "Application error");
            updateSummaryStatus(steamrep, "Application error");
            addSRLinks();
        }
    });

    if (steamrep.history_enabled) {
        getSRHistory(p_id64)
    }
}

function getSRHistory(p_id64) {
    let table = document.createElement("table");
    table.id = `${steamrep.id_prefix}-history-table`;
    table.className = "history-table";

    let table_head = document.createElement("thead");
    table_head.innerHTML = `
         <tr>
            <th colspan="3" id="${steamrep.id_prefix}-table-head" class='history-table-head'>History</th>
         </tr>`;

    let table_body = document.createElement("tbody");
    table_body.id = `${steamrep.id_prefix}-table-body`;
    table.appendChild(table_head);
    table.appendChild(table_body);

    document.getElementById(`${steamrep.id_prefix}-history-container`).appendChild(table)

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://steamrep.com/util.php?op=getHistoryEntries&id=${p_id64}`,
        timeout: steamrep.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function() {
            if (this.readyState === 1) {
                document.getElementById(`${steamrep.id_prefix}-table-head`).innerText = "History - Connecting...";
            }
            else if (this.readyState === 3) {
                document.getElementById(`${steamrep.id_prefix}-table-head`).innerText = "History - Loading...";
            }
            if (this.readyState === 4) {
                if (this.status === 200) {
                    document.getElementById(`${steamrep.id_prefix}-table-head`).innerText = "History";
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(this.responseText,"text/html");
                    let entries = html_response.getElementsByTagName("option");
                    let opted_out = html_response.getElementsByClassName("listdisclaimer").length >= 1;

                    if (opted_out) {
                        table_head.innerHTML += `
                        <tr>
                            <th id="history-optedout" colspan="3">User has opted out of sharing historical data with the public.<br>Only the last 30 days are shown.</th>
                        </tr>`
                    }
                    if (entries.length > 0) {
                        table_body.innerHTML += "<tr style='background:#001c8a;position:sticky;top:0;'><td>Date seen</td><td>Name</td><td>Approx. Friends/SR Banned Friends</td></tr>";

                        for (let i = 0; i < entries.length; i++) {
                            let data = entries[i].firstChild.nodeValue;

                            let table_row = document.createElement("tr");
                            let date_cell = document.createElement("td");
                            let name_cell = document.createElement("td");
                            let friendcount_cell = document.createElement("td");

                            date_cell.innerText = data.match(/\d{4}-\d{2}-\d{2} \d{2}:\d{2}/)[0];
                            try {
                                name_cell.innerText = data.match(/(?<![0-9])\ .+\ (?![0-9])/)[0];
                            } catch {
                                name_cell.innerHTML = "<i>Warning: Empty/Unexpected format</i>";
                                name_cell.style.background = "#ff4800";
                                console.log(`[DEBUG] | SR History: Error regex matching (name) in entry #${i}: ${entries[i].firstChild.nodeValue} , leaving cell empty`);
                            }
                            friendcount_cell.innerText = data.match(/[0-9]+\/[0-9]+$/);

                            table_row.appendChild(date_cell);
                            table_row.appendChild(name_cell);
                            table_row.appendChild(friendcount_cell);
                            table_body.appendChild(table_row);
                        }
                    } else {
                        let table_row = document.createElement("tr")
                        let table_cell = document.createElement("td");
                        if (!opted_out) {
                            table_cell.innerHTML = "No SR history found.<br>The account was freshly made or has never actively traded on Steam before.";
                        } else {
                            table_cell.innerHTML = "No public SR history found.<br>The account has not actively traded on Steam in the past 30 days.";
                        }

                        table_cell.id = "nohistory-cell";
                        table_cell.colSpan = 3;
                        table_row.appendChild(table_cell);
                        table_body.appendChild(table_row);
                    }
                } else {
                    buildTableStatus(steamrep, this.status, "History")
                }
            }
        },
        ontimeout: function() {
            buildTableStatus(steamrep, "Timed out", "History")
        },
        onerror: function() {
            buildTableStatus(steamrep, "Application error", "History")
        }
    });
}

function getBPTFInfo(p_id64, apikey)
{
    // Title
    var title = document.createElement("div");
    title.className = "status-title-container";
    bptf_info.appendChild(title);

    // Content (Information)
    var content = document.createElement("div");
    content.className = "status-info-container";
    content.id = "bp-info-container";
    bptf_info.appendChild(content);

    if (apikey != "")
    {
        GM_xmlhttpRequest({
            method: "GET",
            url: `https://api.backpack.tf/api/users/info/v1?steamids=${p_id64}&key=${apikey}`,
            timeout: bptf_apitimeout, // Time to wait (in milliseconds) for a response
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            onreadystatechange: function() {
                console.log("[DEBUG] | Backpack.tf XMLHttpRequest readyState = " + this.readyState + ", status=" + this.status);
                if (this.readyState == 1) {
                    bptf_info.className = "ban-status-light";

                    title.innerHTML = `<h2 class='status-title title-light'>Backpack.tf - Connecting</h2>`;
                    content.innerHTML = `<p style='display:inline;'>Attempting to connect to Backpack.tf, this can occasionally take a while.</p>`;

                    bptf_sumstatus.className = "summary-status summary-status-light";
                    bptf_sumstatus.innerText = "Connecting";
                }
                else if (this.readyState == 3) {
                    bptf_info.className = "ban-status-light";

                    title.innerHTML = `<h2 class='status-title title-light'>Backpack.tf - Loading</h2>`;
                    content.innerHTML = `<p style='display:inline;'>Loading information, this can occasionally take a while.</p>`;

                    bptf_sumstatus.className = "summary-status summary-status-light";
                    bptf_sumstatus.innerText = "Loading";
                }
                else if (this.readyState == 4 && this.status == 200) {
                    var json_response = JSON.parse(this.responseText);
                    var last_visit, donated, premium, inv_labeltext, inv_label2text = "", inv_value, inv_ranking, inv_rawkeys, inv_rawmetal, inv_usedslots, inv_totalslots;

                    bptf_info.className = "ban-status-normal";
                    title.innerHTML = `<h2 class='status-title title-normal'>Backpack.tf - Normal</h2>`;
                    content.innerHTML = "";

                    Object.values(json_response.users).forEach(val => {
                        console.log(val);

                        if (val.hasOwnProperty("last_online")) {
                            last_visit = convertTimestampPrecise(val.last_online);
                        } else {
                            bptf_info.className = "ban-status-noaccount";
                            title.innerHTML = "<h2 class='status-title title-light'>Backpack.tf - No account</h2>";
                        }

                        if (val.hasOwnProperty("premium")) {
                            premium = "Yes";
                        } else {
                            premium = "No";
                        }

                        if (val.hasOwnProperty("donated")) {
                            donated = val.donated.toFixed(2) + "$";
                        } else {
                            donated = "0$";
                        }

                        if (val.hasOwnProperty("inventory")) {
                            if (val.inventory.hasOwnProperty("440")) {
                                inv_value = val.inventory["440"].value;
                                inv_ranking = typeof val.inventory["440"].ranking != 'undefined' ? "#" + val.inventory["440"].ranking : "N/A";
                                inv_rawkeys = val.inventory["440"].keys;
                                inv_rawmetal = val.inventory["440"].metal;
                                if (val.inventory["440"].hasOwnProperty("slots")) {
                                    inv_usedslots = val.inventory["440"].slots.used;
                                    inv_totalslots = val.inventory["440"].slots.total;
                                } else {
                                    inv_usedslots = 0;
                                    inv_totalslots = 0;
                                }

                                inv_labeltext = `<div class='bp-detail-container'><p style='display:inline;'><b>Inventory:</b> Worth ${inv_value !== undefined ? inv_value : 0} refined (updated @ ${val.inventory["440"].updated !== undefined ? convertTimestampPrecise(val.inventory["440"].updated) : "N/A"})</p></div>`;
                                inv_label2text = `<div class='bp-detail-container'><p style='display:inline;'><b>BP Rank:</b> ${inv_ranking}</p> | <b>Slots:</b> ${inv_usedslots}/${inv_totalslots} | <b>Pure keys:</b> ${inv_rawkeys} | <b>Pure refined:</b> ${inv_rawmetal}</div>`;
                            } else {
                                inv_labeltext = `<div class='bp-detail-container'><p style='display:inline;'><b>Inventory:</b> <span style='color:orange;'>No public TF2 inventory / never updated</span></p></div>`;
                            }
                        } else {
                            inv_labeltext = `<div class='bp-detail-container'><p style='display:inline;'><b>Inventory:</b> <span style='color:orange;'>No public TF2 inventory / never updated</span></p></div>`;
                        }

                        content.innerHTML += `
                                           <div class='bp-detail-container'><p style='display:inline;'><b>Last visited:</b> ${last_visit ? last_visit : "<span style='color:orange;'>User has never visited backpack.tf before</span>"}</p></div>
                                           <div class='bp-detail-container ${premium === "Yes" ? "detail-premium" : ""}'><p style='display:inline;'><b>Premium:</b> ${premium}</p></div>
                                           <div class='bp-detail-container'><p style='display:inline;'><b>Donated:</b> ${donated}</p></div>
                                           `;
                        content.innerHTML += inv_labeltext;
                        content.innerHTML += inv_label2text;

                        if (val.hasOwnProperty("trust")) {
                            let positive = 0, negative = 0, pos_percent, neg_percent;

                            if (val.trust.hasOwnProperty("positive")) {
                                positive = val.trust.positive;
                            }
                            if (val.trust.hasOwnProperty("negative")) {
                                negative = val.trust.negative;
                            }

                            bptf_sumpostrust.innerText = `+ ${positive}`;
                            bptf_sumnegtrust.innerText = `- ${negative}`;
                            bptf_trust_summary.classList.remove("hidden");

                            pos_percent = positive / (positive + negative) * 100;
                            neg_percent = negative / (positive + negative) * 100;

                            content.innerHTML += `<div class='bp-detail-container' id='trust-detail-container'><p style='display:inline;'><b>Trust:</b> +${positive} (${pos_percent.toFixed(2)}%) / -${negative} (${neg_percent.toFixed(2)}%)</p></div>`;

                            if (pos_percent > 0 && neg_percent == 0) { // Only positive trusts
                                document.getElementById('trust-detail-container').classList.add('detail-positive');
                            } else if (pos_percent > 0 || neg_percent > 0) { // Mixed/negative trusts
                                if (pos_percent >= neg_percent) {
                                    document.getElementById('trust-detail-container').classList.add('detail-mixed'); // Mixed (more or equal positive trusts compared to negative trusts)
                                } else {
                                    document.getElementById('trust-detail-container').classList.add('detail-negative'); // Negative (more negative trusts than positive)
                                }
                            }
                        } else {
                            content.innerHTML += `<div class='bp-detail-container' id='trust-detail-container'><p style='display:inline;'><b>Trust:</b> +0 (0.00%) / -0 (0.00%)</p></div>`;
                            bptf_sumpostrust.innerText = "+ 0";
                            bptf_sumnegtrust.innerText = "- 0";
                            bptf_trust_summary.classList.remove("hidden");
                        }

                        if (val.hasOwnProperty("bans")) {
                            // Contains the bans we want to make visible on the page
                            var importantban_list = ["steamrep_scammer", "all", "all features", "price suggestions", "suggestion comments", "trust", "issue tracker", "classifieds", "profile customizations", "reports", "item search"];
                            var hasimportant_ban = false;

                            bans_found:
                            for (let b in val.bans) {
                                for (let i = 0; i < importantban_list.length; i++) {
                                    if (b === importantban_list[i]) {
                                        hasimportant_ban = true; // If found
                                        break bans_found;
                                    }
                                }
                            }

                            if (hasimportant_ban) {
                                content.innerHTML += `<div class='bp-detail-container'><p id='bp-bans' style='display:inline;'></p><b>Current site bans:</b><br><ul id='bp-ban-list'></ul></div>`;
                                let ban_list = document.getElementById('bp-ban-list');

                                for (let key in val.bans) {
                                    if (key === "steamrep_scammer") {
                                        ban_list.innerHTML += `<li style='margin-left: -20px;'>All Features (SR ban):<ul><li>Automatically banned from all features because of ban on SteamRep</li></ul></li>`;
                                    }
                                    if (key === "price suggestions" || key === "suggestion comments" || key === "trust" || key === "issue tracker" ||
                                        key === "classifieds" || key === "profile customizations" || key === "reports" || key === "item search") {
                                        let ban_end = val.bans[key].end != -1 ? `(Ends @ ${convertTimestampPrecise(val.bans[key].end)})` : `(Permanent)`;
                                        ban_list.innerHTML += `<li style='margin-left: -20px;'>${key.charAt(0).toUpperCase() + key.slice(1)}:<ul><li>${val.bans[key].reason} ${ban_end}</li></ul></li>`;
                                    }
                                    else if (key === "all" || key === "all features") {
                                        let ban_end = val.bans[key].end != -1 ? `(Ends @ ${convertTimestampPrecise(val.bans[key].end)})` : `(Permanent)`;
                                        ban_list.innerHTML += `<li style='margin-left: -20px;'>All Features:<ul><li>${val.bans[key].reason} ${ban_end}</li></ul></li>`;
                                    }
                                }

                                if (val.bans.hasOwnProperty("price suggestions")
                                    || val.bans.hasOwnProperty("suggestion comments")
                                    || val.bans.hasOwnProperty("trust")
                                    || val.bans.hasOwnProperty("issue tracker")
                                    || val.bans.hasOwnProperty("classifieds")
                                    || val.bans.hasOwnProperty("profile customizations")
                                    || val.bans.hasOwnProperty("reports")
                                    || val.bans.hasOwnProperty("item search"))
                                {
                                    title.innerHTML = `<h2 class='status-title title-warning'>Backpack.tf - Feature Banned</h2>`;
                                    bptf_info.className = "ban-status-warning";
                                    bptf_sumstatus.className = "summary-status summary-status-warning";
                                    bptf_sumstatus.innerText = "Feature Banned";
                                }

                                if (val.bans.hasOwnProperty("steamrep_scammer") || val.bans.hasOwnProperty("all") || val.bans.hasOwnProperty("all features")) {
                                    title.innerHTML = `<h2 class='status-title title-danger'>Backpack.tf - Banned</h2>`;
                                    bptf_info.className = "ban-status-danger";
                                    username_label.style.textDecoration = "line-through";
                                    username_label.style.color = "red";
                                    bptf_sumstatus.className = "summary-status summary-status-danger";
                                    bptf_sumstatus.innerText = "Banned";
                                }
                            } else if (!last_visit) {
                                bptf_sumstatus.className = "summary-status summary-status-noaccount";
                                bptf_sumstatus.innerText = "No account";
                            } else {
                                bptf_sumstatus.className = "summary-status summary-status-normal";
                                bptf_sumstatus.innerText = "Normal";
                            }
                        } else if (!last_visit) {
                            bptf_sumstatus.className = "summary-status summary-status-noaccount";
                            bptf_sumstatus.innerText = "No account";
                        } else {
                            bptf_sumstatus.className = "summary-status summary-status-normal";
                            bptf_sumstatus.innerText = "Normal";
                        }
                    })
                    addBPLinks();
                }
                else if (this.readyState == 4 && this.status == 400) {
                    bptf_info.className = "ban-status-warning";
                    title.innerHTML = `<h2 class='status-title title-warning'>Backpack.tf - Bad Request</h2>`;
                    content.innerHTML = `<p style='display:inline;'>Error 400, request was malformed/incorrect. Report this issue if it persists.</p><br>`;
                    bptf_sumstatus.className = "summary-status summary-status-warning";
                    bptf_sumstatus.innerText = "Error 400";
                    addBPLinks();
                }
                else if (this.readyState == 4 && this.status == 403) {
                    bptf_info.className = "ban-status-warning";
                    title.innerHTML = `<h2 class='status-title title-warning'>Backpack.tf - Invalid API Key</h2>`;
                    content.innerHTML = `<p style='display:inline;'>Error 403, this is likely due to an invalid API key.</p><br>`;
                    bptf_sumstatus.className = "summary-status summary-status-warning";
                    bptf_sumstatus.innerText = "Error 403";
                    addBPLinks();
                }
                else if (this.readyState == 4 && this.status == 500) {
                    bptf_info.className = "ban-status-warning";
                    title.innerHTML = `<h2 class='status-title title-warning'>Backpack.tf - Internal Server Error</h2>`;
                    content.innerHTML = `<p style='display:inline;'>Error 500, Backpack.tf is currently having server issues.</p><br>`;
                    bptf_sumstatus.className = "summary-status summary-status-warning";
                    bptf_sumstatus.innerText = "Error 500";
                    addBPLinks();
                }
                else if (this.readyState == 4 && this.status == 502) {
                    bptf_info.className = "ban-status-warning";
                    title.innerHTML = `<h2 class='status-title title-warning'>Backpack.tf - Bad Gateway</h2>`;
                    content.innerHTML = `<p style='display:inline;'>Error 502, Backpack.tf is currently having server issues.</p><br>`;
                    bptf_sumstatus.className = "summary-status summary-status-warning";
                    bptf_sumstatus.innerText = "Error 502";
                    addBPLinks();
                }
                else if (this.readyState == 4 && this.status == 503) {
                    bptf_info.className = "ban-status-warning";
                    title.innerHTML = `<h2 class='status-title title-warning'>Backpack.tf - Service Unavailable</h2>`;
                    content.innerHTML = `<p style='display:inline;'>Error 503, Backpack.tf is currently having server issues.</p><br>`;
                    bptf_sumstatus.className = "summary-status summary-status-warning";
                    bptf_sumstatus.innerText = "Error 503";
                    addBPLinks();
                }
            },
            ontimeout: function() {
                bptf_info.className = "ban-status-warning";
                title.innerHTML = `<h2 class='status-title title-warning'>Backpack.tf - Timed out</h2>`;
                content.innerHTML = `<p style='display:inline;'>Backpack.tf did not respond quickly enough.</p><br>`;
                bptf_sumstatus.className = "summary-status summary-status-warning";
                bptf_sumstatus.innerText = "Timed out";
                addBPLinks();
            }

        });
    }
    else // When no backpack.tf API key is set
    {
        bptf_info.className = "ban-status-light";

        title.innerHTML = `<h2 class='status-title title-light'>Backpack.tf - API Key Required</h2>`;
        content.innerHTML = `<p style='display:inline;'>You will need to set the API key in the settings menu to view Backpack.tf information such as bans and trust without going on the website.<br>Your API key can be requested here:
                             <a style="font-weight:bold;" href="https://backpack.tf/developer/apikey/view" target="_blank" rel="noreferrer">https://backpack.tf/developer/apikey/view</a></p><br>
                             <p>Do not share your API key with anyone, as it can be used to perform malicious actions under your identity.</p>`;
        bptf_sumstatus.className = "summary-status summary-status-light";
        bptf_sumstatus.innerText = "API Key Required";
        addBPLinks();
    }
}

function addBPLinks()
{
    let content = document.getElementById("bp-info-container");
    content.innerHTML += `
                          <a class="bp-research-link" href="https://backpack.tf/u/${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Profile</a>
                          <a class="bp-research-link" href="https://backpack.tf/profiles/${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Backpack</a>
                          <a class="bp-research-link" href="https://forums.backpack.tf/steam.php?steamid=${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Forums</a>
                          <a class="bp-research-link bp-trust-btn" href="https://backpack.tf/trust/${profile.id64}?mode=for" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Trust (received)</a>
                          <a class="bp-research-link bp-trust-btn" href="https://backpack.tf/trust/${profile.id64}?mode=from" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Trust (given)</a>
                          <a class="bp-research-link bp-issue-btn" href="https://backpack.tf/issues?text=${profile.id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${BPTF_ICON});"></i>Issues (recieved)</a>
                          `;
}

function getManncoInfo(p_id64)
{
    let content = document.getElementById(`${mannco.id_prefix}-content`);
    let links_html = `<br><a class="mannco-research-link" href="https://mannco.store/store/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${MANNCO_ICON});"></i>Store</a>`;

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://mannco.store/API/userBanSteamDetective.php?id=${p_id64}`,
        timeout: mannco.api_timeout,
        onreadystatechange: function() {
            if (this.readyState === 1) {
                updateRepContainer(mannco, "Connecting");
                updateSummaryStatus(mannco, "Connecting");
            }
            else if (this.readyState === 3) {
                updateRepContainer(mannco, "Loading");
                updateSummaryStatus(mannco, "Loading");
            }
            else if (this.readyState === 4) {
                if (this.status === 200) {
                    var raw_response = this.responseText;
                    // Replace parts of the response text to be JSON parsable
                    raw_response = raw_response.replace('ok:', '"ok":');
                    raw_response = raw_response.replace('result:', '"result":');
                    raw_response = raw_response.replace('reason:', '"reason":');

                    var json_response = JSON.parse(raw_response);
                    if (json_response.hasOwnProperty("ok")) {
                        if (json_response.result === "banned") {
                            updateRepContainer(mannco, "Banned");

                            if (json_response.reason.startsWith("Concealing or being part of a community")) {
                                updateSummaryStatus(mannco, "Scrap/MP/BP Staff");
                                content.innerHTML =
                                `
                                <div id="mannco-massban-disclaimer">
                                    <p style="display:inline;"><b>DISCLAIMER: </b>On March 31st, 2023, Mannco.store has blanket banned every staff member on Scrap.tf, Backpack.tf and Marketplace.tf with this deceptively vague and inaccurate ban reason due to conflicts between the site owners of ScrapTF and Mannco.store.</p>
                                </div>
                                ${content.innerHTML}
                                `;
                            } else {
                                updateSummaryStatus(mannco, "Banned");
                            }
                            document.getElementById("mannco-banreason").innerText = json_response.reason;
                            content.innerHTML += links_html;
                        } else if (json_response.result === "ok") {
                            updateRepContainer(mannco, "Normal");
                            updateSummaryStatus(mannco, "Normal");
                            content.innerHTML += links_html;
                        } else if (json_response.result === "NF") {
                            updateRepContainer(mannco, "No account");
                            updateSummaryStatus(mannco, "No account");
                        }
                    }
                } else {
                    updateRepContainer(mannco, this.status);
                    updateSummaryStatus(mannco, this.status);
                    content.innerHTML += links_html;
                }
            }
        },
        ontimeout: function() {
            updateRepContainer(mannco, "Timed out");
            updateSummaryStatus(mannco, "Timed out");
            content.innerHTML += links_html;
        },
        onerror: function() {
            updateRepContainer(mannco, "Application error");
            updateSummaryStatus(mannco, "Application error");
            content.innerHTML += links_html;
        }
    });
}

// Fetches Marketplace and Scrap.tf info from rep.tf
function getExtraInfo(id64)
{
    var mpban_info = document.getElementById("mp-information");
    var scrapban_info = document.getElementById("scrap-information");

    // Title
    var mp_title = document.createElement("div");
    var scrap_title = document.createElement("div");
    mp_title.className = "status-title-container";
    scrap_title.className = "status-title-container";
    mpban_info.appendChild(mp_title);
    scrapban_info.appendChild(scrap_title);

    // Content (Information)
    var mp_content = document.createElement("div");
    var scrap_content = document.createElement("div");
    mp_content.className = "status-info-container";
    scrap_content.className = "status-info-container";
    mpban_info.appendChild(mp_content);
    scrapban_info.appendChild(scrap_content);

    GM_xmlhttpRequest({
        method: "POST",
        url: `https://rep.tf/api/bans?str=${id64}`,
        timeout: reptf_apitimeout, // Time to wait (in milliseconds) for a response
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        onreadystatechange: function() {
            console.log("[DEBUG] | Rep.tf XMLHttpRequest readyState = " + this.readyState + ", status=" + this.status);
            if (this.readyState == 1) {
                mp_title.innerHTML = "<h2 class='status-title title-light'>Marketplace.tf - Connecting</h2>";
                mp_content.innerHTML = "<p style='display:inline;'>Attempting to connect to Rep.tf, this can occasionally take a while.</p>";
                mpban_info.className = "ban-status-light";
                scrap_title.innerHTML = "<h2 class='status-title title-light'>Scrap.tf - Connecting</h2>";
                scrap_content.innerHTML = "<p style='display:inline;'>Attempting to connect to Rep.tf, this can occasionally take a while.</p>";
                scrapban_info.className = "ban-status-light";
                mptf_sumstatus.className = "summary-status summary-status-light";
                mptf_sumstatus.innerText = "Connecting";
                scrap_sumstatus.className = "summary-status summary-status-light";
                scrap_sumstatus.innerText = "Connecting";
            }
            else if (this.readyState == 3) {
                mp_title.innerHTML = "<h2 class='status-title title-light'>Marketplace.tf - Loading</h2>";
                mp_content.innerHTML = "<p style='display:inline;'>Loading information, this can occasionally take a while.</p>";
                mpban_info.className = "ban-status-light";
                mptf_sumstatus.className = "summary-status summary-status-light";
                mptf_sumstatus.innerText = "Loading";
                scrap_title.innerHTML = "<h2 class='status-title title-light'>Scrap.tf - Loading</h2>";
                scrap_content.innerHTML = "<p style='display:inline;'>Loading information, this can occasionally take a while.</p>";
                scrapban_info.className = "ban-status-light";
                scrap_sumstatus.className = "summary-status summary-status-light";
                scrap_sumstatus.innerText = "Loading";
            }
            else if (this.readyState == 4 && this.status == 200) {
                var json_response = JSON.parse(this.responseText);

                // Marketplace.tf
                if (json_response.mpBans.banned === "good") {
                    mpban_info.className = "ban-status-normal";
                    mp_title.innerHTML = "<h2 class='status-title title-normal'>Marketplace.tf - Normal</h2>";
                    mp_content.innerHTML = "<p style='display:inline;'>Normal - No special reputation</p>";
                    mptf_sumstatus.className = "summary-status summary-status-normal";
                    mptf_sumstatus.innerText = "Normal";
                } else if (json_response.mpBans.banned === "bad") {
                    mpban_info.className = "ban-status-danger";
                    mp_title.innerHTML = "<h2 class='status-title title-danger'>Marketplace.tf - Banned</h2>";
                    mp_content.innerHTML = `<p style='display:inline;'><b>Reason:</b> ${json_response.mpBans.message}</p>`;
                    mptf_sumstatus.className = "summary-status summary-status-danger";
                    mptf_sumstatus.innerText = "Banned";
                } else if (json_response.mpBans.message.indexOf("Failed to get data") != -1) {
                    mpban_info.className = "ban-status-warning";
                    mp_title.innerHTML = "<h2 class='status-title title-warning'>Marketplace.tf - Failed to get data</h2>";
                    mp_content.innerHTML = "<p style='display:inline;'>Rep.tf cannot get data for this profile at the moment.</p>";
                    mptf_sumstatus.className = "summary-status summary-status-warning";
                    mptf_sumstatus.innerText = "Rep.tf get data fail";
                }
                mp_content.innerHTML += `<br><a class="mptf-research-link" href="https://marketplace.tf/shop/${id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${MPTF_ICON});"></i>Shop</a>`;

                // Scrap.tf
                if (json_response.stfBans.banned === "good" && json_response.stfBans.message.indexOf("Member since") != -1) {
                    scrapban_info.className = "ban-status-normal";
                    scrap_title.innerHTML = "<h2 class='status-title title-normal'>Scrap.tf - Normal</h2>";
                    scrap_content.innerHTML = `<p style='display:inline;'>${json_response.stfBans.message.replace('No active bans.<br/>','')}</p><br>`;
                    scrap_content.innerHTML += `<a class="scrap-research-link" href="https://scrap.tf/profile/${id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SCRAP_ICON});"></i>Profile</a>`;
                    scrap_sumstatus.className = "summary-status summary-status-normal";
                    scrap_sumstatus.innerText = "Normal";
                } else if (json_response.stfBans.banned === "good" && json_response.stfBans.message.indexOf("Member since") === -1) {
                    scrapban_info.className = "ban-status-noaccount";
                    scrap_title.innerHTML = "<h2 class='status-title title-light'>Scrap.tf - No account</h2>";
                    scrap_content.innerHTML = "<p style='display:inline;'>Not found - User has no account on the website</p>";
                    scrap_sumtitle.onclick = function() {alert("User has no account on the website.")};
                    scrap_sumstatus.className = "summary-status summary-status-noaccount";
                    scrap_sumstatus.innerText = "No account";
                } else if (json_response.stfBans.banned === "bad") {
                    scrapban_info.className = "ban-status-danger";
                    scrap_title.innerHTML = "<h2 class='status-title title-danger'>Scrap.tf - Banned</h2>";
                    scrap_content.innerHTML = `<p style='display:inline;'>${json_response.stfBans.message.replace(/<\/?[^>]+>/gi, '').replace('Member', '<br>Member')}</p><br>`;
                    if (scrap_content.innerHTML.includes("Currently community banned for:")) {
                        scrap_content.innerHTML = scrap_content.innerHTML.replace("Currently site banned for:", "<br>Currently site banned for:");
                    }
                    scrap_sumstatus.className = "summary-status summary-status-danger";
                    scrap_sumstatus.innerText = "Banned";

                    if (json_response.stfBans.message.indexOf("Member since") != -1) {
                        scrap_content.innerHTML += `<a class="scrap-research-link" href="https://scrap.tf/profile/${id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SCRAP_ICON});"></i>Profile</a>`;
                        scrap_sumtitle.onclick = function() {alert("User has no account on the website.")};
                    }
                } else if (json_response.stfBans.message.indexOf("Failed to get data") != -1) {
                    scrapban_info.className = "ban-status-warning";
                    scrap_title.innerHTML = "<h2 class='status-title title-warning'>Scrap.tf - Failed to get data</h2>";
                    scrap_content.innerHTML = "<p style='display:inline;'>Rep.tf cannot get data for this profile at the moment.</p>";
                    scrap_sumstatus.className = "summary-status summary-status-warning";
                    scrap_sumstatus.innerText = "Rep.tf get data fail";
                }
            }
        },
        ontimeout: function() {
            mp_title.innerHTML = "<h2 class='status-title title-warning'>Marketplace.tf - Timed out</h2>";
            mp_content.innerHTML = "<p style='display:inline;'>Rep.tf did not respond quickly enough.</p>";
            mpban_info.className = "ban-status-warning";
            mp_content.innerHTML += `<br><a class="mptf-research-link" href="https://marketplace.tf/shop/${id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${MPTF_ICON});"></i>Shop</a>`;
            mptf_sumstatus.className = "summary-status summary-status-warning";
            mptf_sumstatus.innerText = "Timed out";
            scrap_title.innerHTML = "<h2 class='status-title title-warning'>Scrap.tf - Timed out</h2>";
            scrap_content.innerHTML = "<p style='display:inline;'>Rep.tf did not respond quickly enough.</p>";
            scrap_content.innerHTML += `<br><a class="scrap-research-link" href="https://scrap.tf/profile/${id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SCRAP_ICON});"></i>Profile</a>`;
            scrapban_info.className = "ban-status-warning";
            scrap_sumstatus.className = "summary-status summary-status-warning";
            scrap_sumstatus.innerText = "Timed out";
        }
    });
}

function checkSkialPresence(p_id3) {
    var stats_info = document.getElementById("skial-information");

    // Title
    var title = document.createElement("div");
    title.className = "status-title-container";
    stats_info.appendChild(title);

    // Content (Information)
    var content = document.createElement("div");
    content.className = "status-info-container";
    stats_info.appendChild(content);

    let id = p_id3.replace(/\[|\]|(U:1:)/g, "");

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://stats.skial.com/api?type=search_player&page=0&id=${id}`,
        timeout: skial_apitimeout, // Time to wait (in milliseconds) for a response
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        onreadystatechange: function() {
            console.log("[DEBUG] | Skial XMLHttpRequest readyState = " + this.readyState + ", status=" + this.status);

            if (this.readyState == 1) {
                stats_info.className = "ban-status-light";
                title.innerHTML = `<h2 class='status-title title-light'>Skial - Connecting</h2>`;
                content.innerHTML = `<p style='display:inline;'>Attempting to connect to Skial, this can occasionally take a while.</p>`;
                skial_sumstatus.className = "summary-status summary-status-light";
                skial_sumstatus.innerText = "Connecting";
            }
            else if (this.readyState == 3) {
                stats_info.className = "ban-status-light";
                title.innerHTML = `<h2 class='status-title title-light'>Skial - Loading</h2>`;
                content.innerHTML = `<p style='display:inline;'>Loading information, this can occasionally take a while.</p>`;
                skial_sumstatus.className = "summary-status summary-status-light";
                skial_sumstatus.innerText = "Loading";
            }
            else if (this.readyState == 4 && this.status == 200) {
                var json_response = JSON.parse(this.responseText);

                if (json_response.hasOwnProperty("Results")) {
                    var results = json_response.Results;

                    if (results.length > 0) {
                        title.innerHTML = `<h2 class='status-title title-normal'>Skial - Normal</h2>`;
                        content.innerHTML = `<p style='display:inline;'>The user has played on Skial servers before.<br>Current bans are not displayed if present for the moment.</p>`;
                        stats_info.className = "ban-status-normal";
                        skial_sumstatus.className = "summary-status summary-status-normal";
                        skial_sumstatus.innerText = "Normal";

                        var skial_servers = document.createElement("div");
                        skial_servers.id = "skial-servers";
                        stats_info.appendChild(skial_servers);

                        let table = document.createElement("table");
                        table.id = "skial-servers-table";
                        table.className = "history-table";

                        let table_head = document.createElement("thead");
                        table_head.innerHTML = `
                                               <tr>
                                                   <th colspan="3" class="history-table-head" id="skial-servers-table-head">Visited servers</th>
                                               </tr>
                                               <tr style='background:#001c8a;position:sticky;top:0;'><td>Server type</td><td>Username</td><td>Last seen</td></tr>
                                               `;
                        let table_body = document.createElement("tbody");
                        table.appendChild(table_head);
                        table.appendChild(table_body);

                        document.getElementById("skial-servers").appendChild(table)

                        for (let r = 0; r < results.length; r++) {
                            let table_row = document.createElement("tr");
                            let servername_cell = document.createElement("td");
                            let username_cell = document.createElement("td");
                            let date_cell = document.createElement("td");

                            servername_cell.innerText = results[r].Game;
                            username_cell.innerText = results[r].Name;
                            date_cell.innerText = convertTimestampPrecise(results[r].Time);

                            table_row.appendChild(servername_cell);
                            table_row.appendChild(username_cell);
                            table_row.appendChild(date_cell);
                            table_body.appendChild(table_row);
                        }

                        getSkialNameHistory(id);
                    } else {
                        title.innerHTML = `<h2 class='status-title title-light'>Skial - No account</h2>`;
                        stats_info.className = "ban-status-noaccount";
                        skial_sumstatus.className = "summary-status summary-status-noaccount";
                        skial_sumstatus.innerText = "No account";
                        content.innerHTML = `<p style='display:inline;'>Not found - User has no account on the website</p>`;
                    }
                } else {
                    title.innerHTML = `<h2 class='status-title title-normal'>Skial - No results detected</h2>`;
                    stats_info.className = "ban-status-warning";
                    skial_sumstatus.className = "summary-status summary-status-warning";
                    skial_sumstatus.innerText = "No results detected";
                    content.innerHTML = `<p style='display:inline;'>Skial gave a response but did not return any expected results.<br>Report this issue if it persists.</p>`;
                }
            }
            else if (this.readyState == 4 && this.status == 400) {
                stats_info.className = "ban-status-warning";
                title.innerHTML = `<h2 class='status-title title-warning'>Skial - Bad request</h2>`;
                content.innerHTML = `<p style='display:inline;'>Error 400, request was malformed/incorrect. Report this issue if it persists.</p>`;
                skial_sumstatus.className = "summary-status summary-status-warning";
                skial_sumstatus.innerText = "Error 400";
            }
            else if (this.readyState == 4 && this.status == 403) {
                stats_info.className = "ban-status-warning";
                title.innerHTML = `<h2 class='status-title title-warning'>Skial - Forbidden</h2>`;
                content.innerHTML = `<p style='display:inline;'>Error 403, request was not authorized. Report this issue if it persists.</p>`;
                skial_sumstatus.className = "summary-status summary-status-warning";
                skial_sumstatus.innerText = "Error 403";
            }
            else if (this.readyState == 4 && this.status == 500) {
                stats_info.className = "ban-status-warning";
                title.innerHTML = `<h2 class='status-title title-warning'>Skial - Internal Server Error</h2>`;
                content.innerHTML = `<p style='display:inline;'>Error 500, Skial is currently having server issues.</p>`;
                skial_sumstatus.className = "summary-status summary-status-warning";
                skial_sumstatus.innerText = "Error 500";
            }
            else if (this.readyState == 4 && this.status == 502) {
                stats_info.className = "ban-status-warning";
                title.innerHTML = `<h2 class='status-title title-warning'>Skial - Bad Gateway</h2>`;
                content.innerHTML = `<p style='display:inline;'>Error 502, Skial is currently having server issues.</p>`;
                skial_sumstatus.className = "summary-status summary-status-warning";
                skial_sumstatus.innerText = "Error 502";
            }
            else if (this.readyState == 4 && this.status == 503) {
                stats_info.className = "ban-status-warning";
                title.innerHTML = `<h2 class='status-title title-warning'>Skial - Service Unavailable</h2>`;
                content.innerHTML = `<p style='display:inline;'>Error 503, Skial is currently having server issues.</p>`;
                skial_sumstatus.className = "summary-status summary-status-warning";
                skial_sumstatus.innerText = "Error 503";
            }

            if (this.readyState == 4) {
                content.innerHTML += `<br><a class="skial-research-link" href="https://stats.skial.com/#summary/player/SteamID/All/${p_id3}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SKIAL_ICON});"></i>Stats</a>
                                          <a class="skial-research-link" href="https://www.skial.com/sourcebans/index.php?p=banlist&advSearch=${p_id3}&advType=steamid" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${SKIAL_ICON});"></i>Bans</a>`;
            }
        },
        ontimeout: function() {
            stats_info.className = "ban-status-warning";
            title.innerHTML = `<h2 class='status-title title-warning'>Skial - Timed out</h2>`;
            content.innerHTML = `<p style='display:inline;'>Skial did not respond quickly enough.</p>`;
            skial_sumstatus.className = "summary-status summary-status-warning";
            skial_sumstatus.innerText = "Timed out";
        },
        onerror: function() {
            stats_info.className = "ban-status-warning";
            title.innerHTML = `<h2 class='status-title title-warning'>Skial - Application error</h2>`;
            content.innerHTML = `<p style='display:inline;'>An application error occured while connecting to Skial.<br>This may be due to a Content Security Policy issue, report this issue if it persists.</p>`;
            skial_sumstatus.className = "summary-status summary-status-warning";
            skial_sumstatus.innerText = "Application error";
        }
    });
}

function getSkialNameHistory(p_id, page = 0) {
    var stats_info = document.getElementById("skial-information");

    var table_head, table_body, table;

    if (page == 0) {
        var skial_names = document.createElement("div");
        skial_names.id = "skial-names";
        stats_info.appendChild(skial_names);

        table = document.createElement("table");
        table.id = "skial-names-table";
        table.className = "history-table";

        table_head = document.createElement("thead");
        table_head.innerHTML = `
                               <tr>
                                   <th colspan="2" class="history-table-head" id="skial-names-table-head">Name history</th>
                               </tr>
                               <tr style='background:#001c8a;position:sticky;top:0;'><td>Name</td><td>Date seen</td></tr>
                               `;
        table_body = document.createElement("tbody");
        table_body.id = "skial-names-table-body";
        table.appendChild(table_head);
        table.appendChild(table_body);

        document.getElementById("skial-names").appendChild(table)
    } else {
        table_head = document.getElementById("skial-names-table-head");
        table_body = document.getElementById("skial-names-table-body");
    }

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://stats.skial.com/api?type=search_player&id=${p_id}&all=1&page=${page}`,
        timeout: skial_apitimeout, // Time to wait (in milliseconds) for a response
        headers: {
            "Content-Type": "application/json",
            "Accept": "application/json"
        },
        onreadystatechange: function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("skial-names-table-head").innerText = `Name history`;

                var json_response = JSON.parse(this.responseText);

                if (json_response.hasOwnProperty("Results")) {
                    var results = json_response.Results;

                    if (results.length > 0) {
                        for (let r = 0; r < results.length; r++) {
                            let table_row = document.createElement("tr");
                            let username_cell = document.createElement("td");
                            let date_cell = document.createElement("td");

                            username_cell.innerText = results[r].Name;
                            date_cell.innerText = convertTimestampPrecise(results[r].Time);

                            table_row.appendChild(username_cell);
                            table_row.appendChild(date_cell);
                            table_body.appendChild(table_row);
                        }

                        if (results.length == 20) {
                            getSkialNameHistory(p_id, ++page)
                        }
                    } else if (results.length == 0 && page == 0) {
                        let table_row = document.createElement("tr");
                        let error_cell = document.createElement("td");
                        error_cell.innerHTML = `No name history found.`;
                        error_cell.className = "skialerror-cell";
                        error_cell.colSpan = 3;
                        table_row.appendChild(error_cell);
                        table_body.appendChild(table_row);
                    }
                } else {
                    let table_row = document.createElement("tr");
                    let error_cell = document.createElement("td");
                    error_cell.innerHTML = `ERROR: Skial returned an unexpected response when loading page ${page} of the API, report this issue if it persists.`;
                    error_cell.className = "skialerror-cell";
                    error_cell.colSpan = 3;
                    table_row.appendChild(error_cell);
                    table_body.appendChild(table_row);
                }
            }
            else if (this.readyState == 4 && this.status == 400) {
                let table_row = document.createElement("tr");
                let error_cell = document.createElement("td");
                error_cell.innerHTML = `ERROR: Skial gave a Bad Response error (400) when loading page ${page} of the API, report this issue if it persists.`;
                error_cell.className = "skialerror-cell";
                error_cell.colSpan = 3;
                table_row.appendChild(error_cell);
                table_body.appendChild(table_row);
            }
            else if (this.readyState == 4 && this.status == 403) {
                let table_row = document.createElement("tr");
                let error_cell = document.createElement("td");
                error_cell.innerHTML = `ERROR: Skial gave a Forbidden error (403) when loading page ${page} of the API, you might be checking profiles too quickly.`;
                error_cell.className = "skialerror-cell";
                error_cell.colSpan = 3;
                table_row.appendChild(error_cell);
                table_body.appendChild(table_row);
            }
            else if (this.readyState == 4 && this.status == 429) {
                let table_row = document.createElement("tr");
                let error_cell = document.createElement("td");
                error_cell.innerHTML = `ERROR: Skial gave a Too Many Requests error (429) when loading page ${page} of the API, you might be checking profiles too quickly.`;
                error_cell.className = "skialerror-cell";
                error_cell.colSpan = 3;
                table_row.appendChild(error_cell);
                table_body.appendChild(table_row);
            }
            else if (this.readyState == 4 && this.status == 500) {
                let table_row = document.createElement("tr");
                let error_cell = document.createElement("td");
                error_cell.innerHTML = `ERROR: Skial gave a Internal Server Error (500) when loading page ${page} of the API, their server might be having issues.`;
                error_cell.className = "skialerror-cell";
                error_cell.colSpan = 3;
                table_row.appendChild(error_cell);
                table_body.appendChild(table_row);
            }
            else if (this.readyState == 4 && this.status == 502) {
                let table_row = document.createElement("tr");
                let error_cell = document.createElement("td");
                error_cell.innerHTML = `ERROR: Skial gave a Bad Gateway error (502) when loading page ${page} of the API, their server might be having issues.`;
                error_cell.className = "skialerror-cell";
                error_cell.colSpan = 3;
                table_row.appendChild(error_cell);
                table_body.appendChild(table_row);
            }
            else if (this.readyState == 4 && this.status == 503) {
                let table_row = document.createElement("tr");
                let error_cell = document.createElement("td");
                error_cell.innerHTML = `ERROR: Skial gave a Service Unavailable error (503) when loading page ${page} of the API, their server might be having issues.`;
                error_cell.className = "skialerror-cell";
                error_cell.colSpan = 3;
                table_row.appendChild(error_cell);
                table_body.appendChild(table_row);
            }
        },
        ontimeout: function() {
            let table_row = document.createElement("tr");
            let error_cell = document.createElement("td");
            error_cell.innerHTML = `ERROR: Skial timed out when loading page ${page} of the API.`;
            error_cell.className = "skialerror-cell";
            error_cell.colSpan = 3;
            table_row.appendChild(error_cell);
            table_body.appendChild(table_row);
        },
        onerror: function() {
            let table_row = document.createElement("tr");
            let error_cell = document.createElement("td");
            error_cell.innerHTML = `ERROR: An application error occured when loading page ${page} of the Skial API.<br>This may be due to a Content Security Policy issue, report this issue if it persists.`;
            error_cell.className = "skialerror-cell";
            error_cell.colSpan = 3;
            table_row.appendChild(error_cell);
            table_body.appendChild(table_row);
        }
    });
}

function getSteamIdUkInfo(p_myid, p_apikey, p_profileid64) {
    let content = document.getElementById(`${steamiduk.id_prefix}-content`);
    let links_html = `<br><a class="sid-research-link" href="https://steamid.uk/profile/${p_profileid64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMIDUK_ICON});"></i>Profile</a>`;

    if (p_myid != "" && p_apikey != "") {
        GM_xmlhttpRequest({
            method: "GET",
            url: `https://steamidapi.uk/v2/steamid.php?myid=${p_myid}&apikey=${p_apikey}&input=${p_profileid64}`,
            timeout: steamiduk.api_timeout, // Time to wait (in milliseconds) for a response
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            onreadystatechange: function() {
                if (this.readyState === 1) {
                    updateRepContainer(steamiduk, "Connecting");
                    updateSummaryStatus(steamiduk, "Connecting");
                }
                else if (this.readyState === 3) {
                    updateRepContainer(steamiduk, "Loading");
                    updateSummaryStatus(steamiduk, "Loading");
                } else if (this.readyState === 4) {
                    if (this.status === 200) {
                        var json_response = JSON.parse(this.responseText);

                        if (json_response.hasOwnProperty("auth")) {
                            if (json_response.profile_bans.steamid_ban != 1) {
                                updateRepContainer(steamiduk, "Normal");
                                updateSummaryStatus(steamiduk, "Normal");
                            } else if (json_response.profile_bans.steamid_ban == 1) {
                                updateRepContainer(steamiduk, "Banned");
                                updateSummaryStatus(steamiduk, "Banned");
                            }

                            let optout_label = "";

                            if (json_response.steamid_data.steamid_optout == 1) {
                                optout_label = `<div class="sid-detail-container detail-negative" style="margin-bottom:-15px;">
                                                    <p style='display:inline;'>This user has opted out of SteamID and has had historical data removed.</p>
                                                </div>`;
                            }

                            content.innerHTML = `<div class="sid-detail-container">
                                                     <p style='display:inline;'><b>Daily profile lookups:</b> ${json_response.auth.daily_count}/${json_response.auth.daily_limit}</p>
                                                 </div>
                                                 ${optout_label}`;

                            if (json_response.steamid_data.steamid_optout != 1) {
                                content.innerHTML += `<div class="sid-detail-container" style="margin-bottom:-15px;">
                                                          <p><b>Name changes recorded:</b> ${json_response.steamid_data.name_history_count}</p>
                                                          <p><b>Custom URL changes recorded:</b> ${json_response.steamid_data.url_changes}</p>
                                                      </div>
                                                     `;
                            }
                        } else if (json_response.hasOwnProperty("error")) {
                            if (json_response.error.hasOwnProperty("errorid")) {
                                if (json_response.error.errorid == 0) {
                                    updateRepContainer(steamiduk, "Database connectivity issue");
                                    updateSummaryStatus(steamiduk, "Database issue");
                                } else if (json_response.error.errorid == 1) {
                                    updateRepContainer(steamiduk, "ID64 not found");
                                    updateSummaryStatus(steamiduk, "ID64 not found");
                                } else if (json_response.error.errorid == 3) {
                                    updateRepContainer(steamiduk, "Invalid ID64/API Key settings");
                                    updateSummaryStatus(steamiduk, "Invalid settings");
                                } else if (json_response.error.errorid == 4) {
                                    updateRepContainer(steamiduk, "Daily lookup limit reached");
                                    updateSummaryStatus(steamiduk, "Lookup limit reached");
                                } else if (json_response.error.errorid == 9) {
                                    updateRepContainer(steamiduk, "API Key Disabled");
                                    updateSummaryStatus(steamiduk, "API Key Disabled");
                                } else if (json_response.error.errorid == 10) {
                                    updateRepContainer(steamiduk, "Internal API error");
                                    updateSummaryStatus(steamiduk, "Internal API error");
                                }
                            } else {
                                updateRepContainer(steamiduk, "Unknown error");
                                updateSummaryStatus(steamiduk, "Unknown error");
                            }
                        }
                    } else {
                        updateRepContainer(steamiduk, this.status);
                        updateSummaryStatus(steamiduk, this.status);
                    }

                    content.innerHTML += links_html;
                }
            },
            ontimeout: function() {
                updateRepContainer(steamiduk, "Timed out");
                updateSummaryStatus(steamiduk, "Timed out");
                content.innerHTML += links_html;
            },
            onerror: function() {
                updateRepContainer(steamiduk, "Application error");
                updateSummaryStatus(steamiduk, "Application error");
                content.innerHTML += links_html;
            }
        });
    } else if (p_myid === "" && p_apikey === "") {
        updateRepContainer(steamiduk, "API Key Required");
        updateSummaryStatus(steamiduk, "API Key Required");
        content.innerHTML += links_html;
    } else if (p_myid === "" && p_apikey != "") {
        updateRepContainer(steamiduk, "ID64 not set");
        updateSummaryStatus(steamiduk, "ID64 not set");
        content.innerHTML += links_html;
    } else if (p_myid != "" && p_apikey === "") {
        updateRepContainer(steamiduk, "API Key not set");
        updateSummaryStatus(steamiduk, "API Key not set");
        content.innerHTML += links_html;
    }
}

function getSteamTradesInfo(p_id64) {
    let content = document.getElementById(`${steamtrades.id_prefix}-content`);
    let info = document.getElementById(`${steamtrades.id_prefix}-info-container`);
    let links_html = `<br><a class="st-research-link" href="https://www.steamtrades.com/user/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMTRADES_ICON});"></i>Profile</a>
                          <a class="st-research-link" href="https://www.steamtrades.com/trades/search?user=${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMTRADES_ICON});"></i>Trades</a>`;

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://www.steamtrades.com/user/${p_id64}`,
        timeout: steamtrades.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function() {
            if (this.readyState === 1) {
                updateRepContainer(steamtrades, "Connecting");
                updateSummaryStatus(steamtrades, "Connecting");
            }
            else if (this.readyState === 3) {
                updateRepContainer(steamtrades, "Loading");
                updateSummaryStatus(steamtrades, "Loading");
            }
            else if (this.readyState === 4) {
                if (this.status === 200) {
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(this.responseText,"text/html");
                    let regdate_text = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(1) > div:nth-child(2)");

                    if (regdate_text) {
                        if (regdate_text.innerText != "-") {
                            let table_container = document.createElement("div");
                            table_container.id = `${steamtrades.id_prefix}-details`;
                            info.appendChild(table_container);

                            let table = document.createElement("table");
                            table.id = `${steamtrades.id_prefix}-details-table`;

                            let table_body = document.createElement("tbody");
                            table_body.id = `${steamtrades.id_prefix}-details-table-body`;

                            let registration_row = document.createElement("tr");
                            registration_row.id = `${steamtrades.id_prefix}-registration-row`;
                            registration_row.innerHTML = "<td>Registered</td>";
                            let registration_cell = document.createElement("td");
                            registration_cell.id = `${steamtrades.id_prefix}-registration-date`;
                            registration_cell.classList.toggle("color-warning");
                            registration_cell.innerText = "Unknown";
                            registration_row.appendChild(registration_cell);

                            let lastonline_row = document.createElement("tr");
                            lastonline_row.id = `${steamtrades.id_prefix}-lastonline-row`;
                            lastonline_row.innerHTML = "<td>Last online</td>";
                            let lastonline_cell = document.createElement("td");
                            lastonline_cell.id = `${steamtrades.id_prefix}-lastonline-date`;
                            lastonline_cell.classList.toggle("color-warning");
                            lastonline_cell.innerText = "No data found";
                            lastonline_row.appendChild(lastonline_cell);

                            let reputation_row = document.createElement("tr");
                            reputation_row.id = `${steamtrades.id_prefix}-reputation-row`;
                            reputation_row.innerHTML = "<td>Reputation</td>";
                            let reputation_cell = document.createElement("td");
                            reputation_cell.id = `${steamtrades.id_prefix}-reputation-date`;
                            reputation_cell.classList.toggle("color-warning");
                            reputation_cell.innerText = "No data found";
                            reputation_row.appendChild(reputation_cell);

                            table_body.appendChild(registration_row);
                            table_body.appendChild(lastonline_row);
                            table_body.appendChild(reputation_row);

                            let regdate_data = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(1) > div:nth-child(2) > span:nth-child(1)");
                            let lastonline_data = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(2) > div:nth-child(2) > span:nth-child(1)");
                            let lastonline_text = html_response.querySelector("div.sidebar_table:nth-child(7) > div:nth-child(2) > div:nth-child(2)");
                            let positive_rep = html_response.querySelector(".increment_positive_review_count");
                            let negative_rep = html_response.querySelector(".increment_negative_review_count");

                            if (regdate_data) {
                                let registration_timestamp = regdate_data.getAttribute("data-timestamp");
                                if (registration_timestamp) {
                                    registration_cell.classList.toggle("color-warning");
                                    registration_cell.innerText = convertTimestampPrecise(registration_timestamp, true);
                                } else {
                                    console.log(`[DEBUG] | ${steamtrades.module_name} : No registration timestamp attribute found.`);
                                }
                            } else {
                                console.log(`[DEBUG] | ${steamtrades.module_name} : No registration date data found.`);
                            }

                            if (lastonline_data) {
                                let lastonline_timestamp = lastonline_data.getAttribute("data-timestamp");
                                if (lastonline_timestamp) {
                                    lastonline_cell.classList.toggle("color-warning");
                                    lastonline_cell.innerText = convertTimestampPrecise(lastonline_timestamp, true);
                                } else {
                                    console.log(`[DEBUG] | ${steamtrades.module_name} : No last online timestamp attribute found.`);
                                }
                            } else if (lastonline_text) {
                                if (lastonline_text.innerText === "Now") {
                                    lastonline_cell.classList.toggle("color-warning");
                                    lastonline_cell.innerText = "Now";
                                }
                            } else {
                                console.log(`[DEBUG] | ${steamtrades.module_name} : No last online data found.`);
                            }

                            if (positive_rep && negative_rep) {
                                let postrust_count = parseInt(positive_rep.innerText);
                                let negtrust_count = parseInt(negative_rep.innerText);

                                if (!isNaN(postrust_count) && !isNaN(negtrust_count)) {
                                    updateRepCountSummary(steamtrades, postrust_count, negtrust_count);
                                    let pos_percent = 0, neg_percent = 0;

                                    if (postrust_count > 0 || negtrust_count > 0) {
                                        pos_percent = postrust_count / (postrust_count + negtrust_count) * 100;
                                        neg_percent = negtrust_count / (postrust_count + negtrust_count) * 100;
                                    }

                                    reputation_cell.classList.toggle("color-warning");
                                    reputation_cell.innerText = `+${postrust_count} (${pos_percent.toFixed(2)}%) / -${negtrust_count} (${neg_percent.toFixed(2)}%)`;

                                    if (pos_percent > 0 && neg_percent == 0) {
                                        reputation_cell.classList.add('detail-positive');
                                    } else if (pos_percent > 0 || neg_percent > 0) {
                                        if (pos_percent >= neg_percent) {
                                            reputation_cell.classList.add('detail-mixed');
                                        } else {
                                            reputation_cell.classList.add('detail-negative');
                                        }
                                    }
                                }
                            } else {
                                console.log(`[DEBUG] | ${steamtrades.module_name} : Reputation data not found.`);
                            }

                            table.appendChild(table_body);
                            document.getElementById(`${steamtrades.id_prefix}-details`).appendChild(table)

                            updateRepContainer(steamtrades, "Normal");
                            updateSummaryStatus(steamtrades, "Normal");
                        } else {
                            updateRepContainer(steamtrades, "No account");
                            updateSummaryStatus(steamtrades, "No account");
                        }
                    } else {
                        updateRepContainer(steamtrades, "Missing data");
                        updateSummaryStatus(steamtrades, "Missing data");
                    }
                } else {
                    updateRepContainer(steamtrades, this.status);
                    updateSummaryStatus(steamtrades, this.status);
                }

                content.innerHTML += links_html;
            }
        },
        ontimeout: function() {
            updateRepContainer(steamtrades, "Timed out");
            content.innerHTML += links_html;
            updateSummaryStatus(steamtrades, "Timed out");
        },
        onerror: function() {
            updateRepContainer(steamtrades, "Application error");
            content.innerHTML += links_html;
            updateSummaryStatus(steamtrades, "Application error");
        }
    });
}

function getVACBannedInfo(p_id64)
{
    let content = document.getElementById(`${vacbanned.id_prefix}-content`);
    let info = document.getElementById(`${vacbanned.id_prefix}-info-container`);
    let links_html = `<br><a class="vacbanned-research-link" style="width:170px;" href="http://vacbanned.com/view/detail/id/${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${VAC_ICON});"></i>Profile (Cached)</a>
                          <a class="vacbanned-research-link" style="width:170px;" href="http://vacbanned.com/engine/check/?qsearch=${p_id64}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${VAC_ICON});"></i>Profile (Force check)</a>`;

    GM_xmlhttpRequest({
        method: "GET",
        url: `http://vacbanned.com/view/detail/id/${p_id64}`,
        timeout: vacbanned.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function() {
            if (this.readyState === 1) {
                updateRepContainer(vacbanned, "Connecting");
                updateSummaryStatus(vacbanned, "Connecting");
            }
            else if (this.readyState === 3) {
                updateRepContainer(vacbanned, "Loading");
                updateSummaryStatus(vacbanned, "Loading");
            }
            else if (this.readyState === 4) {
                if (this.status === 200) {
                    var dp = new DOMParser();
                    var html_response = dp.parseFromString(this.responseText,"text/html");
                    var tables = html_response.getElementsByTagName("table");

                    if (tables.length === 2) {
                        var names_tablehead, details_tablebody, names_tablebody, details_table, names_table;

                        // Details table
                        var data = tables[0].firstElementChild.children;

                        let vacbanned_details = document.createElement("div");
                        vacbanned_details.id = "vacbanned-details";
                        info.appendChild(vacbanned_details);

                        details_table = document.createElement("table");
                        details_table.id = "vacbanned-details-table";

                        details_tablebody = document.createElement("tbody");
                        details_tablebody.id = "vacbanned-details-table-body";
                        details_table.appendChild(details_tablebody);
                        document.getElementById("vacbanned-details").appendChild(details_table)

                        for (let i = 4; i < 9; i++) { // Start from 4th element, first three rows don't contain relevant information.
                            try {
                                if (i === 4) {
                                    if (data[i].children[1].innerText.trim() === "NOT BANNED") {
                                        if (!profile.is_vacbanned) {
                                            updateRepContainer(vacbanned, "Normal");
                                            updateSummaryStatus(vacbanned, "Normal");
                                        } else {
                                            updateRepContainer(vacbanned, "Outdated data");
                                            updateSummaryStatus(vacbanned, "Outdated data");
                                        }
                                    } else if (data[i].children[1].innerText.trim() === "UNBANNED") {
                                        updateRepContainer(vacbanned, "Unbanned");
                                        updateSummaryStatus(vacbanned, "Unbanned");
                                    } else {
                                        updateRepContainer(vacbanned, "Banned");
                                        updateSummaryStatus(vacbanned, "Banned");
                                    }
                                } else {
                                    if (i === 5 && data[i].children[1].innerText === "") { continue; } // Don't show country if info is empty
                                    let table_row = document.createElement("tr");
                                    let detail_cell = document.createElement("td");
                                    let info_cell = document.createElement("td");

                                    detail_cell.innerText = data[i].children[0].innerText;
                                    info_cell.innerText = data[i].children[1].innerText;

                                    table_row.appendChild(detail_cell);
                                    table_row.appendChild(info_cell);
                                    details_tablebody.appendChild(table_row);
                                }
                            } catch(e) {
                                console.log(`%c[DEBUG] | Parsing failed at row index ${i} : ${e}`, "color: red; font-size: 20px;");
                            }
                        }

                        // Name table
                        let vacbanned_names = document.createElement("div");
                        vacbanned_names.id = "vacbanned-names";
                        info.appendChild(vacbanned_names);

                        names_table = document.createElement("table");
                        names_table.id = "vacbanned-names-table";
                        names_table.className = "history-table";

                        names_tablehead = document.createElement("thead");
                        names_tablehead.innerHTML =
                            `
                        <tr>
                            <th colspan="2" class="history-table-head" id="vacbanned-names-table-head">Name history</th>
                        </tr>
                        <tr style='background:#001c8a;position:sticky;top:0;'><td>Name</td><td>Date seen (CET)</td></tr>
                        `;
                        names_tablebody = document.createElement("tbody");
                        names_tablebody.id = "vacbanned-names-table-body";
                        names_table.appendChild(names_tablehead);
                        names_table.appendChild(names_tablebody);
                        document.getElementById("vacbanned-names").appendChild(names_table)

                        data = tables[1].firstElementChild.children;
                        if (data.length > 2) {
                            for (let i = 2; i < data.length; i++) { // Start from 3rd element, first two rows don't contain information.
                                try {
                                    if (data[i].children.length > 1) {
                                        let table_row = document.createElement("tr");
                                        let date_cell = document.createElement("td");
                                        let name_cell = document.createElement("td");

                                        date_cell.innerText = data[i].children[0].innerText;
                                        name_cell.innerText = data[i].children[1].innerText;

                                        table_row.appendChild(name_cell);
                                        table_row.appendChild(date_cell);
                                        names_tablebody.appendChild(table_row);
                                    } else {
                                        let table_row = document.createElement("tr");
                                        let error_cell = document.createElement("td");
                                        error_cell.innerHTML = `No name history found.`;
                                        error_cell.id = "vacbanned-error-cell";
                                        error_cell.colSpan = 2;
                                        table_row.appendChild(error_cell);
                                        names_tablebody.appendChild(table_row);
                                        break;
                                    }
                                } catch (e) {
                                    console.log(`%c[DEBUG] | Parsing failed at row index ${i} : ${e}`, "color: red; font-size: 20px;");
                                }
                            }
                        } else {
                            let table_row = document.createElement("tr");
                            let error_cell = document.createElement("td");
                            error_cell.innerHTML = `No name history found.`;
                            error_cell.id = "vacbanned-error-cell";
                            error_cell.colSpan = 2;
                            table_row.appendChild(error_cell);
                            names_tablebody.appendChild(table_row);
                        }

                    } else {
                        updateRepContainer(vacbanned, "No account");
                        updateSummaryStatus(vacbanned, "No account");
                    }
                } else {
                    updateRepContainer(vacbanned, this.status);
                    updateSummaryStatus(vacbanned, this.status);
                }

                content.innerHTML += links_html;
            }
        },
        ontimeout: function() {
            updateRepContainer(vacbanned, "Timed out");
            content.innerHTML += links_html;
            updateSummaryStatus(vacbanned, "Timed out");
        },
        onerror: function() {
            updateRepContainer(vacbanned, "Application error");
            content.innerHTML += links_html;
            updateSummaryStatus(vacbanned, "Application error");
        }
    });
}

function getSteamHistoryInfo(p_id64) {
    let content = document.getElementById(`${steamhistory.id_prefix}-content`);
    let info = document.getElementById(`${steamhistory.id_prefix}-info-container`);
    content.innerHTML += `<br><a class="${steamhistory.id_prefix}-research-link" href="${steamhistory.profile_url}" target="_blank" rel="noreferrer"><i class="research-icon" style="background-image: url(${STEAMHISTORY_ICON});"></i>Profile</a>`;

    function buildHistoryTable(p_datatype, p_data) {
        let data, table, table_head, table_body, table_container, detail_text;

        if (p_datatype === "usernames") {
            detail_text = "Username";
        } else if (p_datatype === "names") {
            detail_text = "Real name";
        } else if (p_datatype === "urls") {
            detail_text = "URL";
        }

        table_container = document.createElement("div");
        table_container.id = `steamhistory-${p_datatype}`;
        table_container.className = "history-table-container";
        info.appendChild(table_container);

        table = document.createElement("table");
        table.id = `steamhistory-${p_datatype}-table`;
        table.className = "history-table";

        table_head = document.createElement("thead");
        table_head.innerHTML =
            `
        <tr>
            <th colspan="2" class="history-table-head" id="steamhistory-${p_datatype}-table-head">${detail_text} history</th>
        </tr>
        <tr style='background:#001c8a;position:sticky;top:0;'><td>${detail_text}</td><td>Date seen</td></tr>
        `;

        table_body = document.createElement("tbody");
        table_body.id = "steamhistory-usernames-table-body";
        table.appendChild(table_head);
        table.appendChild(table_body);

        document.getElementById(`steamhistory-${p_datatype}`).appendChild(table);

        data = p_data;

        if (data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                try {
                    let table_row = document.createElement("tr");
                    let date_cell = document.createElement("td");
                    let detail_cell = document.createElement("td");

                    date_cell.innerText = data[i].children[1].innerText.replace('[Estimated]', '');
                    if (date_cell.innerText.startsWith("1969")) {
                        date_cell.innerHTML = '<span class="color-warning">N/A</span>';
                    }

                    if (p_datatype === "urls") {
                        detail_cell.innerText = data[i].children[0].innerText.split('/')[4]
                    } else {
                        detail_cell.innerText = data[i].children[0].innerText;
                        detail_cell.innerHTML = detail_cell.innerHTML.replace('[email&nbsp;protected]', '<span class="color-warning">[email&nbsp;protected]</span>');
                    }

                    table_row.appendChild(detail_cell);
                    table_row.appendChild(date_cell);
                    table_body.appendChild(table_row);
                } catch (e) {
                    console.log(`%c[DEBUG] | SteamHistory.net | ${detail_text} history parsing failed at row index ${i} : ${e}`, "color: red; font-size: 20px;");
                }
            }
        } else {
            let table_row = document.createElement("tr");
            let error_cell = document.createElement("td");
            if (p_datatype === "usernames") {
                error_cell.innerText = `No username history found.`;
            } else if (p_datatype === "names") {
                error_cell.innerText = `No real name history found.`;
            } else if (p_datatype === "urls") {
                error_cell.innerText = `No URL history found.`;
            }
            error_cell.className = "color-warning";
            error_cell.colSpan = 2;
            table_row.appendChild(error_cell);
            table_body.appendChild(table_row);
        }
    }

    GM_xmlhttpRequest({
        method: "GET",
        url: `https://steamhistory.net/id/${p_id64}`,
        timeout: steamhistory.api_timeout, // Time to wait (in milliseconds) for a response
        onreadystatechange: function() {
            if (this.readyState === 1) {
                updateRepContainer(steamhistory, "Connecting");
                updateSummaryStatus(steamhistory, "Connecting");
            }
            else if (this.readyState === 3) {
                updateRepContainer(steamhistory, "Loading");
                updateSummaryStatus(steamhistory, "Loading");
            }
            else if (this.readyState === 4) {
                if (this.status === 200) {
                    let dp = new DOMParser();
                    let html_response = dp.parseFromString(this.responseText,"text/html");
                    let tables = html_response.getElementsByClassName("table-sm");

                    if (tables.length === 6) {
                        updateRepContainer(steamhistory, "Normal");
                        updateSummaryStatus(steamhistory, "Normal");

                        buildHistoryTable("usernames", tables[0].children[1].children);
                        buildHistoryTable("names", tables[1].children[1].children);
                        buildHistoryTable("urls", tables[2].children[1].children);
                    } else {
                        updateRepContainer(steamhistory, "Not indexed");
                        updateSummaryStatus(steamhistory, "Not indexed");
                    }
                } else {
                    updateRepContainer(steamhistory, this.status);
                    updateSummaryStatus(steamhistory, this.status);
                }
            }
        },
        ontimeout: function() {
            updateRepContainer(steamhistory, "Timed out");
            updateSummaryStatus(steamhistory, "Timed out");
        },
        onerror: function() {
            updateRepContainer(steamhistory, "Application error");
            updateSummaryStatus(steamhistory, "Application error");
        }
    });
}

function convertTimestamp(p_timestamp, p_fullmonths = false)
{
    let months, date;
    if (p_fullmonths) {
        date = new Date(p_timestamp);
        months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    } else {
        date = new Date(p_timestamp * 1000);
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    }
    let year = date.getFullYear();
    let month = months[date.getMonth()];
    let day = date.getDate();

    if (p_fullmonths) {
        return `${month} ${day}, ${year}`;
    } else {
        return `${day} ${month} ${year}`;
    }
}

function convertTimestampPrecise(p_timestamp, p_fullmonths = false, p_isunixtimestamp = true)
{
    let months, date;
    if (p_fullmonths) {
        months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
    } else {
        months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    }

    if (p_isunixtimestamp) {
        date = new Date(p_timestamp * 1000);
    } else {
        date = new Date(p_timestamp);
    }
    
    if (p_fullmonths) {
        return `${months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()} @ ${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    } else {
        return `${date.getDate()} ${months[date.getMonth()]} ${date.getFullYear()} ${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}`;
    }
}

function buildLinkButton(p_siteurl, p_extraparams = ["", ""], p_idtype, p_isarchive = false) {
    let link = document.createElement("a");
    let ids = {"ID2": profile.id2.replace('STEAM_1', 'STEAM_0'), "ID3": profile.id3, "ID64": profile.id64}

    link.className = "research-link";

    if (p_idtype === "URL") {
        link.href = p_siteurl + `${p_isarchive ? "https://steamcommunity.com/id/" : ""}` + p_extraparams[0] + profile.custom_url + p_extraparams[1];
    } else {
        link.href = p_siteurl + `${p_isarchive ? "https://steamcommunity.com/profiles/" : ""}` + p_extraparams[0] + ids[p_idtype] + p_extraparams[1];
    }

    link.innerText = p_idtype;
    link.target = "_blank";
    link.rel = "noreferrer";

    return link;
}

function buildArchiveLink(p_siteicon = "", p_sitename = "No site name set", p_siteurl = "https://example.com/", p_extraparams = ["",""])
{
    let site_container = document.createElement("div")
    site_container.className = "research-site-container";

    if (p_siteicon != "") {
        let icon = document.createElement("i");
        icon.className = "research-icon";
        icon.style.backgroundImage = `url(${p_siteicon})`;
        site_container.appendChild(icon);
    }

    let name = document.createElement("span");
    name.className = "research-site-label";
    name.innerText = p_sitename;
    site_container.appendChild(name);

    let link_container = document.createElement("div")
    link_container.className = "research-link-container";
    site_container.appendChild(link_container);

    link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID3", true));
    link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID64", true));
    if (profile.custom_url != "") {
        link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "URL", true));
    }

    return site_container;
}

function buildResearchLink(p_siteicon = "", // Data URI or Icon URL used (if applicable)
                            p_sitename = "No site name set",
                            p_siteurl = "https://example.com/", // Base site URL, without parameters
                            p_acceptsid2 = false,
                            p_acceptsid3 = false,
                            p_acceptsid64 = false,
                            p_acceptscustomurl = false,
                            p_extraparams = ["",""]) // Extra parameters/characters to add at the beginning and/or end of the Steam ID/URL
{

    let site_container = document.createElement("div")
    let accepted_formats = p_acceptsid2 + p_acceptsid3 + p_acceptsid64 + p_acceptscustomurl;

    if (accepted_formats === 1) {
        site_container = document.createElement("a")
        site_container.classList.add("simple-site");
    }

    site_container.classList.add("research-site-container");

    if (p_siteicon != "") {
        let icon = document.createElement("i");
        icon.className = "research-icon";
        icon.style.backgroundImage = `url(${p_siteicon})`;
        site_container.appendChild(icon);
    }

    let name = document.createElement("span");
    name.className = "research-site-label";
    name.innerText = p_sitename;
    site_container.appendChild(name);

    if (accepted_formats > 1) {
        let link_container = document.createElement("div")
        link_container.className = "research-link-container";
        site_container.appendChild(link_container);

        if (p_acceptsid2) {
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID2"));
        }

        if (p_acceptsid3) {
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID3"));
        }

        if (p_acceptsid64) {
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "ID64"));
        }

        if (p_acceptscustomurl && profile.custom_url != ""){
            link_container.appendChild(buildLinkButton(p_siteurl, p_extraparams, "URL"));
        }
    } else {
        if (p_acceptsid2) {
            site_container.href = p_siteurl + p_extraparams[0] + profile.id2.replace('STEAM_1', 'STEAM_0') + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        } else if (p_acceptsid3) {
            site_container.href = p_siteurl + p_extraparams[0] + profile.id3 + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        } else if (p_acceptsid64) {
            site_container.href = p_siteurl + p_extraparams[0] + profile.id64 + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        } else if (p_acceptscustomurl && profile.custom_url != "") {
            site_container.href = p_siteurl + p_extraparams[0] + profile.custom_url + p_extraparams[1];
            site_container.target = "_blank";
            site_container.rel = "noreferrer";
        }
    }

    return site_container;
}

function buildSummary(p_module) {
    console.log(`[DEBUG] | Calling buildSummary() for ${p_module.module_name} | Summary ${p_module.summary_enabled ? "enabled" : "DISABLED"}`);

    if (p_module.summary_enabled) {
        let summary_container = document.getElementById("summary-container");
        let summary = document.createElement("div");
        let summary_title = document.createElement("a");
        let summary_status = document.createElement("span");

        summary_title.innerText = p_module.summary_name ? p_module.summary_name : p_module.module_name;
        summary_title.id = `${p_module.id_prefix}-summary-title`;
        summary_title.className = "summary-title";
        summary_title.href = p_module.profile_url;
        summary_title.target = "_blank";
        summary_title.rel = "noreferrer";

        summary_status.innerText = "Unknown";
        summary_status.id = `${p_module.id_prefix}-summary-status`;
        summary_status.className = "summary-status summary-status-light";

        summary.appendChild(summary_title);
        summary.appendChild(summary_status);

        summary.id = `${p_module.id_prefix}-summary`;
        summary.className = "site-summary";
        summary_container.appendChild(summary);
    }
}

function updateSummaryStatus(p_module, p_status) {
    if (p_module.summary_enabled) {
        let summary_status = document.getElementById(`${p_module.id_prefix}-summary-status`);

        if (p_status === "Unknown" || p_status.toString().startsWith("Connecting") || p_status.toString().startsWith("Loading") || p_status === "API Key Required") {
            summary_status.className = "summary-status summary-status-light";
        } else if (p_status === "Normal") {
            summary_status.className = "summary-status summary-status-normal";
        } else if (p_status === "No account") {
            document.getElementById(`${p_module.id_prefix}-summary-title`).onclick = function() {alert("User has no account on the website.")};
            summary_status.className = "summary-status summary-status-noaccount";
        } else if (p_status === "Scammer" || p_status === "Banned" || p_status === "Scrap/MP/BP Staff") {
            summary_status.className = "summary-status summary-status-danger";
        } else if (p_status === "Admin") {
            summary_status.className = "summary-status summary-status-special";
        } else if (!isNaN(p_status - parseFloat(p_status)) ||
                   p_status === "Caution" || p_status.toString().startsWith("Timed out") || p_status.toString().startsWith("Application error") ||
                   p_status === "Outdated data" || p_status === "Unbanned" || p_status === "Not indexed" ||
                   p_status === "Database issue" || p_status === "API Key not set" ||p_status === "ID64 not set" ||
                   p_status === "ID64 not found" || p_status === "Invalid settings" || p_status === "Lookup limit reached" ||
                   p_status === "API Key Disabled" || p_status === "Internal API error" || p_status === "Unknown error" ||
                   p_status === "Missing data") {
            summary_status.className = "summary-status summary-status-warning";
        } else {
            alert(`Invalid summary status specified for ${p_module.module_name} , status: ${p_status}. Report the issue if you see this message.`)
        }

        if (!isNaN(p_status - parseFloat(p_status))) {
            if (p_status >= 400 && p_status <= 599) {
                summary_status.innerText = `Error ${p_status}`;
            } else {
                summary_status.innerText = `HTTP status ${p_status}`;
            }
        } else {
            summary_status.innerText = p_status;
        }
    }
}

function buildRepCountSummary(p_module, p_reptype = "Reputation") {
    if (p_module.summary_enabled) {
        let summary_container = document.getElementById("summary-container");

        let rep_summary = document.createElement("table");
        rep_summary.id = `${p_module.id_prefix}-rep-summary`;
        rep_summary.className = "rep-summary hidden";

        let rep_summary_title = document.createElement("td");
        rep_summary_title.id = `${p_module.id_prefix}-rep-summary-title`;
        rep_summary_title.className = "rep-summary-title";
        rep_summary_title.innerText = p_reptype;

        let pos_trust = document.createElement("td");
        pos_trust.innerText = "+ ?";
        pos_trust.id = `${p_module.id_prefix}-positive-trust-count`;
        pos_trust.className = "rep-summary-positive";
        let neg_trust = document.createElement("td");
        neg_trust.innerText = "- ?";
        neg_trust.id = `${p_module.id_prefix}-negative-trust-count`;
        neg_trust.className = "rep-summary-negative";

        rep_summary.appendChild(rep_summary_title);
        rep_summary.appendChild(pos_trust);
        rep_summary.appendChild(neg_trust);

        summary_container.appendChild(rep_summary);
    }
}

function updateRepCountSummary(p_module, p_poscount, p_negcount) {
    if (p_module.summary_enabled) {
        let rep_summary = document.getElementById(`${p_module.id_prefix}-rep-summary`);
        let pos_trust = document.getElementById(`${p_module.id_prefix}-positive-trust-count`);
        let neg_trust = document.getElementById(`${p_module.id_prefix}-negative-trust-count`);

        rep_summary.classList.remove("hidden");
        pos_trust.innerText = `+ ${p_poscount}`;
        neg_trust.innerText = `- ${p_negcount}`;
    }
}

function buildSummaryContainer() {
    console.log(`[DEBUG] | Calling buildSummaryContainer()`);

    // Determines where to insert the HTML code
    let summary_area = document.getElementsByClassName("profile_rightcol")[0];

    let account_infobox = document.createElement("div");
    account_infobox.id = "account-info";
    account_infobox.innerHTML = `<p id="account-loading-label">Loading script...</p>
                                 <p id="account-loading-description">If you see this for more than a minute, please report the issue.</p>`;
    summary_area.insertBefore(account_infobox, summary_area.childNodes[0]);

    let summary_container = document.createElement("div");
    summary_container.id = "summary-container";
    account_infobox.parentNode.insertBefore(summary_container, account_infobox.nextSibling); // Inserts after account info

    if (steamrep.is_enabled) {
        buildSummary(steamrep);
    }

    if (bptf_enabled) {
        bptf_sumtitle.innerText = "Backpack.tf";
        bptf_sumtitle.id = "bptf-summary-title";
        bptf_sumtitle.className = "summary-title";
        bptf_sumtitle.href = `https://backpack.tf/u/${g_rgProfileData.steamid}`;
        bptf_sumtitle.target = "_blank";
        bptf_sumtitle.rel = "noreferrer";

        bptf_sumstatus.innerText = "Unknown";
        bptf_sumstatus.id = "bptf-summary-status";
        bptf_sumstatus.className = "summary-status summary-status-light";

        bptf_summary.appendChild(bptf_sumtitle);
        bptf_summary.appendChild(bptf_sumstatus);
        bptf_summary.id = "bptf-summary";
        bptf_summary.className = "site-summary";

        var trust_summary_title = document.createElement("div");
        trust_summary_title.id = "trust-summary-title";
        trust_summary_title.innerText = "Trust";
        bptf_trust_summary.id = "bptf-trust-summary";
        bptf_trust_summary.className = "site-summary hidden";
        bptf_sumpostrust.innerText = "+ ?";
        bptf_sumpostrust.className = "trust-summary trust-summary-positive";
        bptf_sumnegtrust.innerText = "- ?";
        bptf_sumnegtrust.className = "trust-summary trust-summary-negative";
        bptf_trust_summary.appendChild(trust_summary_title);
        bptf_trust_summary.appendChild(bptf_sumpostrust);
        bptf_trust_summary.appendChild(bptf_sumnegtrust);

        summary_container.appendChild(bptf_summary);
        summary_container.appendChild(bptf_trust_summary);
    }

    if (mannco.is_enabled) {
        buildSummary(mannco);
    }

    if (reptf_enabled) {
        // Marketplace.tf
        mptf_sumtitle.innerText = "Marketplace.tf";
        mptf_sumtitle.id = "mptf-summary-title";
        mptf_sumtitle.className = "summary-title";
        mptf_sumtitle.href = `https://marketplace.tf/shop/${g_rgProfileData.steamid}`;
        mptf_sumtitle.target = "_blank";
        mptf_sumtitle.rel = "noreferrer";

        mptf_sumstatus.innerText = "Unknown";
        mptf_sumstatus.id = "mptf-summary-status";
        mptf_sumstatus.className = "summary-status summary-status-light";

        mptf_summary.appendChild(mptf_sumtitle);
        mptf_summary.appendChild(mptf_sumstatus);

        mptf_summary.id = "mptf-summary";
        mptf_summary.className = "site-summary";

        // Scrap.tf
        scrap_sumtitle.innerText = "Scrap.tf";
        scrap_sumtitle.id = "scrap-summary-title";
        scrap_sumtitle.className = "summary-title";
        scrap_sumtitle.href = `https://scrap.tf/profile/${g_rgProfileData.steamid}`;
        scrap_sumtitle.target = "_blank";
        scrap_sumtitle.rel = "noreferrer";

        scrap_sumstatus.innerText = "Unknown";
        scrap_sumstatus.id = "scrap-summary-status";
        scrap_sumstatus.className = "summary-status summary-status-light";

        scrap_summary.appendChild(scrap_sumtitle);
        scrap_summary.appendChild(scrap_sumstatus);

        scrap_summary.id = "scrap-summary";
        scrap_summary.className = "site-summary";

        // Add Marketplace.tf and Scrap.tf summaries when both are built
        summary_container.appendChild(mptf_summary);
        summary_container.appendChild(scrap_summary);
    }

    if (skial_enabled) {
        skial_sumtitle.innerText = "Skial";
        skial_sumtitle.id = "skial-summary-title";
        skial_sumtitle.className = "summary-title";
        skial_sumtitle.onclick = function () {window.open(`https://stats.skial.com/#summary/player/SteamID/All/${profile.id3}`, "_blank")};

        skial_sumstatus.innerText = "Unknown";
        skial_sumstatus.id = "skial-summary-status";
        skial_sumstatus.className = "summary-status summary-status-light";

        skial_summary.appendChild(skial_sumtitle);
        skial_summary.appendChild(skial_sumstatus);

        skial_summary.id = "skial-summary";
        skial_summary.className = "site-summary";
        summary_container.appendChild(skial_summary);
    }

    if (vacbanned.is_enabled) {
        buildSummary(vacbanned);
    }

    if (steamhistory.is_enabled) {
        buildSummary(steamhistory);
    }

    if (steamiduk.is_enabled) {
        buildSummary(steamiduk);
    }

    if (steamtrades.is_enabled) {
        buildSummary(steamtrades);
        buildRepCountSummary(steamtrades);
    }
}

function buildDetailsMenu() {
    console.log(`[DEBUG] | Calling buildDetailsMenu()`);

    // Determines where to insert the HTML code
    let leftprofile_area = document.getElementsByClassName("profile_leftcol")[0];
    let left_infobox = document.createElement("div");

    // Buttons for details toggle and settings
    var btn_container = document.createElement("div");
    btn_container.className = "btn-container";

    let toggle_btn = document.createElement("button");
    let settings_btn = document.createElement("button");

    toggle_btn.id = "toggle-details-btn";
    toggle_btn.innerText = "View links and details";
    toggle_btn.addEventListener("click", toggleDetails);

    settings_btn.id = "settings-btn";
    settings_btn.innerText = "Settings";

    btn_container.appendChild(toggle_btn);
    btn_container.appendChild(settings_btn);

    // Details menu
    left_infobox.id = "reputation-menu";
    left_infobox.className = "hidden"; // Hide on page load
    left_infobox.innerHTML = "<h1 class='reputation-menu-title'>Status</h1>";

    leftprofile_area.insertBefore(left_infobox, leftprofile_area.childNodes[0]);
    leftprofile_area.insertBefore(btn_container, leftprofile_area.childNodes[0]);

    // Trading status boxes
    if (steamrep.is_enabled) {
        buildRepContainer(steamrep);
    }

    if (bptf_enabled) {
        bptf_info.id = "bptf-information";
        bptf_info.className = "ban-status-normal";
        left_infobox.appendChild(bptf_info);
    }

    if (mannco.is_enabled) {
        buildRepContainer(mannco);
    }

    if (reptf_enabled) {
        marketplace_info.id = "mp-information";
        marketplace_info.className = "ban-status-normal";
        left_infobox.appendChild(marketplace_info);

        scrap_info.id = "scrap-information";
        scrap_info.className = "ban-status-normal";
        left_infobox.appendChild(scrap_info);
    }

    if (skial_enabled) {
        skial_info.id = "skial-information";
        skial_info.className = "ban-status-normal";
        left_infobox.appendChild(skial_info);
    }

    if (vacbanned.is_enabled) {
        buildRepContainer(vacbanned);
    }

    if (steamhistory.is_enabled) {
        buildRepContainer(steamhistory);
    }

    if (steamiduk.is_enabled) {
        buildRepContainer(steamiduk);
    }

    if (steamtrades.is_enabled) {
        buildRepContainer(steamtrades);
    }
}

function buildTableStatus(p_module, p_status, p_tablename) {
    let table_head = document.getElementById(`${p_module.id_prefix}-table-head`);
    let table_body = document.getElementById(`${p_module.id_prefix}-table-body`);
    let title_text, statuscell_text;
    let status_descriptions = {
        400: "Error 400, request was malformed/incorrect. Report this issue if it persists.",
        401: "Error 401, request was not authorized. Report this issue if it persists.",
        403: `Error 403${p_module.requires_apikey ? ", your API key was most likely entered incorrectly" : ". Try going on the website and try again after. Report the issue if it persists"}.`,
        404: "Error 404. This should not happen in normal circumstances. Report this issue if it persists.",
        405: "Error 405. This should not happen in normal circumstances. Report this issue if it persists.",
        408: `The request sent to ${p_module.module_name} took longer than ${p_module.module_name} was prepared to wait.`,
        414: "Error 414. This should not happen in normal circumstances. Report this issue if it persists",
        429: `Error 429, you might be checking profiles too quickly.`
    }

    if (!isNaN(p_status - parseFloat(p_status))) { // Check if status is a number (for HTTP status codes)
        // Table header text
        if (p_status in status_names) {
            title_text = `${p_tablename} - ${status_names[p_status]}`;
        } else {
            if (p_status >= 400 && p_status <= 499) {
                title_text = `${p_tablename} - Client error (${p_status})`;
            } else if (p_status >= 500 && p_status <= 599) {
                title_text = `${p_tablename} - Server error (${p_status})`;
            } else {
                title_text = `${p_tablename} - Unexpected status code (${p_status})`;
            }
        }

        // Status cell text
        if (p_status in status_descriptions) {
            statuscell_text = status_descriptions[p_status];
        } else {
            if (p_status >= 400 && p_status <= 499) {
                statuscell_text = `A client error (${p_status}) has occured when making a request to ${p_module.module_name}. Report this issue if it persists.`;
            } else if (p_status >= 500 && p_status <= 599) {
                statuscell_text = `Error ${p_status}, ${p_module.module_name} is currently having server issues.`;
            } else {
                statuscell_text = `Unexpected HTTP status code (${p_status}), report this issue if it persists.`;
            }
        }
    } else if (p_status === "Timed out") {
        title_text = `${p_tablename} - Timed out`;
        statuscell_text = `${p_module.module_name} did not respond quickly enough.`;
    } else if (p_status === "Application error") {
        title_text = `${p_tablename} - Application error`;
        statuscell_text = `An application error occured when attempting to connect to ${p_module.module_name}.`;
    }

    if (p_module.module_name === "SteamRep") {
        table_head.innerHTML = `
                               <tr>
                                   <th colspan="3" class="history-table-head" id="${p_module.id_prefix}-table-head">${title_text}</th>
                               </tr>`;
        let table_row = document.createElement("tr");
        let table_cell = document.createElement("td");
        table_cell.innerText = statuscell_text;
        table_cell.id = "history-error";
        table_cell.colSpan = 3;
        table_row.appendChild(table_cell);
        table_body.appendChild(table_row);
    }
}

function buildRepContainer(p_module) {
    console.log(`[DEBUG] | Calling buildRepContainer() for ${p_module.module_name}`);

    // Contains the title and details of a module
    let container = document.createElement("div");
    container.className = "module-info-container status-light";
    container.id = `${p_module.id_prefix}-info-container`;

    let title_container = document.createElement("div");
    title_container.className = "status-title-container";
    title_container.id = `${p_module.id_prefix}-title-container`;
    title_container.innerHTML = `<h2 id='${p_module.id_prefix}-title' class='status-title title-light'>${p_module.module_name} - Unknown</h2>`;
    container.appendChild(title_container);

    // Description and data containers
    let content = document.createElement("div");
    content.className = "status-info-container";
    content.id = `${p_module.id_prefix}-content`;
    let rep_description = document.createElement("p");
    rep_description.id = `${p_module.id_prefix}-reputation-description`;
    rep_description.className = "reputation-description";
    rep_description.innerHTML = `Unknown status - This should normally appear for less than one or two seconds.<br>Report this issue if it stays indefinitely.`;
    content.appendChild(rep_description);
    container.appendChild(content);

    // Add to "View links and details" menu
    document.getElementById("reputation-menu").appendChild(container);
}

function updateRepContainer(p_module, p_status) {
    let container = document.getElementById(`${p_module.id_prefix}-info-container`);
    let title = document.getElementById(`${p_module.id_prefix}-title`);
    let content = document.getElementById(`${p_module.id_prefix}-content`);
    let rep_description = document.getElementById(`${p_module.id_prefix}-reputation-description`);

    if (p_status.toString().startsWith("Connecting") || p_status.toString().startsWith("Loading") || p_status === "API Key Required") {
        container.className = `module-info-container status-light`;
        title.className = `status-title title-light`;
        title.innerText = `${p_module.module_name} - ${p_status}`;

        if (p_status.startsWith("Connecting")) {
            rep_description.innerText = `Attempting to connect to ${p_module.module_name}, this can occasionally take a while.`;
        } else if (p_status.startsWith("Loading")) {
            rep_description.innerText = `Loading information, this can occasionally take a while.`;
        } else if (p_status === "API Key Required") {
            if (p_module.module_name === steamiduk.module_name) {
                rep_description.innerHTML = `You will need to set the ID64 and API key in the settings menu to view SteamID.uk information without going on the website.<br>
                                             Your API key can be requested here:
                                             <a style="font-weight:bold;" href="https://steamid.uk/steamidapi/" target="_blank" rel="noreferrer">https://steamid.uk/steamidapi/</a><br>
                                             Do not share your API key with anyone, as it can be used to perform malicious actions under your identity.`;
            }
        }
    } else if (p_status === "No account") {
        container.className = `module-info-container status-noaccount`;
        title.className = `status-title title-noaccount`;
        title.innerText = `${p_module.module_name} - ${p_status}`;

        rep_description.innerText = `User has no account or data found on the website.`;
    } else if (p_status === "Normal") {
        container.className = `module-info-container status-normal`;
        title.className = `status-title title-normal`;
        title.innerText = `${p_module.module_name} - ${p_status}`;

        if (p_module.module_name === vacbanned.module_name) {
            rep_description.innerText = `User was not VAC banned at the last checked date/time.`;
        } else {
            rep_description.innerText = `No special reputation.`;
        }
    } else if (p_status === "Banned" || p_status === "Scammer") {
        container.className = `module-info-container status-danger`;
        title.className = `status-title title-danger`;
        title.innerText = `${p_module.module_name} - ${p_status}`;

        if (p_status === "Banned") {
            if (p_module.module_name === vacbanned.module_name) {
                rep_description.innerText = "User is VAC banned.";
            } else if (p_module.module_name === mannco.module_name) {
                rep_description.innerHTML = `<b>Reason:</b> <span id="mannco-banreason"></span>`;
            }
        } else if (p_status === "Scammer") {
            // Status exclusive to SteamRep
            rep_description.innerText = "You will get banned if you trade with this person.";
            content.innerHTML += `<br><p id='${p_module.id_prefix}-tags' class='tags-scammer'><b>Tags:</b> </p>`;
        }
    } else if (p_status === "Caution" || p_status === "Outdated data" || p_status === "Unbanned" || p_status === "Not indexed" ||
               p_status === "Database connectivity issue" || p_status === "API Key not set" || p_status === "ID64 not set" ||
               p_status === "ID64 not found" || p_status === "Invalid ID64/API Key settings" || p_status === "Daily lookup limit reached" ||
               p_status === "API Key Disabled" || p_status === "Internal API error" || p_status === "Unknown error" ||
               p_status === "Missing data") {
        container.className = `module-info-container status-warning`;
        title.className = `status-title title-warning`;
        title.innerText = `${p_module.module_name} - ${p_status}`;

        if (p_status === "Caution") {
            rep_description.innerText = "The user may have scammed someone and/or had suspicious behavior in the past.";
            content.innerHTML += `<br><p id='${p_module.id_prefix}-tags' class='tags-caution'><b>Tags:</b> </p>`;
        } else if (p_status === "Outdated data") {
            rep_description.innerText = "User was not VAC banned at the last checked date/time, but is currently VAC banned.";
        } else if (p_status === "Unbanned") {
            rep_description.innerText = "User was VAC banned previously but is no longer banned.";
        } else if (p_status === "Not indexed") {
            rep_description.innerText = "User was not indexed or the data expected was not found. Try again in a minute.";
        } else if (p_status === "Database connectivity issue") {
            rep_description.innerText = `${p_module.module_name} is currently having database issues.`;
        } else if (p_status === "API Key not set") {
            rep_description.innerHTML = `The ID64 appears to be set, but you also need to set the API key of your account in the settings menu.<br>
                                         Your API key can be requested here:
                                         <a style="font-weight:bold;" href="https://steamid.uk/steamidapi/" target="_blank" rel="noreferrer">https://steamid.uk/steamidapi/</a><br>
                                         Do not share your API key with anyone, as it can be used to perform malicious actions under your identity.`;
        } else if (p_status === "ID64 not set") {
            rep_description.innerText = "The API key appears to be set, but you also need to set the ID64 of your account in the settings menu.";
        } else if (p_status === "ID64 not found") {
            rep_description.innerHTML = "The profile you visited couldn't be found.<br>This means that the profile was never checked on SteamID's website before.";
        } else if (p_status === "Invalid ID64/API Key settings") {
            rep_description.innerHTML = "The ID64 and/or the API key in your settings are invalid.<br>Make sure both are correctly typed/pasted.";
        } else if (p_status === "Daily lookup limit reached") {
            rep_description.innerHTML = "You have reached your daily lookup limit.<br>You should be able to lookup more profiles tomorrow.";
        } else if (p_status === "API Key Disabled") {
            rep_description.innerText = "Your API key is disabled, contact a SteamID admin for help.";
        } else if (p_status === "Internal API error") {
            rep_description.innerText = `${p_module.module_name}'s API had an error, contact a SteamID admin if the issue persists.`;
        } else if (p_status === "Unknown error") {
            rep_description.innerText = `${p_module.module_name} gave an error response but did not specify which one.`;
        } else if (p_status === "Missing data") {
            rep_description.innerText = `The data expected for ${p_module.module_name} was not found. Report this issue if it persists.`;
        }
    } else if (p_status === "Admin") {
        // Status exclusive to SteamRep
        container.className = `module-info-container status-special`;
        title.className = `status-title title-special`;
        title.innerText = `${p_module.module_name} - ${p_status}`;
        rep_description.innerHTML = "The user is a trusted member of the community.<br>Be careful, as impersonators may post links to this profile to decieve you.";
        content.innerHTML += `<br><p id='${p_module.id_prefix}-tags' class='tags-trusted'><b>Tags:</b> </p>`;
    } else if (!isNaN(p_status - parseFloat(p_status))) { // Check if status is a number (for HTTP status codes)
        container.className = `module-info-container status-warning`;
        title.className = `status-title title-warning`;

        let status_descriptions = {
            400: "Error 400, request was malformed/incorrect. Report this issue if it persists.",
            401: "Error 401, request was not authorized. Report this issue if it persists.",
            403: `Error 403, you might be blocked/ratelimited${p_module.requires_apikey ? " or your API key wasn't entered correctly" : ". Try going on the website or try again later"}.`,
            404: "Error 404. This should not happen in normal circumstances. Report this issue if it persists.",
            405: "Error 405. This should not happen in normal circumstances. Report this issue if it persists.",
            408: `The request sent to ${p_module.module_name} took longer than ${p_module.module_name} was prepared to wait.`,
            414: "This should not happen in normal circumstances. Report this issue if it persists",
            429: `Error 429, you might be checking profiles too quickly.`
        }

        // Title
        if (p_status in status_names) {
            title.innerText = `${p_module.module_name} - ${status_names[p_status]} (${p_status})`;
        } else {
            if (p_status >= 400 && p_status <= 499) {
                title.innerText = `${p_module.module_name} - Client Error (${p_status})`;
            } else if (p_status >= 500 && p_status <= 599) {
                title.innerText = `${p_module.module_name} - Server Error (${p_status})`;
            } else {
                title.innerText = `${p_module.module_name} - Unexpected HTTP status code (${p_status})`;
            }
        }

        // Description
        if (p_status in status_descriptions) {
            rep_description.innerText = status_descriptions[p_status];
        } else {
            if (p_status >= 400 && p_status <= 499) {
                rep_description.innerText = `A client error has occured when making a request to ${p_module.module_name}. Report this issue if it persists.`;
            } else if (p_status >= 500 && p_status <= 599) {
                rep_description.innerText = `Error ${p_status}, ${p_module.module_name} is currently having server issues.`;
            } else {
                rep_description.innerText = "Unexpected HTTP status code, report this issue if it persists.";
            }
        }
    } else if (p_status.startsWith("Timed out")) {
        container.className = `module-info-container status-warning`;
        title.className = `status-title title-warning`;
        title.innerText = `${p_module.module_name} - ${p_status}`;
        rep_description.innerText = `${p_module.module_name} did not respond quickly enough.`;
    } else if (p_status.startsWith("Application error")) {
        container.className = `module-info-container status-warning`;
        title.className = `status-title title-warning`;
        title.innerText = `${p_module.module_name} - ${p_status}`;

        if (p_module.module_name === "VACBanned.com") {
            rep_description.innerHTML = `An application error occured while connecting to VACBanned.com.<br>If you are using HTTPS-only mode on your browser, you will have to accept the expired certificate on the website to view its information.`;
        } else {
            rep_description.innerHTML = `An application error occured while connecting to ${p_module.module_name}.<br>
                                         This may be due to a Content Security Policy issue, report this issue if it persists.`;
        }
    } else {
        container.className = `module-info-container status-warning`;
        title.className = `status-title title-warning`;
        title.innerText = `${p_module.module_name} - Invalid Status`;
        rep_description.innerText = `The status indicated in the code '${p_status}' does not exist. Report the issue if you see this.`;
    }
}

function switchSettingSection(p_section) {
    document.getElementById("general-settings").classList.add("hidden");
    document.getElementById("module-settings").classList.add("hidden");
    document.getElementById("link-settings").classList.add("hidden");
    document.getElementById("debug-settings").classList.add("hidden");
    document.getElementById("general-section-button").classList.remove("section-selected");
    document.getElementById("module-section-button").classList.remove("section-selected");
    document.getElementById("link-section-button").classList.remove("section-selected");
    document.getElementById("debug-section-button").classList.remove("section-selected");

    if (p_section === "General") {
        document.getElementById("general-settings").classList.remove("hidden");
        document.getElementById("general-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").innerText = "General settings";
    } else if (p_section === "Modules") {
        document.getElementById("module-settings").classList.remove("hidden");
        document.getElementById("module-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").innerText = "Module settings";
    } else if (p_section === "Links") {
        document.getElementById("link-settings").classList.remove("hidden");
        document.getElementById("link-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").innerText = "Link settings";
    } else if (p_section === "Debug") {
        document.getElementById("debug-settings").classList.remove("hidden");
        document.getElementById("debug-section-button").classList.add("section-selected");
        document.getElementById("setting-section-title").innerText = "Debug settings";
    } else {
        alert("Invalid section specified. Report the issue if you see this.");
    }
}

function buildSettingsModal() {
    console.log(`[DEBUG] | Calling buildSettingsModal()`);

    let responsive_header = document.getElementsByClassName("responsive_header")[0];

    // Defines where the settings button is
    let settings_btn = document.getElementById("settings-btn");

    // Settings modal
    let settings_modal = document.createElement("div");
    settings_modal.id = "settings-modal";
    settings_modal.className = "modal";
    settings_modal.innerHTML =
        `
    <div class="modal-content">
        <div class="settings-sidebar">
            <a id="general-section-button" class="section-selected">General</a>
            <a id="module-section-button">Modules</a>
            <a id="link-section-button">Links</a>
            <a id="debug-section-button">Debug</a>
            <p>v${GM_info.script.version}</p>
            <p>2024-06-03</p>
        </div>
        <span class="close">&times;</span>
        <div id="setting-section-title-container">
            <h1 id="setting-section-title">General settings</h1>
        </div>
        <div class="setting-element-container" id="general-settings">
            <form>
                <fieldset id="element-settings-container">
                    <h2 class="setting-title">Elements</h2>
                    <div>
                        <label for="agedisclaimer-enabled-input">Account age disclaimer:</label>
                        <input name="agedisclaimer-enabled-input" id="agedisclaimer-enabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamdetective.agedisclaimer_enabled ? "checked" : "" }>
                    </div>
                </fieldset>
                <hr class="settings-splitter">
            </form>
        </div>
        <div class="setting-element-container hidden" id="module-settings">
            <form>
                <fieldset id="steamrep-settings-container">
                    <div>
                        <h2 class="setting-title">SteamRep</h2>
                        <input name="steamrep-enabled-input" id="steamrep-enabled-input" class="setting-toggle" type="checkbox" ${steamrep.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamrep-historyenabled-input">History:</label>
                        <input name="steamrep-historyenabled-input" id="steamrep-historyenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamrep.history_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamrep-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="steamrep-summaryenabled-input" id="steamrep-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamrep.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamrep-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="steamrep-timeout-input" class="setting-textbox" name="steamrep-timeout-input" value="${steamrep.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="bptf-settings-container">
                     <div>
                          <h2 class="setting-title">Backpack.tf</h2>
                          <input name="bptf-enabled-input" id="bptf-enabled-input" class="setting-toggle" type="checkbox" ${bptf_enabled ? "checked" : "" }>
                     </div>
                     <p>To request/get the API key, go here: <a style="font-weight:bold;" href="https://backpack.tf/developer/apikey/view" target="_blank" rel="noreferrer">https://backpack.tf/developer/apikey/view</a></p>
                     <div>
                          <label for="bptf-apikey-input">Backpack.tf API Key:</label>
                          <input type="text" id="bptf-apikey-input" class="setting-textbox" name="bptf-apikey-input" value="${bptf_apikey}">
                     </div>
                     <div>
                          <label for="bptf-timeout-input">Timeout (in milliseconds):</label>
                          <input type="text" id="bptf-timeout-input" class="setting-textbox" name="bptf-timeout-input" value="${bptf_apitimeout}">
                     </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="mannco-settings-container">
                    <div>
                        <h2 class="setting-title">Mannco.store</h2>
                        <input name="mannco-enabled-input" id="mannco-enabled-input" class="setting-toggle" type="checkbox" ${mannco.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="mannco-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="mannco-summaryenabled-input" id="mannco-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${mannco.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="mannco-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="mannco-timeout-input" class="setting-textbox" name="mannco-timeout-input" value="${mannco.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="reptf-settings-container">
                    <div>
                        <h2 class="setting-title">Rep.tf (Scrap.tf + Marketplace.tf)</h2>
                        <input name="reptf-enabled-input" id="reptf-enabled-input" class="setting-toggle" type="checkbox" ${reptf_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="reptf-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="reptf-timeout-input" class="setting-textbox" name="reptf-timeout-input" value="${reptf_apitimeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="sid-settings-container">
                    <div>
                        <h2 class="setting-title">SteamID.uk</h2>
                        <input name="sid-enabled-input" id="sid-enabled-input" class="setting-toggle" type="checkbox" ${steamiduk.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <p>
                            To request/get the API key, go here: <a style="font-weight:bold;" href="https://steamid.uk/steamidapi/" target="_blank" rel="noreferrer">https://steamid.uk/steamidapi/</a>
                            <br> The ID64 to input is the one from the account you requested the API key with. This is not optional.
                        </p>
                        ${typeof g_steamID == 'string' ? " <h5 style='margin-bottom:5px;color:#f7ff00;'>ID64 of currently logged in account: " + g_steamID + "</h5>" : ""}
                    </div>
                    <div>
                        <label for="sid-myid-input">SteamID.uk ID64:</label>
                        <input type="text" id="sid-myid-input" class="setting-textbox" name="sid-myid-input" value="${steamiduk.my_id}">
                    </div>
                    <div>
                        <label for="sid-myid-apikey">SteamID.uk API Key:</label>
                        <input type="text" id="sid-apikey-input" class="setting-textbox" name="sid-apikey-input" value="${steamiduk.api_key}">
                    </div>
                    <div>
                        <label for="sid-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="sid-timeout-input" class="setting-textbox" name="sid-timeout-input" value="${steamiduk.api_timeout}">
                    </div>
                    <div>
                        <label for="sid-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="sid-summaryenabled-input" id="sid-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamiduk.summary_enabled ? "checked" : "" }>
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="skial-settings-container">
                    <div>
                        <h2 class="setting-title">Skial</h2>
                        <input name="skial-enabled-input" id="skial-enabled-input" class="setting-toggle" type="checkbox" ${skial_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="skial-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="skial-timeout-input" class="setting-textbox" name="skial-timeout-input" value="${skial_apitimeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="vacbanned-settings-container">
                <div>
                    <h2 class="setting-title">VACBanned.com</h2>
                    <input name="vacbanned-enabled-input" id="vacbanned-enabled-input" class="setting-toggle" type="checkbox" ${vacbanned.is_enabled ? "checked" : "" }>
                </div>
                <div>
                    <label for="vacbanned-summaryenabled-input">Show status summary in right-hand side:</label>
                    <input name="vacbanned-summaryenabled-input" id="vacbanned-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${vacbanned.summary_enabled ? "checked" : "" }>
                </div>
                <div>
                    <label for="vacbanned-timeout-input">Timeout (in milliseconds):</label>
                    <input type="text" id="vacbanned-timeout-input" class="setting-textbox" name="vacbanned-timeout-input" value="${vacbanned.api_timeout}">
                </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="steamhistory-settings-container">
                    <div>
                        <h2 class="setting-title">SteamHistory.net</h2>
                        <input name="steamhistory-enabled-input" id="steamhistory-enabled-input" class="setting-toggle" type="checkbox" ${steamhistory.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamhistory-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="steamhistory-summaryenabled-input" id="steamhistory-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamhistory.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="steamhistory-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="steamhistory-timeout-input" class="setting-textbox" name="steamhistory-timeout-input" value="${steamhistory.api_timeout}">
                    </div>
                </fieldset>
                <hr class="settings-splitter">
                <fieldset id="st-settings-container">
                    <div>
                        <h2 class="setting-title">SteamTrades.com</h2>
                        <input name="st-enabled-input" id="st-enabled-input" class="setting-toggle" type="checkbox" ${steamtrades.is_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="st-summaryenabled-input">Show status summary in right-hand side:</label>
                        <input name="st-summaryenabled-input" id="st-summaryenabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamtrades.summary_enabled ? "checked" : "" }>
                    </div>
                    <div>
                        <label for="st-timeout-input">Timeout (in milliseconds):</label>
                        <input type="text" id="st-timeout-input" class="setting-textbox" name="st-timeout-input" value="${steamtrades.api_timeout}">
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="setting-element-container hidden" id="link-settings">
            <p>Nothing here... yet.</p>
        </div>
        <div class="setting-element-container hidden" id="debug-settings">
            <form>
                <fieldset id="element-settings-container">
                    <p class="color-warning">
                        <b>WARNING:</b> Turning these options on without knowing what they do is not recommended.<br>
                        The script may not behave as intended if you enable them!
                    </p>
                    <h2 class="setting-title">Feature debugging</h2>
                    <div>
                        <label for="forcedfallback-enabled-input">Forced fallback mode:</label>
                        <input name="forcedfallback-enabled-input" id="forcedfallback-enabled-input" class="setting-toggle setting-toggle-sm" type="checkbox" ${steamdetective.forcedfallback_enabled ? "checked" : "" }>
                    </div>
                </fieldset>
                <hr class="settings-splitter">
            </form>
        </div>
        <div id="setting-button-container">
            <button id="save-input-btn">Save</button>
            <button id="reset-input-btn">Reset</button>
        </div>
    </div>
    `;
    document.getElementsByClassName("responsive_page_content")[0].prepend(settings_modal);
    let settings_close = document.getElementsByClassName("close")[0];
    settings_btn.onclick = function() {
        settings_modal.style.display = "block";
        responsive_header.style.zIndex = "0";
        document.querySelector("body").classList.add("scroll-lock");
    }
    settings_close.onclick = function() {
        settings_modal.style.display = "none";
        responsive_header.style.zIndex = "20";
        document.querySelector("body").classList.remove("scroll-lock");
    }
    let settings_save = document.getElementById("save-input-btn");
    settings_save.addEventListener("click", saveSettings);
    let settings_reset = document.getElementById("reset-input-btn");
    settings_reset.addEventListener("click", resetSettings);

    document.getElementById("general-section-button").onclick = () => switchSettingSection("General");
    document.getElementById("module-section-button").onclick = () => switchSettingSection("Modules");
    document.getElementById("link-section-button").onclick = () => switchSettingSection("Links");
    document.getElementById("debug-section-button").onclick = () => switchSettingSection("Debug");

    window.onclick = function(event) {
        if (event.target === settings_modal) {
            settings_modal.style.display = "none";
            responsive_header.style.zIndex = "20";
            document.querySelector("body").classList.remove("scroll-lock");
        }
    }
}

function setTimeoutSetting(p_id, p_storageid) {
    if (document.getElementById(p_id).value > 0 && document.getElementById(p_id).value <= 300000) {
        GM_setValue(p_storageid, document.getElementById(p_id).value);
    } else {
        GM_setValue(p_storageid, DEFAULT_TIMEOUT); // Set at default value if invalid or too big
    }
}

function saveSettings() {
    if (confirm("Are you sure that you want to save all your current settings?")) {
        // General settings
        GM_setValue("agedisclaimer_enabled", document.getElementById("agedisclaimer-enabled-input").checked);

        // Module settings
        GM_setValue("steamrep_enabled", document.getElementById("steamrep-enabled-input").checked);
        GM_setValue("steamrep_historyenabled", document.getElementById("steamrep-historyenabled-input").checked);
        GM_setValue("bptf_enabled", document.getElementById("bptf-enabled-input").checked);
        GM_setValue("mannco_enabled", document.getElementById("mannco-enabled-input").checked);
        GM_setValue("reptf_enabled", document.getElementById("reptf-enabled-input").checked);
        GM_setValue("skial_enabled", document.getElementById("skial-enabled-input").checked);
        GM_setValue("vacbanned_enabled", document.getElementById("vacbanned-enabled-input").checked);
        GM_setValue("steamhistory_enabled", document.getElementById("steamhistory-enabled-input").checked);
        GM_setValue("sid_enabled", document.getElementById("sid-enabled-input").checked);
        GM_setValue("strades_enabled", document.getElementById("st-enabled-input").checked);

        GM_setValue("steamrep_summaryenabled", document.getElementById("steamrep-summaryenabled-input").checked);
        GM_setValue("mannco_summaryenabled", document.getElementById("mannco-summaryenabled-input").checked);
        GM_setValue("vacbanned_summaryenabled", document.getElementById("vacbanned-summaryenabled-input").checked);
        GM_setValue("steamhistory_summaryenabled", document.getElementById("steamhistory-summaryenabled-input").checked);
        GM_setValue("sid_summaryenabled", document.getElementById("sid-summaryenabled-input").checked);
        GM_setValue("strades_summaryenabled", document.getElementById("st-summaryenabled-input").checked);

        GM_setValue("bptf_apikey", document.getElementById("bptf-apikey-input").value.trim());
        GM_setValue("sid_myid", document.getElementById("sid-myid-input").value.trim());
        GM_setValue("sid_apikey", document.getElementById("sid-apikey-input").value.trim());

        setTimeoutSetting("steamrep-timeout-input", "steamrep_apitimeout");
        setTimeoutSetting("bptf-timeout-input", "bptf_apitimeout");
        setTimeoutSetting("mannco-timeout-input", "mannco_apitimeout");
        setTimeoutSetting("reptf-timeout-input", "reptf_apitimeout");
        setTimeoutSetting("skial-timeout-input", "skial_apitimeout");
        setTimeoutSetting("vacbanned-timeout-input", "vacbanned_apitimeout");
        setTimeoutSetting("steamhistory-timeout-input", "steamhistory_apitimeout");
        setTimeoutSetting("sid-timeout-input", "sid_apitimeout");
        setTimeoutSetting("st-timeout-input", "strades_apitimeout");

        // Debug settings
        GM_setValue("forcedfallback_enabled", document.getElementById("forcedfallback-enabled-input").checked);

        alert("Saved settings.");
    }
}

function resetSettings() {
    if (confirm("Are you sure that you want to reset all your settings to default values? This cannot be undone.")) {
        document.getElementById("agedisclaimer-enabled-input").checked = true;

        document.getElementById("steamrep-enabled-input").checked = true;
        document.getElementById("steamrep-summaryenabled-input").checked = true;
        document.getElementById("steamrep-historyenabled-input").checked = true;
        document.getElementById("steamrep-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("bptf-enabled-input").checked = true;
        document.getElementById("bptf-apikey-input").value = "";
        document.getElementById("bptf-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("mannco-enabled-input").checked = true;
        document.getElementById("mannco-summaryenabled-input").checked = true;
        document.getElementById("mannco-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("reptf-enabled-input").checked = true;
        document.getElementById("reptf-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("skial-enabled-input").checked = true;
        document.getElementById("skial-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("vacbanned-enabled-input").checked = true;
        document.getElementById("vacbanned-summaryenabled-input").checked = false;
        document.getElementById("vacbanned-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("steamhistory-enabled-input").checked = true;
        document.getElementById("steamhistory-summaryenabled-input").checked = false;
        document.getElementById("steamhistory-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("sid-enabled-input").checked = true;
        document.getElementById("sid-summaryenabled-input").checked = false;
        document.getElementById("sid-myid-input").value = "";
        document.getElementById("sid-apikey-input").value = "";
        document.getElementById("sid-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("st-enabled-input").checked = false;
        document.getElementById("st-summaryenabled-input").checked = true;
        document.getElementById("st-timeout-input").value = DEFAULT_TIMEOUT;

        document.getElementById("forcedfallback-enabled-input").checked = false;

        GM_deleteValue("agedisclaimer_enabled");

        GM_deleteValue("steamrep_enabled");
        GM_deleteValue("steamrep_summaryenabled");
        GM_deleteValue("steamrep_historyenabled");
        GM_deleteValue("steamrep_apitimeout");

        GM_deleteValue("bptf_enabled");
        GM_deleteValue("bptf_apikey");
        GM_deleteValue("bptf_apitimeout");

        GM_deleteValue("mannco_enabled");
        GM_deleteValue("mannco_summaryenabled");
        GM_deleteValue("mannco_apitimeout");

        GM_deleteValue("reptf_enabled");
        GM_deleteValue("reptf_apitimeout");

        GM_deleteValue("skial_enabled");
        GM_deleteValue("skial_summaryenabled");
        GM_deleteValue("skial_apitimeout");

        GM_deleteValue("vacbanned_enabled");
        GM_deleteValue("vacbanned_summaryenabled");
        GM_deleteValue("vacbanned_apitimeout");

        GM_deleteValue("steamhistory_enabled");
        GM_deleteValue("steamhistory_summaryenabled");
        GM_deleteValue("steamhistory_apitimeout");

        GM_deleteValue("sid_enabled");
        GM_deleteValue("sid_summaryenabled");
        GM_deleteValue("sid_myid");
        GM_deleteValue("sid_apikey");
        GM_deleteValue("sid_apitimeout");

        GM_deleteValue("strades_enabled");
        GM_deleteValue("strades_summaryenabled");
        GM_deleteValue("strades_apitimeout");

        GM_deleteValue("forcedfallback_enabled");

        alert("Reset settings.");
    }
}

// Class Styles
GM_addStyle
(`
 :root {
     --color-error: red;
     --color-danger: yellow;
     --color-limited: yellow;
     --color-warning: orange;
 }

 #acc-age-disclaimer { padding: 10px 5px 10px 5px; background: #ffffff0d; color: white; width: 250px; display: inline-block; margin-bottom: 5px; margin-top: 5px; border-radius: 5px; }
 #account-info { background: rgba(0, 0, 0, 0.4) none repeat scroll 0% 0%; padding: 7px; text-align: center; border-radius: 5px; margin: -10px -10px 10px -10px; color: white; animation-name: popup; animation-duration: 0.3s; }
 #account-data-table { padding: 5px; width: 100%; }
 #account-data-url { font-family: monospace; font-size: 14px; }
 #account-loading-label { font-size: 16px; margin-bottom: -10px; }
 #bp-ban-list {margin-top: 3px;}
 #bptf-trust-summary {margin-top: -5px;}
 #mannco-massban-disclaimer {background: #000000bd; border-radius: 5px; padding: 15px; color: white; margin-bottom: 7px;}
 #nohistory-cell, .skialerror-cell, #vacbanned-error-cell, #history-error {color: var(--color-warning);}
 #history-optedout {background: #800; color: white; padding-top: 5px; padding-bottom: 5px;}
 #reputation-menu {background: rgba(0, 0, 0, 0.6) none repeat scroll 0% 0%; border-radius: 5px; margin-bottom: 13px; padding: 20px; animation-name: popup; animation-duration: 0.3s; }
 #save-input-btn, #reset-input-btn {width: 125px;height: 50px;background: black;border: 1px solid white;color: white;font-size: 18px;}
 #save-input-btn:hover, #reset-input-btn:hover {background: #ffffff1a}
 #save-input-btn:active #reset-input-btn:active {background: #ffffff80}
 #sr-history, #skial-servers, #skial-names, #vacbanned-details, #vacbanned-names, #st-details {max-height: 350px; overflow-y: auto; margin-top: 10px;}
 #sr-tags {display: inline;}
 .tags-scammer { color: orange; }
 .tags-trusted { color: yellow; }
 .tags-caution { color: #ef7; }
 #bp-bans {color: orange;}
 #toggle-details-btn, #settings-btn {width: 200px; height: 40px; background: rgba(0, 0, 0, 0.6) none repeat scroll 0% 0%; margin: -14px 12px 10px 12px; border-radius: 4px; text-align: center; border: none; color: white; transition: all 0.3s;}
 #toggle-details-btn:hover, #settings-btn:hover {background: #ffffff1a}
 #toggle-details-btn:active, #settings-btn:active {background: #ffffff80}
 #trust-summary-title {width: 110px;display: inline-block;text-align: center;line-height: 25px;background: #ffffff2e;}
 #vacbanned-details-table, #st-details-table {max-height: 300px;}
 #vacbanned-details-table tr td, #st-details-table tr td { text-align: left; padding-left: 15px; padding-top: 8px; padding-bottom: 8px; }
 #vacbanned-details-table tr td:first-child, #st-details-table tr td:first-child { font-weight: bold; background: #0005; }

 #community-ban-indicator { background: #c65d1e2b; color: #ff9a41; padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 5px; border: 1px solid #ff9a41; }
 #trade-ban-indicator, #vac-ban-indicator { background: #a948472b; color: #ff4646; padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 3px; border: 1px solid #ff4646; }
 #limited-account-indicator { background: #ffff002b; color: yellow; padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 5px; border: 1px solid yellow; }

 .account-data-cell { text-align: right; background: #ffffff14; padding-right: 10px; word-wrap: anywhere; }
 .account-label-cell { text-align: left; font-weight: bold; background: #ffffff2e; padding: 9px; width: 38px; transition: background 0.2s; user-select: none; }
 .account-flag { padding: 10px 0px 10px 0px; display: inline-block; margin-top: 5px; width: 94%; border-radius: 5px;}

 .ban-status-danger  { background: #f006; padding: 10px; margin-top: 10px; border-radius: 5px; color:yellow; margin-bottom: 20px; }
 .ban-status-light { background: #68696ad4; padding: 10px; margin-top: 10px; border-radius: 5px; color:white; margin-bottom: 20px; }
 .ban-status-noaccount .bp-detail-container { background: #00000050; }
 .ban-status-noaccount { background: #4d4d4dd4; padding: 10px; margin-top: 10px; border-radius: 5px; color:white; margin-bottom: 20px; }
 .ban-status-normal  { background: #ffffff1a; padding: 10px; margin-top: 10px; border-radius: 5px; color:white; margin-bottom: 20px; }
 .ban-status-special { background: #2cff0066; padding: 10px; margin-top: 10px; border-radius: 5px; color:white; margin-bottom: 20px; }
 .ban-status-warning { background: #c84b00d4; padding: 10px; margin-top: 10px; border-radius: 5px; color:white; margin-bottom: 20px; }

 .bp-detail-container, .sid-detail-container {margin-top: 5px;background: #00000030;padding: 10px;}
 .bp-research-link, .scrap-research-link, .mptf-research-link, .mannco-research-link, .sid-research-link, .skial-research-link, .vacbanned-research-link, .sh-research-link, .st-research-link { background: #ffffff1a; padding: 10px; margin-right: 10px; margin-top: 10px; display: inline-block; border-radius: 4px; width: 160px; font-size: 15px;}
 .bp-research-link:hover, .scrap-research-link:hover, .mptf-research-link:hover, .mannco-research-link:hover, .sid-research-link:hover, .skial-research-link:hover, .vacbanned-research-link:hover, .sh-research-link:hover, .st-research-link:hover { background: #ffffff30; transition: all 0.3s;}
 .bp-trust-btn, .bp-issue-btn {width: 160px;}

 .btn-container {display: flex; flex-direction: row; justify-content: center; align-items: center}

 .close { color: white; float: right; font-size: 28px; font-weight: bold;}
 .close:hover, .close:focus {color: yellow; text-decoration: none; cursor: pointer;}

 .text-bold { font-weight: bold; }
 .font-default { font-size: inherit !important; font-family: 'Motiva Sans', Arial, Helvetica, sans-serif !important; }
 .scroll-lock { height: 100%; overflow-y: hidden; }
 .color-error { color: var(--color-error); }
 .color-limited { color: var(--color-limited); }
 .color-warning { color: var(--color-warning); }
 .corner-topleft-5px { border-radius: 5px 0px 0px 0px; }
 .corner-topright-5px { border-radius: 0px 5px 0px 0px; }
 .corner-bottomleft-5px { border-radius: 0px 0px 0px 5px; }
 .corner-bottomright-5px { border-radius: 0px 0px 5px 0px; }
 .label-clickable:hover { background: #ffffff4a; cursor: pointer; }
 .label-clickable:active { background: #ffffff80 }

 .detail-mixed {background: #ff780057;}
 .detail-negative {background: #ff000057;}
 .detail-positive {background: #22c60057;}
 .detail-premium {background: #770ebf57;}

 .hidden {display: none !important;}

 #sh-info-container .history-table td { width: 33%; white-space: nowrap; }
 .history-table, .history-table th, .history-table td, #vacbanned-details-table, #st-details-table { border-collapse: collapse; }
 .history-table td { padding: 4px; }
 .history-table-head {font-size: 18px; height: 38px; background: #00000036;}
 .history-table tr td:first-child {white-space: nowrap;}
 .history-table-container {max-height: 350px; overflow-y: auto; margin-top: 10px;}
 .history-table, #vacbanned-details-table, #st-details-table { width: 100%; background: #00000030; text-align: center; overflow-y: auto; height: 100%; max-height: 350px;}

 .modal {display: none; position: fixed; z-index: 900; left: 0; top: 0; width: 100%; height: 100%; overflow: auto; background-color: rgb(0,0,0); background-color: rgba(0,0,0,0.8);}
 .modal-content { position: relative; background-color: black; color: white; margin: auto; padding: 20px; width: 1150px; height: 90vh; min-height: 300px; border: 1px solid gray; animation-name: popup-modal; animation-duration: 0.35s}

 .module-info-container { background: #ffffff1a; padding: 10px; margin-top: 10px; border-radius: 5px; color:white; margin-bottom: 20px; }

 .reputation-description { display: inline; }
 .reputation-menu-title { color: white; }

 .research-icon { display: inline-block; width: 16px; height: 16px; margin-right: 5px;}
 .research-site-container > .research-icon {transform: translateY(2px);}
 .research-link { margin-left: 5px; padding: 5px; background: #ffffff1a; margin-top: 10px; display: block; width: 100%; text-align: center; margin-right: 5px; border-radius: 3px; }
 .research-link-container { display: flex; flex-direction: row; justify-content: center; align-items: center; }
 .research-link:hover, .simple-site:hover { background: #ffffff30; transition: all 0.3s;}
 .research-site-container { background: #ffffff1a; padding: 10px; margin-right: 10px; margin-top: 10px; display: inline-block; border-radius: 4px; width: 274px; font-size: 15px; color: white;}
 .research-site-label {margin-left: 6px;}

 /* Settings menu */
 form > fieldset:first-child { margin-top: 12px; }
 form > fieldset:last-child { margin-bottom: 12px; }
 #setting-button-container {height: 10%; margin-top: 15px; margin-left: 150px;}
 #settings-modal fieldset {border: none;}
 #setting-section-title-container {margin-left: 150px;padding: 0px 10px; margin-bottom: 15px;}
 .setting-textbox {width: 220px;background: white !important;color: black !important;padding: 5px;}
 .setting-title {display: inline; color: white; font-weight: bold; user-select: none;}
 .setting-toggle {appearance: none; background: #ff0000; color: white; border-radius: 5px; padding: 5px; min-width: 36px; text-align:center; font-family: "Motiva Sans",Arial,Helvetica,sans-serif;}
 .setting-toggle-sm {font-size: 10px;font-weight: bold;}
 .setting-toggle:checked {background: #00f000; color: #014401;}
 .setting-toggle:checked:after {content: "ON";}
 .setting-toggle:not(:checked):before {content: "OFF";}
 .setting-toggle:hover, #save-input-btn:hover, #reset-input-btn:hover {cursor: pointer;}
 h2 + .setting-toggle {transform: translateY(-4px);}
 .setting-element-container {border: 1px solid #757575;overflow: auto;margin-left: 150px;padding: 0px 10px;height: calc(100% - 115px);}
 .setting-element-container a {color: #36c7ff;}
 .settings-sidebar a {padding: 10px;text-decoration: none;font-size: 20px;color: white;display: block;padding-left: 9px;background: #242424;margin-bottom: 8px;transition: all 0.2s;height: 32px;line-height: 32px;}
 .settings-sidebar a.section-selected {background:#175ca6;}
 .settings-sidebar a:hover {background:#175ca6;font-weight:bold;font-size:22px;}
 .settings-sidebar > a:first-child { margin-top: 15px; }
 .settings-sidebar p {text-align: center}
 .settings-sidebar {height: 100%;width: 150px; top: 0;left: 0;position: absolute; background: #171717;}

 .sid-subdetail {background: #00000030; padding: 10px; margin-top: 10px;}
 .sid-subtitle {font-size: 20px;}
 .sid-subtitle-container {text-align: center; padding: 10px; background: #00000036;}

 .site-summary {background: rgba(0, 0, 0, 0.3) none repeat scroll 0% 0%; margin-bottom: 5px; color:white; animation-name: popup; animation-duration: 0.35s; max-width: 268px;}

 .settings-splitter {border: 1px dashed #ffffff75; margin-top: 5px; margin-top: 12px; margin-bottom: 12px;}
 .splitter {border: 1px solid #ffffff75; margin-top: 5px; margin-bottom: 14px;}

 .sr-research-link { background: #ffffff1a; padding: 10px; margin-right: 10px; margin-top: 10px; display: inline-block; border-radius: 4px; width: 235px; font-size: 15px;}
 .sr-research-link:hover { background: #ffffff30; transition: all 0.3s;}
 .sr-warning-label {color:orange; display:inline;}

 .status-info-container {padding: 5px;}
 .status-title {font-weight: bold !important; text-shadow: none !important;}
 .status-title-container {padding: 10px; background: #00000069; border-radius: 5px; margin-bottom: 3px;}
 .status-danger { background: #f006; color: var(--color-danger); }
 .status-warning { background: #c84b00d4; }
 .status-light { background: #68696ad4; }
 .status-normal { background: #ffffff1a; }
 .status-special { background: #2cff0066; }
 .status-noaccount { background: #4b4b4bd4; }

 .summary-status {display: inline-block;width: 158px;text-align: center;height: 35px;line-height: 35px;}
 .summary-status-danger  { background: #f006; color:yellow; }
 .summary-status-light { background: #68696ad4; color:white; }
 .summary-status-noaccount { background: #4b4b4bd4; color:white; }
 .summary-status-normal  { background: none; color:white; }
 .summary-status-special { background: #2cff0066; color:white; }
 .summary-status-warning { background: #c84b00d4; color:white; }
 .summary-title {background: #ffffff0d;display: inline-block;line-height: 35px;height: 35px;width: 110px;text-align: center; transition: background 0.3s;}
 .summary-title:hover {background: #ffffff45; cursor:pointer;}

 .rep-summary {text-align: center; color: white; border-collapse: collapse; table-layout: fixed; width: 268px; animation-name: popup; animation-duration: 0.35s; margin-top: -5px;}
 .rep-summary-positive {background: #22c60057; width: 77px;}
 .rep-summary-negative {background: #ff000057; width: 77px;}
 .rep-summary-title {width: 108px;line-height: 25px;background: #d0d0d02e;}

 .title-danger { color: yellow !important; }
 .title-normal, .title-special, .title-warning, .title-light, .title-noaccount { color: white !important; }
 .trust-summary {width: 79px;display: inline-block;text-align: center;line-height: 25px;}
 .trust-summary-negative {background: #ff000057;}
 .trust-summary-positive {background: #22c60057;}

 .username-label-banned {text-decoration: line-through; color: red;}
 .username-label-special {color: rgb(130, 255, 103); font-weight: bold;}

 /* Adjustments for long URLs */
 @media only screen and (min-width: 911px) {
    .url-max-long {font-size: 11px !important;}
    .url-extra-long {font-size: 13px !important;}
    .url-long {padding-right: 0px; text-align: center;}
 }

 /* Styles for smaller screens */
 @media only screen and (max-width: 1200px) {
    .modal-content {width: 90%;}
 }

 @media only screen and (max-width: 900px) {
    .settings-sidebar {width: 100px;}
    .settings-sidebar a {padding-left: 5px;font-size: 18px;}
    .setting-element-container, #setting-section-title-container, #setting-button-container {margin-left: 100px;}
 }

 @media only screen and (max-width: 393px) {
    #save-input-btn, #reset-input-btn {width: 40%;}
    .setting-element-container {height: calc(100% - 155px);}
 }

 /* Animations */
 @keyframes popup {
  from {transform: scale(0.1);}
  to {transform: scale(1);}
 }

 @keyframes popup-modal {
  from {transform: scaleX(0.1);}
  to {transform: scaleX(1);}
 }

`);

buildSummaryContainer();
buildDetailsMenu();
buildSettingsModal();
getSteamInfo();